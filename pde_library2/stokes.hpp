/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2024,
    
     Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#ifndef MAD_Stokes
#define MAD_Stokes

#include <iostream>
#include <math.h>
#include <vector>
#include <slepc.h>
#include <STLvectorUtils.hpp>
#include <petscWrapper.hpp>
#include <tools.hpp>
#include <parameters.hpp>
#include <masterElement.hpp>
#include <geometry.hpp>
#include <boundaries.hpp>
#include <calculus.hpp>
#include <fem.hpp>

using namespace std;

class Stokes{

  public:
    Stokes(){}
    ~Stokes(){}

    void initialize(Parameters parameters, const Geometry & geometry);
    void finalize();
    void assembleSUparameters();
    void computeJacobians();
    void computeLocal2GlobalMappings();
    void update(Vec u, double time);
    void setSolver();
    void assembleLHS();
    Vec solve();

    Mat A;
    KSP ksp;
    Calculus calculus;

    int nbDofs;
    Boundary bd;

  private:

    Geometry geo;
    MasterElement fe;
    MasterElementBD feBD;
    Parameters par;

    /* Streamline upwind stabilization */
    vector<vector<double>> m_tau_supg;
    vector<MasterElement> m_finiteElements;

    PetscErrorCode code;
    int m_verbose;

    int nbDofsPerNode; 
    int nbVertices;
    int nbDofsVel;
    int nbDofsPress;
    int nbNodesPerElement;
    vector<vector<int>> m_loc2Global;

    int m_world_rank, m, n;
    vector<Mat> m_ip;

    FEM fem;

};  

#endif
