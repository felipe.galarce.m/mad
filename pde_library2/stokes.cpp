/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2023,
    
     Felipe Galarce at INRIA/WIAS/PUCV

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<stokes.hpp>

void Stokes::initialize(Parameters parameters, const Geometry & geometry){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "Navier Stokes: Initializing" << endl;
  par = parameters;
  geo = geometry;
  bd.initialize(par, geo);
  calculus.initialize(par, geo, bd);
  fe.initialize(par, geo.dimension());
  feBD.initialize(par, geo.dimension());
  m_verbose = par.verbose();

  nbDofs = 0;
  for (int i = 0; i < par.nbVariables(); i++){
    nbDofs += par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbDofsPress = geo.nbVertices;
  nbDofsVel = 0;
  for (int i = 0; i < par.nbVariables()-1; i++){
    nbDofsVel = par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbVertices = geo.nbVertices;
  nbDofsPerNode = par.nbDofsPerNode()[0]; 
  nbNodesPerElement = geo.dimension()+1; 

  mat(A, nbDofs, nbDofs); 

  fem.initialize(par, geo, bd, A);
  code = MatGetOwnershipRange(A, &m, &n); CHKERR(code);

  if (m_world_rank == 0) cout << "Navier Stokes: dofs splitting:" << endl;
  cout << "  Proc #"<< m_world_rank << " : " << m << " " << n << endl;

  assembleSUparameters();
  computeJacobians();
  computeLocal2GlobalMappings();
}

void Stokes::finalize(){
  MatDestroy(&A);
  KSPDestroy(&ksp);
}

void Stokes::assembleSUparameters(){
  /* Stabilization parameter
     Calculate as given in Tobiska, L., Lube, G., 1991. Math. 59, 13–29. https://doi.org/10.1007/BF01385768
     The term involving the velocity norm is missing */
  double viscosity = par.viscosity();
  m_tau_supg.resize(geo.elements().size());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Assembling Streamline Upwind stabilization parameters. Part " << partId << endl;
    m_tau_supg[partId].resize(geo.elements()[partId].size());

    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      fem.setSimplex(simplex);

      double Ck = 30.0;
      double hk = fem.feSize();
      double tau_2 = pow(viscosity, 2.0)/pow(hk, 4.0);
      m_tau_supg[partId][feId] = pow(Ck*tau_2, -0.5);
    }
  }
}

void Stokes::computeJacobians(){
  m_finiteElements.resize(geo.nbElements());
  int iter = 0;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Computing Jacobians. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      m_finiteElements[iter].initialize(par, geo.dimension());
      m_finiteElements[iter].setCoordinates(coordinates);
      m_finiteElements[iter].computeSize();
      iter = iter + 1;
    }
  }
}

void Stokes::computeLocal2GlobalMappings(){
  int iter = 0;
  m_loc2Global.resize(geo.nbElements());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Computing local2Global mappings." << endl;
    for (vector<int> simplex : geo.elements()[partId]){ /* loop on elements */
      int nbNodesPerElement = simplex.size();
      /* loc2glob dof mapping */
      m_loc2Global[iter].resize(nbNodesPerElement*nbDofsPerNode);
      for (int i = 0; i < nbNodesPerElement; i++){
        for (int comp = 0; comp < nbDofsPerNode; comp++){
          m_loc2Global[iter][nbDofsPerNode*i+comp] = nbDofsPerNode*simplex[i]+comp;
        }
      }
      iter = iter + 1;
    }
  }
}

void Stokes::assembleLHS(){
  int iter = 0;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Assembling discretization matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      fem.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]);
      iter = iter + 1;

      if (feId % (geo.elements()[partId].size() / m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }

      for (int var = 0; var < par.nbVariables()-1; var++){

        /* grad u : grad v */
        for (int comp = 0; comp < geo.dimension(); comp++){
          // Add laplacian terms
          if (comp == var){
            fem.du_x_dv_y(2.0*par.viscosity(), comp, comp, var, var);
          } else {
            fem.du_x_dv_y(par.viscosity(), comp, comp, var, var);
          }
        }
      }

      /* -p div(v) */
      for (int comp = 0; comp < geo.dimension(); comp++){
        fem.u_dv_y(-1.0, comp, comp, geo.dimension());
      }

      /* div(u) q */
      for (int comp = 0; comp < geo.dimension(); comp++){
        fem.du_x_v(1.0, comp, geo.dimension(), comp);
      }

      /* Brezzi Pitkaranta term  */
      for (int comp = 0; comp < geo.dimension(); comp++){
        fem.du_x_dv_y(m_tau_supg[partId][feId], comp, comp, geo.dimension(), geo.dimension());
      }
    }
  }
  code = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERR(code);

  double normLHS = norm(A);
  if (m_world_rank == 0) cout << "Navier Stokes: Norm LHS = " << normLHS << endl;

  if (m_world_rank == 0) cout << "KSP: Setting operators" << endl;
  configureKSP(ksp, par);
  code = KSPSetOperators(ksp, A, A); CHKERR(code); 
  code = KSPSetUp(ksp); CHKERR(code);
}

Vec Stokes::solve(){

  Vec b = zeros(geo.nbVertices*par.nbVariables());
  bd.block(A, "symmetric");
  bd.block(b, "symmetric");

  Vec u = zeros(nbDofs);
  if (m_world_rank == 0) cout << "Stokes: Solving linear system" << endl;
  code = KSPSolve(ksp, b, u); CHKERR(code); 
  
  int its;
  double rnorm;
  code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
  code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
  if (par.solver() == "gmres"){
    if (m_world_rank == 0) cout << "Stokes: " << its << " iterations. Residual norm = " << rnorm << endl;
  }
  return u;
}
