/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2024,
    
     Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<saint_venant.hpp>

void SV::initialize(Parameters parameters, const Geometry & geometry){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "SV: Initializing" << endl;
  par = parameters;
  geo = geometry;
  fe.initialize(par, geo.dimension());
  m_verbose = par.verbose();
  nbDofs = 0;
  assert(par.nbDofsPerNode().size() == 2);

  nbVertices = geo.nbVertices;
  nbDofs = (par.nbDofsPerNode()[0] + par.nbDofsPerNode()[1])*nbVertices;
  nbDofsVel = geo.nbVertices;
  nbDofsArea = geo.nbVertices;
  nbDofsPerNode = par.nbDofsPerNode()[0]; 

  vec(u0, nbDofs);

  mat(A_static, nbDofs, nbDofs); 
  mat(A_transient, nbDofs, nbDofs); 
  mat(M, nbDofs, nbDofs); 

  femStat.initialize(par, geo, A_static);
  femTran.initialize(par, geo, A_transient);
  femMass.initialize(par, geo, M);

  /* Initial condition */
  double V0 = 1.0;
  double A0 = 1.0;
  for (int i = 0; i < geo.nbVertices; i++){
    vecSet(u0, i, V0);
    vecSet(u0, geo.nbVertices + i, A0);
  }
  assemble(u0); 
}

void SV::finalize(){
  MatDestroy(&A_static);
  MatDestroy(&A_transient);
  VecDestroy(&u0);
  KSPDestroy(&ksp);
}

Mat SV::assembleLHS(){

  Vec u0_seq = getSequential(u0);
  code = MatZeroEntries(A_transient); CHKERR(code);
  
  for (int partId = 0; partId < geo.elements().size(); partId++){

    if (m_world_rank == 0) cout << "SV: Assembling discretization matrix. Part " << partId << endl;

    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on finiteElement */

      vector<int> simplex = geo.elements()[partId][feId];
      femTran.setSimplex(simplex);

      /* Assemble elementary matrices */
      vector<double> u_element = geo.getNodalValues(u0_seq, simplex, 1, 0);
      vector<double> a_element = geo.getNodalValues(u0_seq, simplex, 1, 1);

      double a_mean = (a_element[0] + a_element[1])/2.0;

      /* u du/dx , v */
      femTran.a_du_x_v(1.0, 0, u_element, 0, 0); 

      /* u/A0 , v */
//      femTran.u_dot_v_scalar(-par.kappa()/a_mean, 0, 0);

      /* A0 du/dx , q */
      femTran.a_du_x_v(1.0, 0, a_element, 1, 0); 

      /* u0 dA/dx , q */
      femTran.a_du_x_v(1.0, 0, u_element, 1, 1); 
    
      /* 1/2 rho (1/sqrt(A0) da/dx , v) */
      vector<double> sq_a_element(2);
      sq_a_element[0] = 1.0/sqrt(a_element[0]);
      sq_a_element[1] = 1.0/sqrt(a_element[1]);
      femTran.a_du_x_v(1.0/2.0/par.density()*par.beta(), 0, sq_a_element, 0, 1); 
    }
  }
  assemble(A_transient);

  double normLHS = norm(A_transient);
  if (m_world_rank == 0) cout << "SV: Norm LHS time dependent = " << normLHS << endl;

  code = MatAXPY(A_transient, 1.0, A_static, DIFFERENT_NONZERO_PATTERN); CHKERR(code);

  normLHS = norm(A_transient);
  if (m_world_rank == 0) cout << "SV: Norm LHS = " << normLHS << endl;

  return A_transient;
}

Mat SV::assembleLHS_static(){

  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "SV: Assembling discretization matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on finiteElement */

      if (feId % (geo.elements()[partId].size() / par.verbose()) == 0){
        if (m_world_rank == 0) cout << "    Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }
      vector<int> simplex = geo.elements()[partId][feId];
      femMass.setSimplex(simplex);
      femStat.setSimplex(simplex);

      /* u \cdot v */
      for (int var = 0; var < par.nbVariables(); var++){
        femMass.u_dot_v_scalar(1.0/par.timeStep(), var, var);
      }
//      femStat.du_x_dv_y(femStat.feSize()*femStat.feSize(), geo.dimension(), geo.dimension());
    }
  }
  assemble(M);
  assemble(A_static);

  /* LHS static matrix */
  code = MatAXPY(A_static, 1.0, M, DIFFERENT_NONZERO_PATTERN); CHKERR(code);

  double normLHS = norm(A_static);
  if (m_world_rank == 0) cout << "SV: Norm LHS = " << normLHS << endl;

  return A_static;
}

Vec SV::assembleRHS(){
  Vec b = zeros(nbDofs);
  code = MatMult(M, u0, b); CHKERR(code);
  double normRHS  = norm(b);
  if (m_world_rank == 0) cout << "SV: Norm RHS = " << normRHS << endl;
  return b;
}

void SV::update(Vec u, double time){
  code = VecZeroEntries(u0); CHKERR(code);
  code = VecAXPY(u0, 1.0, u); CHKERR(code);
}

void SV::setSolver(){
  if (m_world_rank == 0) cout << "KSP: Configuring" << endl;
  configureKSP(ksp, par);
}

void SV::setLHS(Mat C){
  if (m_world_rank == 0) cout << "KSP: Seting operators" << endl;
  code = KSPSetOperators(ksp, C, C); CHKERR(code); 
  code = KSPSetUp(ksp); CHKERR(code);
}

void SV::solve(Vec b, Vec u){
  if (m_world_rank == 0) cout << "SV: Solving linear system" << endl;
  code = KSPSolve(ksp, b, u); CHKERR(code); 
  int its;
  double rnorm;
  code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
  code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
  if (par.solver() == "gmres"){
    if (m_world_rank == 0) cout << "SV: " << its << " iterations. Residual norm = " << rnorm << endl;
  }
}
