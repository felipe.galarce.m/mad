#include<matrix.hpp>

Matrix::Matrix(int m, int n, string type){
  mat(A, m, n, type);
}

Matrix::Matrix(Mat B){
  PetscErrorCode code;
  code = MatDuplicate(B, MAT_COPY_VALUES, &A); CHKERR(code);
  //MatDestroy(&B);
}

Matrix::Matrix(Vec & diag){
  PetscErrorCode code;
  int m;
  VecGetSize(diag, &m);
  mat(A, m, m);
	code = MatDiagonalSet(A, diag, INSERT_VALUES); CHKERR(code); 
}

Matrix operator*(Matrix A, Matrix B){
  int m, n;
  PetscErrorCode code;
  MatGetSize(A.petsc(), &m, NULL);
  MatGetSize(B.petsc(), NULL, &n);
  Mat P = mat(m,n);
  code = MatMatMult(A.petsc(), B.petsc(), MAT_INITIAL_MATRIX, PETSC_DEFAULT, &P); CHKERR(code);
  Matrix Q(P);
  return Q;
}

Matrix operator+(Matrix A, Matrix B){
  PetscErrorCode code;
  int m, n;
  code = MatGetSize(A.petsc(), &m, &n); CHKERR(code);
  Mat C = mat(m, n);
  code = MatDuplicate(A.petsc(), MAT_COPY_VALUES, &C); CHKERR(code);
  code = MatAXPY(C, 1.0, B.petsc(), DIFFERENT_NONZERO_PATTERN); CHKERR(code);
  Matrix Q(C);
  return Q;
}

Matrix operator-(Matrix A, Matrix B){
  PetscErrorCode code;
  int m, n;
  code = MatGetSize(A.petsc(), &m, &n); CHKERR(code);
  Mat C = mat(m, n);
  code = MatDuplicate(A.petsc(), MAT_COPY_VALUES, &C); CHKERR(code);
  code = MatAXPY(C, -1.0, B.petsc(), DIFFERENT_NONZERO_PATTERN); CHKERR(code);
  Matrix Q(C);
  return Q;
}

Matrix transpose(Matrix & A){
  Mat Apetsc = A.petsc();
  Mat Atr = transpose(Apetsc);
  Matrix A_tr(Atr);
  return A_tr;
}

Matrix operator*(double coef, Matrix A){
  PetscErrorCode code;
  MatScale(A.petsc(), coef);
  Matrix Q(A.petsc());
  return Q;
}
