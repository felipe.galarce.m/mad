/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2024,
    
     Mauricio Portilla & Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#ifndef MAD_matrix
#define MAD_matrix

#include <iostream>
#include <math.h>
#include <vector>
#include <slepc.h>
#include <STLvectorUtils.hpp>
#include <petscWrapper.hpp>
#include <tools.hpp>
#include <fstream>
#include <matrix.hpp>
#include <parameters.hpp>

class Matrix {
public:
	Matrix(){}
	Matrix(int m, int n, string type = "dense");
	Matrix(Mat B);
	Matrix(Vec & diag);
  Mat petsc(){
    return A;
  }
  Mat A;
};


Matrix operator*(Matrix A, Matrix B);
Matrix operator+(Matrix A, Matrix B);
Matrix operator-(Matrix A, Matrix B);
Matrix operator*(double coef, Matrix A);
Matrix trasponse(Matrix & A);

#endif
