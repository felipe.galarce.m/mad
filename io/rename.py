import os

folder = './sim00000/'
var_name = 'vort'
nbSnaps = 4000

def list2str(list0):
	return ''.join(list0)

def wild(numb):
	numb_string = str(numb)
	numb_len = len(numb_string)
	wildcard = (5-numb_len)*['0']
	wildcard.extend(numb_string)
	return list2str(wildcard)

for i in range(0,nbSnaps):
	os.system('mv '+folder+var_name+'_'+str(i)+'.csv '+folder+var_name+'.'+wild(i)+'.scl')