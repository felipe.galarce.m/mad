import os

#folder = './vks_default_vort/sim00000/'
var_name = 'vort'

file_list = os.listdir()
nbVertices = 3885
nbSnaps = 4000


for fi in range(0,nbSnaps):
	filename = file_list[fi]
	print(filename)
	with open(file_list[fi], 'r') as f:
		lines = f.readlines()
		lines = lines[(-nbVertices-1):]  # Only considers the group 5, the last one
		lines[0] = 'Scalar per node\n'
		for i in range(1, len(lines)):
			if len(lines[i]) != 13:
				# Iterates over the found number
				if len(lines[i]) < 13:
					diff = 13-len(lines[i])
					if lines[i][0] != '-':
						lines[i] = ' '+lines[i]
					if diff>1:
						for j in range(0, len(lines[i])):
							if lines[i][j] == 'e':
								lines[i] = lines[i][0:j]+ (diff-1)*'0' + lines[i][j:]
				else:  #len(lines[i]) > 13
					diff = len(lines[i])-13
					if lines[i][0] != '-':
						lines[i] = ' '+lines[i]
					if diff>1:
						for j in range(0, len(lines[i])):
							if lines[i][j] == 'e':
								lines[i] = lines[i][0:j-diff] + lines[i][j:]
			if i>0 and (i)%6!=0:
				lines[i] = lines[i][0:-1]
	#Erase file contents
	open(filename, 'w').close()
	#Rewrite file contents
	with open(filename, 'w') as f:
		for line in lines:
			f.write(line)
