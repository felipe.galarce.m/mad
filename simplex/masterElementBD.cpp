/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2025,
    
     Felipe Galarce at INRIA / WIAS / PUCV

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<masterElementBD.hpp>

void MasterElementBD::initialize(Parameters parameters, int dimension){

  par = parameters;
  m_dimension = dimension;
  m_nbNodesPerElement = m_dimension;
  m_nbDofsPerNode = par.nbDofsPerNode()[0];

  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);

  m_quadraturePoints.resize(par.nbQuadraturePointsBD());
  m_weights.resize(par.nbQuadraturePointsBD());
  for (int i = 0; i < par.nbQuadraturePointsBD(); i++){ 
    m_quadraturePoints[i].resize(3);  
    for (int comp = 0; comp < 3; comp++){
      m_quadraturePoints[i][comp] = 0.0;  
    }
  }

  /* Qudrature rules based on Legendre polynomials. Exact for a poly of degree 2n-1 */
  /* Reference elements are [-1,1], the origin half of [0,1] x [0,1], and the origin half of [0,1] x [0,1] x [0,1] */
  vector<vector<double>> q_rule = importdata(par.root_dir() 
    + "/simplex/quadrature_rules/" + to_string(m_dimension-1) + "D_" + to_string(par.nbQuadraturePointsBD()) + "p.txt");
  for (int i = 0; i < par.nbQuadraturePointsBD(); i++){ 
    for (int comp = 0; comp < m_dimension-1; comp++){
      m_quadraturePoints[i][comp] = q_rule[i][comp];
    }
    m_weights[i] = q_rule[i][m_dimension-1];
  }

  /* Pre-compute shape functions on quadrature points */
  m_phi.resize(m_nbNodesPerElement);
  for (int i = 0; i < m_nbNodesPerElement; i++){
    m_phi[i].resize(par.nbQuadraturePointsBD());
  }
  for (int i = 0; i < par.nbQuadraturePointsBD(); i++){
    if (m_dimension == 3){
      m_phi[0][i] = 1.0 - m_quadraturePoints[i][0] - m_quadraturePoints[i][1]; /* 1 - \xi - \eta  */
      m_phi[1][i] = m_quadraturePoints[i][0]; /* \xi    */
      m_phi[2][i] = m_quadraturePoints[i][1]; /* \eta   */
    } else if (m_dimension == 2){
      m_phi[0][i] = 1.0/2.0 * (1.0 - m_quadraturePoints[i][0]); /* 1/2 (1 - \xi)  */
      m_phi[1][i] = 1.0/2.0 * (1.0 + m_quadraturePoints[i][0]); /* 1/2 (1 + \xi)  */
    } else {
      exit(1);
    }
  } 

  m_grad_phi.resize(m_dimension);

  if (m_dimension == 3){
    m_grad_phi[0].resize(m_dimension-1);
    m_grad_phi[1].resize(m_dimension-1);
    m_grad_phi[2].resize(m_dimension-1);

    m_grad_phi[0][0] = -1;
    m_grad_phi[0][1] = -1;

    m_grad_phi[1][0] = 1;
    m_grad_phi[1][1] = 0;

    m_grad_phi[2][0] = 0;
    m_grad_phi[2][1] = 1;

  } else if (m_dimension == 2){
    m_grad_phi[0].resize(m_dimension-1);
    m_grad_phi[1].resize(m_dimension-1);
    m_grad_phi[0][0] = -1.0/2.0;
    m_grad_phi[1][0] =  1.0/2.0;
  }

  /* allocate jacobian */
  m_J.resize(3);
  for (int i = 0; i < 3; i++){
    m_J[i].resize(m_dimension - 1);
  }

  m_weights_sum = 0.0;
  for (int qp = 0; qp < par.nbQuadraturePointsBD(); qp++){
    m_weights_sum = m_weights_sum + m_weights[qp];
  }
}

void MasterElementBD::setCoordinates(const vector<vector<double>> & coordinates){
  m_coordinates = coordinates;

  if (m_dimension == 2){
    m_J[0][0] = 1.0/2.0*m_coordinates[1][0] - 1.0/2.0*m_coordinates[0][0]; 
    m_J[1][0] = 1.0/2.0*m_coordinates[1][1] - 1.0/2.0*m_coordinates[0][1]; 
    m_J[2][0] = 0.0;
  }

  if (m_dimension == 3){
    m_J[0][0] = m_coordinates[1][0] - m_coordinates[0][0]; 
    m_J[1][0] = m_coordinates[1][1] - m_coordinates[0][1];
    m_J[2][0] = m_coordinates[1][2] - m_coordinates[0][2];

    m_J[0][1] = m_coordinates[2][0] - m_coordinates[0][0];
    m_J[1][1] = m_coordinates[2][1] - m_coordinates[0][1];
    m_J[2][1] = m_coordinates[2][2] - m_coordinates[0][2];
  }
  m_sqrtDetJTJ = sqrt(fabs(determinant(transpose(m_J)*m_J)));

  m_dphi_dx.resize(m_nbNodesPerElement);
//  if (m_dimension == 2){
//    m_dphi_dx[0].resize(m_dimension);
//    m_dphi_dx[1].resize(m_dimension);
//    double dx = m_coordinates[1][0] -  m_coordinates[0][0];
//    double dy = m_coordinates[1][1] -  m_coordinates[0][1];
//    if (dx != 0.0){
//      m_dphi_dx[0][0] = -1.0/(dx);
//      m_dphi_dx[1][0] =  1.0/(dx);
//    } else {
//      m_dphi_dx[0][0] = 0.0;
//      m_dphi_dx[1][0] = 0.0;
//    }
//    if (dy != 0.0){
//      m_dphi_dx[0][1] = -1.0/dy;
//      m_dphi_dx[1][1] =  1.0/dy;
//    } else {
//      m_dphi_dx[0][1] = 0.0;
//      m_dphi_dx[1][1] = 0.0;
//    }
//  }

  /* Generalized formulation of differential geometry */
  m_pinvJ = invert(transpose(m_J)*m_J)*transpose(m_J);
  m_dphi_dx.resize(m_nbNodesPerElement);
  for (int i = 0; i < m_nbNodesPerElement; i++){
    m_dphi_dx[i] = transpose(m_pinvJ) * m_grad_phi[i];
  }
  m_abs_detJac_sumWeigths = m_sqrtDetJTJ * m_weights_sum;
}

void MasterElementBD::computeVolume(){
  if (m_dimension == 3){
    vector<double> v1 = (m_coordinates[1] - m_coordinates[0]);
    vector<double> v2 = (m_coordinates[2] - m_coordinates[0]);
    m_volume = norm(cross(v1,v2))/2;
  } else if  (m_dimension == 2){
    m_volume = norm(m_coordinates[1] - m_coordinates[0]);
  }
}

void MasterElementBD::computeNormal(){
  m_normal.resize(3);
  if (m_dimension == 3){
    /* exterior normal if convention is well-followed on geometry file */
    vector<double> v1 = m_coordinates[1] - m_coordinates[0];
    vector<double> v2 = m_coordinates[2] - m_coordinates[0];
    m_normal = cross(v1,v2);
  } else if (m_dimension == 2){
    m_normal[0] = -m_coordinates[0][1] + m_coordinates[1][1];
    m_normal[1] = -m_coordinates[1][0] + m_coordinates[0][0];
    m_normal[2] = 0.0;
  }
  m_normal = m_normal/norm(m_normal);
}

double MasterElementBD::mass(int i, int j){
  double mass = 0.0;
  for (int qp = 0; qp < par.nbQuadraturePointsBD(); qp++){
    mass = mass + phi(qp,i) * phi(qp,j) * m_weights[qp];
  }
  return mass * m_sqrtDetJTJ;
}

double MasterElementBD::phi_dphi_i_COMP_dphi_j_COMP(int i, int j, int x, int y, vector<double> u){
  double conv = 0.0;
  for (int k = 0; k < m_dimension; k++){
    for (int qp = 0; qp < par.nbQuadraturePointsBD(); qp++){
      conv = conv + u[k] * m_phi[k][qp] * m_weights[qp];
    }
  }
  return conv * m_dphi_dx[i][x] * m_dphi_dx[j][y] * m_sqrtDetJTJ;
}

double MasterElementBD::stiff(int i, int j){
//  double stiff = dot(m_pinvJ*m_grad_phi[i], m_pinvJ*m_grad_phi[j]);
  double stiff = dot(m_dphi_dx[i], m_dphi_dx[j]);
  return stiff * m_sqrtDetJTJ;
}

double MasterElementBD::phi(const int & qp_point_id, int id_shape_function){
  return m_phi[id_shape_function][qp_point_id];
}

double MasterElementBD::computeFlow(vector<double> vectorField){
  double integral = 0.0;
  for (int i = 0; i < m_nbNodesPerElement; i++){
    for (int comp = 0; comp < m_nbDofsPerNode; comp++){
      for (int qp = 0; qp < par.nbQuadraturePointsBD(); qp++){
        integral += vectorField[m_nbDofsPerNode*i+comp] * m_normal[comp] * phi(qp, i) * m_weights[qp] * fabs(m_sqrtDetJTJ);
      }
    }
  }
  return integral;
}

double MasterElementBD::dphi_i_COMP_dphi_j_COMP(int i, int j, int comp_i, int comp_j){
  return m_dphi_dx[i][comp_i] * m_dphi_dx[j][comp_j] * m_abs_detJac_sumWeigths;
}

//void MasterElementBD::computeSize(){
//  m_size = 0.0;
//  for (int i = 0; i < m_nbNodesPerElement; i++){
//    for (int j = 0; j < m_nbNodesPerElement; j++){
//      double dist = norm(m_coordinates[i] - m_coordinates [j]);
//      if (i != j && dist > m_size)
//        m_size = dist;
//    }
//  }
//}

void MasterElementBD::computeSize(){
  if (m_dimension == 2){
    m_size = norm(m_coordinates[1] - m_coordinates[0]);
  } else if (m_dimension == 3){
    double size_a = norm(m_coordinates[1] - m_coordinates[0]);
    double size_b = norm(m_coordinates[2] - m_coordinates[0]);
    double size_c = norm(m_coordinates[1] - m_coordinates[2]);
    double s = 0.5 * (size_a + size_b + size_c);

    /* diametro de circunferencia inscrita */
//    m_size = 2.0*sqrt(((s-size_a)*(s-size_b)*(s-size_c))/s);
    /* diametro de circunferencia circunscrita */
    double r_ins = sqrt(((s-size_a)*(s-size_b)*(s-size_c))/s);
    m_size = size_a*size_b*size_c/(r_ins*(size_a + size_b + size_c));
  }
}
