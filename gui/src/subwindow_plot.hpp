#ifndef SUBWINDOW_PLOT
#define SUBWINDOW_PLOT
#include "subwindowClass.hpp"

class swPlotViewer : public SubWindow {
public:
	swPlotViewer();
	void setContent() override;
};

#endif