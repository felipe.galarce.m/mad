#ifndef SUBWINDOW
#define SUBWINDOW
#include <iostream>
#include <thread>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "misc/cpp/imgui_stdlib.h"
#include "mini/ini.h"
#include "parameter_recogn.h"
//#include "subwindow.h"
#include "tools_exec.h"
#include "tools_misc.h"
#include "implot.h"
#include "mesh.hpp"
#include <math.h>


class SubWindow {
	//Pendiente
public:
	virtual void display();
	virtual void setContent();
	std::string sWindowTitle;
	std::string sWelcomeText;
	bool focus = false;
};


class swParEditor : public SubWindow {
public:
	swParEditor();
	void temp_method();
	void setContent() override;
	std::map<std::string,int> ParamManifest = GetParamManifest();;
	std::tuple< std::map<std::string, std::string>, std::map<std::string, std::string> > Aliases = GetParamAlias();
	std::map<std::string, std::string> ParamAlias = std::get<0>(Aliases);
	std::map<std::string, std::string> ToolTips = std::get<1>(Aliases);
	std::map<std::string,std::vector<std::string>> aDropdown = GetaDropdown();
	mINI::INIStructure current_par_ini;
	
	static void HelpMarker(const char* desc);
	void DisplayParam(std::string ParamKey, std::string &ParamRealValue,std::string IdInput, int ParamNumber);
	bool bReloadScene = false;
private:
	bool show_pars = false;
	bool bTryToLoad = true;
	std::map<std::string, bool> aBool;
	std::vector<std::string> secc;
	std::vector<std::string> keys;
	std::vector<std::string> pars;
	std::vector<int> param_numb;
	std::vector<int> item_current_idx{std::vector<int>(128,0)};
};

#endif