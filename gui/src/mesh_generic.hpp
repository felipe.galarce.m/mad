#ifndef MESH_CLASS_GEN
#define MESH_CLASS_GEN

#include <string>

#include "VAO.h"
#include "EBO.h"
#include "Camera.h"
//#include "Texture.h"

class MeshGen {
public:
	std::vector <Vertex> vertices;
	std::vector <GLuint> faces;
	//std::vector <Texture> textures;
	// Store VAO in public so it can be used in the Draw function
	VAO VAO;

	// Initializes the mesh
	MeshGen(std::vector <Vertex>& vertices, std::vector <GLuint>& indices, std::vector <Texture>& textures);

	// Draws the mesh
	void Draw(Shader& shader, Camera& camera);
};

#endif
