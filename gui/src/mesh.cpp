#include "mesh.hpp"

std::vector<std::string> split(std::string s){
	// Create a stringstream object with the input string
	std::stringstream ss(s);

	// Tokenize the input string by delimiter
	std::string token;
	std::vector<std::string> tokens;
    char delimiter = ' ';

	while (std::getline(ss, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}


Mesh::Mesh(std::string _filename) {
	filename = _filename;
	if (filename != " ") {
		std::string s;
		std::ifstream in;
		in.open(filename);
		if (in.fail() || in.bad() || !in.is_open()) {
			std::cout << "fail() " << in.fail() << "\n";
			std::cout << "bad() " << in.fail() << "\n";
			std::cout << "!is_open() " << in.fail() << "\n";
			std::cout << "Unable to display mesh file: " << filename << "\n";
			
		}
		int count = 0;
		while(!in.eof()) {
			getline(in, s);
			lines.push_back(s);
			if (s=="Vertices") {
				vertLine = count;
			} else if(s=="Triangles") {
				trianLine = count;
			} else if(s=="Edges") {
				edgeLine = count;
			}
			count += 1;
		}
		in.close();
		dim = 2;
		for (int i=0; i<5; i++) {
			if (lines[i] == "Dimension 3") {
				dim = 3;
				break;
			}
		}
		
		nbVerts = std::stoi(lines[vertLine+1]);
		nbFaces = std::stoi(lines[trianLine+1]);
		if (edgeLine != 0) {
			nbEdges = std::stoi(lines[edgeLine+1]);
		}

	  vertices.resize(nbVerts*3);
	  faces.resize(nbFaces*3);
	  edges.resize(nbEdges*2);
	  groupFaces.resize(nbFaces*3);
	  groupEdges.resize(nbEdges*2);
	  sizeVerts = vertices.size() * sizeof(GLfloat);
	  sizeFaces = faces.size() * sizeof(GLuint);
	  
	}
}

Mesh::~Mesh() {}

void Mesh::setVertices() {
	int current_vert = 0;
	//std::cout << "vertices vector size " << vertices.size() << "\n";
	for (int i=vertLine+2; i<vertLine+2+nbVerts; i++) {
		std::vector<std::string> current_line = split(lines[i]);
		vertices[3*current_vert] = std::stof(current_line[0]);
		vertices[3*current_vert+1] = std::stof(current_line[1]);
		vertices[3*current_vert+2] = 0.0f;
		if (dim == 3 && lines[i] != "0") {
			vertices[3*current_vert+2] = std::stof(current_line[2]);
		}
		//Color (for testing)
		//vertices[6*current_vert+3] = 0.8f;
		//vertices[6*current_vert+4] = 0.3f;
		//vertices[6*current_vert+5] = 0.02f;
		current_vert++;
	}
}

void Mesh::setFaces() {
	int current_face = 0;
	for (int i=trianLine+2; i<trianLine+2+nbFaces; i++) {
		std::vector<std::string> current_line = split(lines[i]);
		//Indices of vertices
		faces[3*current_face] = std::stoul(current_line[0])-1;
		faces[3*current_face+1] = std::stoul(current_line[1])-1;
		faces[3*current_face+2] = std::stoul(current_line[2])-1;
		
		//Saves the physical group
		if (current_line.size() == 4) {
			groupFaces[current_face] = std::stoul(current_line[3]);
		} else {
			groupFaces[current_face] = 0;
		}
		
		current_face++;
	}
}

void Mesh::setEdges() {
	if (edgeLine != 0) {
		int current_edge = 0;
		for (int i=edgeLine+2; i<edgeLine+2+nbEdges; i++) {
			std::vector<std::string> current_line = split(lines[i]);
			//Indices of vertices
			edges[2*current_edge] = std::stoul(current_line[0])-1;
			edges[2*current_edge+1] = std::stoul(current_line[1])-1;
			
			//Saves the physical group
			if (current_line.size() == 3) {
				groupEdges[current_edge] = std::stoul(current_line[2]);
			} else {
				groupEdges[current_edge] = 0;
			}
			current_edge++;
		}
	}
}

void Mesh::draw(Shader shaderProgram, std::vector<float> colors) {
	for (int offset = 0; offset < sizeof(GLuint)*3*(nbFaces); offset += sizeof(GLuint)*3) {
		//Color FACE BY FACE according to its group.
		int current_face = offset/(sizeof(GLuint)*3);
		//int current_vert = mesh.faces[3*current_face];
		GLint color_location = glGetUniformLocation(shaderProgram.ID, "my_color");
		//float currGroup = static_cast<float>(groupFaces[current_face]);
		int currGroup = groupFaces[current_face];
		std::vector<float> vcolor;
		
		//Forces a preset color. If the user changes it, shouldnt be forced...
		//vcolor = getGroupColor(currGroup);
		vcolor={colors[3*currGroup], colors[3*currGroup+1], colors[3*currGroup+2]};
		glUniform3fv(color_location, 1, &vcolor[0]);
		//Render faces ONE BY ONE.
		glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (int*)offset);
	}
	//int count = 0;
	//for (int offset = 0; offset < sizeof(GLuint)*2*(nbEdges); offset += sizeof(GLuint)*2) {
	//	//Render agrouped edges ONE BY ONE.
	//	std::cout << count; 
	//	glLineWidth(5.0);
	//	glDrawElements(GL_LINE_STRIP, 2, GL_UNSIGNED_INT, (int*)offset);
	//	count ++;
	//}
}

std::vector<int> Mesh::getUniqueGroups() {
	// Groups all the posible groups
	std::vector<int> rawgroups = groupFaces;
	if (edgeLine == 0) {
		rawgroups.insert(rawgroups.end(), groupEdges.begin(), groupEdges.end());
	}

	// Sorts all the possible groups
	std::sort(rawgroups.begin(), rawgroups.end());
	
	// Remove duplicates
	auto it = unique(rawgroups.begin(), rawgroups.end()); 
    rawgroups.erase(it, rawgroups.end());
	
	return rawgroups;
}

std::vector<float> getGroupColor(int group) {
	std::vector<float> color; //(3, 0.0);
	// Generate a unique color for each group
	std::vector<std::vector<float>> color_presets = {{1.00, 0.00, 0.00}, 
												{1.00, 0.50, 0.00},
												{1.00, 1.00, 0.00},
												{0.50, 1.00, 0.00},
												{0.00, 1.00, 0.00},
												{0.00, 1.00, 0.50},
												{0.00, 1.00, 1.00},
												{0.00, 0.50, 1.00},
												{0.00, 0.00, 1.00}, 
												{0.50, 0.00, 1.00},
												{1.00, 0.00, 1.00},
												{1.00, 0.00, 0.50}};
	if(group<=12) {
		color = color_presets[group];
	} else {
		//More than 12 groups, paint the faces black (for now)
		color = {0.00, 0.00, 0.00};
	}
	return color;
}
