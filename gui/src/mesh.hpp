#ifndef MESH_CLASS
#define MESH_CLASS

#include <algorithm> 
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "shaderClass.h"

class Mesh {
public:
	//constructor
	Mesh(std::string _filename);
	~Mesh();
	//methods
	void setVertices();
	void setFaces();
	void setEdges();
	std::vector<int> getUniqueGroups();
	void draw(Shader shaderProgram, std::vector<float> colors);
	//vars
	std::string filename;
	int nbVerts = 0;
	int nbFaces = 0;
	int nbEdges = 0;
	int dim;
	int sizeVerts;
	int sizeFaces;
	std::vector<GLfloat> vertices;
	std::vector<GLuint> faces;
	std::vector<GLuint> edges;
	std::vector<int> groupFaces;
	std::vector<int> groupEdges;
	int vertLine = 0;
	int trianLine = 0;
	int edgeLine = 0;
	
private:
	std::vector<std::string> lines;
};

std::vector<float> getGroupColor(int group);



#endif
