#include "subwindowClass.hpp"

void SubWindow::display() {
	ImGui::Begin(sWindowTitle.c_str());
	focus = ImGui::IsWindowFocused();
	ImGui::Text(sWelcomeText.c_str());
	setContent();
	ImGui::End();
}

void SubWindow::setContent() {
	std::cout << "I didnt need to see that...";
}


swParEditor::swParEditor() {
	sWindowTitle = "Par editor";
	sWelcomeText = "Welcome to Multi-physics simulAtions for engineering and Data assimilation\n(or MAD for short).\nTry loading a project by choosing its parameters file.";
	//std::cout << "swParEditor";
	//display();
}

void swParEditor::temp_method() {
	sWindowTitle = "Par editor";
	sWelcomeText = "Welcome to Multi-physics simulAtions for engineering and Data assimilation\n(or MAD for short).\nTry loading a project by choosing its parameters file.";
	std::cout << "swParEditor";
	display();
}

void swParEditor::HelpMarker(const char* desc) {
    ImGui::TextDisabled("(?)");
    if (ImGui::BeginItemTooltip())
    {
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}


void swParEditor::DisplayParam(std::string ParamKey, std::string &ParamRealValue,std::string IdInput, int ParamNumber) {
	int ParamType = GetParamType(ParamKey, ParamManifest);
	//std::cout << ParamKey;
	if (ToolTips.count(ParamKey)) {
		//Create hoverable <?> with a tooltip (if defined)
		std::string tooltip = ToolTips[ParamKey];
		HelpMarker(tooltip.c_str());
		ImGui::SameLine();
	}
	switch(ParamType) {
		case 1: //tBool
		{
			ImGui::Checkbox(IdInput.c_str(), &aBool[ParamKey]);
			pars[ParamNumber] = bts(aBool[ParamKey]);
			break;
		}
		case 20: //tInt
		{
			ImGui::Text("PLACEHOLDER");
			std::string int_id="##int"+ParamKey;
			//ImGui::InputInt(int_id.c_str(), &x)
			break;
		}
		case 30: //tFloat
		{
			ImGui::Text("PLACEHOLDER");
			break;
		}
		case 4: //tDropDown
		{
			std::string combo_id = "##combo"+ParamKey;
			//item_current_idx debe ser diferente para cada dropdown
			//static int item_current_idx = getIndex(aDropdown["solver"], ParamRealValue);
			static std::vector<int> item_current_idx(128,0);
			item_current_idx[ParamNumber] = getIndex(aDropdown[ParamKey], ParamRealValue);
			if (item_current_idx[ParamNumber]<0) {
				item_current_idx[ParamNumber]=0;
			}
			//std::cout << "\n" << ParamRealValue << ParamNumber <<item_current_idx[ParamNumber];
			std::string combo_preview_value = aDropdown[ParamKey+"_alias"][item_current_idx[ParamNumber]];
			
			if (ImGui::BeginCombo(combo_id.c_str(), combo_preview_value.c_str())) {
				for (int n = 0; n < aDropdown[ParamKey].size(); n++) {
					const bool is_selected = (item_current_idx[ParamNumber] == n);
					if (ImGui::Selectable(aDropdown[ParamKey+"_alias"][n].c_str(), is_selected))
						item_current_idx[ParamNumber] = n;

					// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
			pars[ParamNumber] = aDropdown[ParamKey][item_current_idx[ParamNumber]];
			break;
		}
		default:
			ImGui::InputText(IdInput.c_str(), &pars[ParamNumber]);
			break;
	}
}

void swParEditor::setContent() {
	static char par_name[32]="par"; //Imported filename
	static char par_name_to_export[32]="par"; //Filename to export
	//static bool show_pars = false;  //Shows the parameters only if a file is loaded
	static std::vector<int> param_numb; //(32,0);  //Number of parameters in every section
	//static bool bTryToLoad = true;
	
	ImGui::InputText("##Input", par_name, 32);
	// ENABLE LATER. Disabling this doesnot solve the problem of duplicated parameters...
	if (bTryToLoad) {
		//Loads the par into a INIStructure
		std::tuple<mINI::INIStructure, std::map<std::string, bool>> LoadedPar;
		LoadedPar = f_LoadPar(par_name);
		current_par_ini = std::get<0>(LoadedPar);
		aBool = std::get<1>(LoadedPar);
		//Sets keys, pars and secc
		//std::tuple< std::vector<std::string>,std::vector<std::string>,std::vector<std::string>,std::vector<int> > par_info;
		//par_info = GetParInfo(current_par_ini);
		//keys = std::get<0>(par_info);
		//pars = std::get<1>(par_info);
		//secc = std::get<2>(par_info);
		//param_numb = std::get<3>(par_info);
		keys.clear();
		pars.clear();
		secc.clear();
		param_numb.clear();
		tie(keys, pars, secc, param_numb) = GetParInfo(current_par_ini);
		
	
		if (keys.size()>0) {
			show_pars = true;
		}
		bTryToLoad = false;
	}
	if (keys.size()>0) {
			show_pars = true;
	}
	//Load ini if button is pressed
	if (ImGui::Button("Load par")) {
		std::cout << "ImGui::Button(Load par) - The button has been pressed...";
		bReloadScene = true;
		//Loads the par into a INIStructure
		std::cout << "\n Declaring LoadedPar \n";
		std::tuple<mINI::INIStructure, std::map<std::string, bool>> LoadedPar;
		std::cout << "\n Using f_LoadPar() \n";
		//f_LoadPar does something...
		LoadedPar = f_LoadPar(par_name);
		std::cout << "\n Unpacking LoadedPar \n";
		//THE PROBLEM OF THE DUPLICATES IS BECAUSE THE INI STRUCTURE
		//WAS NOT BEING CLEARED. THE NEXT LINE IS REALLY REALLY IMPORTANT
		current_par_ini.clear();
		current_par_ini = std::get<0>(LoadedPar);
		std::cout << "\n ini.size() = " << current_par_ini.size();
		aBool = std::get<1>(LoadedPar);
		//Sets keys, pars and secc
		//std::tuple< std::vector<std::string>,std::vector<std::string>,std::vector<std::string>,std::vector<int> > par_info;
		std::cout << "\n Lets clear keys, pars, secc and param_numb \n";
		keys.clear();
		pars.clear();
		secc.clear();
		param_numb.clear();
		std::cout << "\n secc.size (before) " << secc.size() << "\n";

		//Lets use a tie() for this...
		std::tie(keys, pars, secc, param_numb) = GetParInfo(current_par_ini);
		std::cout << "\n secc.size (after) " << secc.size()  << "\n";
		//exit(1);
		
		show_pars = true;
	}
	
	//Show the parameters after clicking the "Load par" button
	if (show_pars) {
		/*
		i -> Moves between sections (secc)
		j -> Moves between keys (keys)
		k -> Moves between values (pars)
		-- keys.size() = pars.size()
		*/
		ImGui::InputText("##Output", par_name_to_export, 32);
		if (ImGui::Button("Save par")) {
			//Creates a new structure, saving the changes
			mINI::INIStructure new_par = CreatePar(secc, keys, pars, param_numb);
			//Generate the file
			f_SavePar(par_name_to_export, new_par);
		}
		int current_par = 0;
		//std::map<std::string, std::string> ParamAlias = std::get<0>(Aliases);
		//std::map<std::string, std::string> ToolTips = std::get<1>(Aliases);
		//std::cout << "\n secc.size() " << secc.size() << "\n";
		for (int i=0; i<secc.size(); i++) {
			ImGui::SeparatorText(secc[i].c_str());
			for (int j=0; j<param_numb[i]; j++) {
				if (ParamAlias.count(keys[current_par])) {
					ImGui::Text(ParamAlias[keys[current_par]].c_str()); //Displays the alias
				} else {
					ImGui::Text(keys[current_par].c_str()); //Displays the key
				}
				//ImGui::Text(keys[current_par].c_str()); //Displays the key
				ImGui::SameLine();
				std::string id_input = "##" + std::to_string(current_par);
				//USES DisplayParam from HERE CONSIDERING THE TYPES
				DisplayParam(keys[current_par], pars[current_par], id_input, current_par);
				current_par++;
			}
		}
		ImGui::SeparatorText("Execution");
		ImGui::Text("Executable name"); ImGui::SameLine(); HelpMarker("The compiled .exe file in the directory"); ImGui::SameLine();
		static std::string exe_name = GetExeName();
		static int np = std::thread::hardware_concurrency() - 1;
		if(np<1){np = 1;}
		ImGui::InputText("##ExeName", &exe_name);
		ImGui::Text("Number of CPU cores"); ImGui::SameLine(); HelpMarker("Number of cores where the simulation will run. By default uses almost all the cores that can detect, leaving one free");
		ImGui::SameLine();
		ImGui::InputInt("##NP", &np);
		if (ImGui::Button("Run simulation")) {
			//Runs MAD
			mINI::INIStructure new_par = CreatePar(secc, keys, pars, param_numb);
			f_RunMad(np, exe_name, new_par);
		}
		if (ImGui::Button("See results in PARAVIEW")) {
			//NEED A FIX. "paraview" can always run with this command???
			f_RunParaview("paraview", current_par_ini["IO"]["dirResults"], current_par_ini["IO"]["patientName"]);
		}
	}
}