#include "subwindow_render.hpp"

swRenderTools::swRenderTools(std::vector<int> _aiGroups) {
	sWindowTitle = "Render";
	sWelcomeText = " ";
	aiGroups = _aiGroups;
	//display();
}

swRenderTools::~swRenderTools() {}

void swRenderTools::setContent() {
	ImGui::SeparatorText("Physical groups");
	//color is declared in the header now
	//static std::vector<float> color(96,0.0);  // 96 -> Up to 32 colors
	for (int i=0; i<aiGroups.size(); i++) {
		if(aiGroups[i] != 0){
			std::vector<float> vcol = getGroupColor(aiGroups[i]);
			//color[3*i] = vcol[0];
			//color[3*i+1] = vcol[1];
			//color[3*i+2] = vcol[2];
			color.push_back(vcol[0]);
			color.push_back(vcol[1]);
			color.push_back(vcol[2]);
			ImGui::Text("");
			ImGui::SameLine();
			std::string groupID = "Group " + std::to_string(aiGroups[i]);
			ImGui::ColorEdit3(groupID.c_str(), &color[3*i]);
		}
	}
	
	ImGui::SeparatorText("Geometry visualization");
	//static bool WireframeMode = bWireframe;
	ImGui::Checkbox("wireMode", &bWireframe);
	if (bWireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	static float back_col[3] = { 1.0f, 0.0f, 0.2f };
	std::string backID= "Background color";
	ImGui::ColorEdit3(backID.c_str(), back_col);
}