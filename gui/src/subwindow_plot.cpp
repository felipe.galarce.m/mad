#include "subwindow_plot.hpp"

swPlotViewer::swPlotViewer() {
	sWindowTitle = "Plot viewer";
	sWelcomeText = "[PLACEHOLDER TEXT]";
}

void swPlotViewer::setContent() {
	ImPlot::CreateContext();
	int const size = 1000;
	std::vector<double> o_if;
	static double xs1[size], ys1[size];
    for (int i = 0; i < size; ++i) {
        xs1[i] = i; //* 0.001f;
        //ys1[i] = 0.5f + 0.5f * sinf(50 * (xs1[i] + (float)ImGui::GetTime() / 10));
		//ys1[i] = f_GetDoubleFromBin("feedback.bin", 1);
		ys1[i] = 0.5;
    }
    
    if (ImPlot::BeginPlot("Imcompressibility failure")) {
        ImPlot::SetupAxes("t","I.F.");
		ImPlot::SetupAxesLimits(0,10,-0.001,0.001);
        ImPlot::PlotLine("Imcomp. Failure", xs1, ys1, size);
        ImPlot::EndPlot();
		//std::cout << "\n MMM current ys1: MMM" << ys1[0] << "\n";
    }
	ImPlot::DestroyContext();
	ImGui::End();
}