#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <cstddef>
#include <iostream>
#include <string>
#include <map>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "mini/ini.h"
//#include "subwindow.h"
#include "subwindowClass.hpp"
#include "subwindow_render.hpp"
#include "subwindow_plot.hpp"
#include "mm_file.hpp"
#include "shaderClass.h"
#include "VAO.h"
#include "VBO.h"
#include "EBO.h"
#include "mesh.hpp"
#include "camera.hpp"
#include <cmath>
#include <regex>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

static void glfw_error_callback(int error, const char* description) {
    fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

std::string get_env_var(std::string const & key ){
  char * val;                                                                        
  val = std::getenv( key.c_str() );                                                       
  std::string retval = "";                                                           
  if (val != NULL) {                                                                 
    retval = val;                                                                    
  }                                                                                  
  return retval;                                                                        
}   

int main()  {
	//Theres no elegant way to do this by now...
	// first, create a file instance
	mINI::INIFile file1("par");
	// next, create a structure that will hold data
	mINI::INIStructure ini1;
	// now we can read the file
	file1.read(ini1);
	// read a value
	std::string mesh_name = ini1["IO"]["geometryData"];
	if(mesh_name == ""){mesh_name=" ";}
	
	Mesh mesh(mesh_name);
	mesh.setVertices();
	mesh.setEdges();
	mesh.setFaces();

	//Create a binary file for feedback
	mm::file_sink<double> fout("feedback.bin", 5);
	
	//Some variables.
	const unsigned int width = 800;
	const unsigned int height = 800;
	const char *window_title="MAD";

	//Initialize glfw
    glfwInit();



    // Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

	//Window creation
	GLFWwindow* window = glfwCreateWindow(800, 800, window_title, NULL, NULL);

	//Error check if the window is not created
	if (window == NULL) {
		glfwTerminate();
		std::cout << "Failed to create window";
		return -1;
	}
	glfwMakeContextCurrent(window);

	//Load GLAD to configure opengl
	gladLoadGL();
	// Specify the viewport of OpenGL in the Window
	glViewport(0, 0, 800, 800);

	// Generates Shader object using shaders defualt.vert and default.frag
	std::string shaderFolder = get_env_var("MAD_ROOT") + "/gui/";
	Shader shaderProgram((shaderFolder + "/default.vert").c_str(), (shaderFolder + "/default.frag").c_str());

	// Generates Vertex Array Object and binds it
	VAO VAO1;
	
	//glDisable(GL_CULL_FACE); //Disable culling
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //Wireframe mode -> GL_LINE ; Solid mode -> GL_FILL
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //Wireframe mode -> GL_LINE ; Solid mode -> GL_FILL
	
	VAO1.Bind();

	VBO VBO1(&mesh.vertices[0], mesh.sizeVerts);
	EBO EBO1(&mesh.faces[0], mesh.sizeFaces); //I should pass the edges to the EBO?

	// Links VBO to VAO
	VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, 3*sizeof(float), (void*)0);
	VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, 3*sizeof(float), (void*)(3*sizeof(float)));
	// Unbind all to prevent accidentally modifying them
	VAO1.Unbind();
	VBO1.Unbind();
	EBO1.Unbind();

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
#ifdef __EMSCRIPTEN__
    ImGui_ImplGlfw_InstallEmscriptenCanvasResizeCallback("#canvas");
#endif
    ImGui_ImplOpenGL3_Init(glsl_version);
    bool td_sim = true;
    bool another_bool = false;

    // Main loop
#ifdef __EMSCRIPTEN__
    // For an Emscripten build we are disabling file-system access, so let's not attempt to do a fopen() of the imgui.ini file.
    // You may manually call LoadIniSettingsFromMemory() to load settings from your own storage.
    io.IniFilename = nullptr;
    EMSCRIPTEN_MAINLOOP_BEGIN
#else
	// Enables the Depth Buffer
	glEnable(GL_DEPTH_TEST);
	
	// Creates camera object
	Camera camera(width, height, glm::vec3(0.0f, 0.0f, 2.0f));
	

	while (!glfwWindowShouldClose(window)) {
#endif
		//Take care of all GLFW events
		glfwPollEvents();
		


		// Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        //HERE CALLS SUBWINDOW CLASSES
		static swParEditor sw_par_ed;
		sw_par_ed.display();
		static swRenderTools sw_render_tools(mesh.getUniqueGroups());
		sw_render_tools.display();
		sw_render_tools.aiGroups = mesh.getUniqueGroups();
		static swPlotViewer sw_plot_viewer;
		//sw_plot_viewer.display();
		
		if (sw_par_ed.bReloadScene) {
			(&mesh)->~Mesh();
			std::string mesh_name = sw_par_ed.current_par_ini["IO"]["geometryData"];

			new (&mesh) Mesh(mesh_name);
			mesh.setVertices();
			mesh.setFaces();
			mesh.setEdges();

			(&VAO1)->~VAO();
			new (&VAO1) VAO();
			VAO1.Bind();

			(&VBO1)->~VBO();
			new (&VBO1) VBO(&mesh.vertices[0], mesh.sizeVerts);

			(&EBO1)->~EBO();
			new (&EBO1) EBO(&mesh.faces[0], mesh.sizeFaces);

			VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, 3*sizeof(float), (void*)0);
			VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, 3*sizeof(float), (void*)(3*sizeof(float)));

			VAO1.Unbind();
			VBO1.Unbind();
			EBO1.Unbind();
			
			(&sw_render_tools)->~swRenderTools();
			new (&sw_render_tools) swRenderTools(mesh.getUniqueGroups());
			
			sw_par_ed.bReloadScene = false;
		}
		
		
		//Enable the inputs for the mesh viewer only if
		//the focus is not in a subwindow (par editor, plots, render, etc)
		//camera.enableInputs = !sw_render_tools.focus;
		camera.enableInputs = !sw_par_ed.focus && !sw_render_tools.focus;
		
		// Rendering
        ImGui::Render();
        //int display_w, display_h;
        //glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, 800, 800);
        // Specify the color of the background
		glClearColor(0.07f, 0.13f, 0.17f, 1.0f);
		// Clean the back buffer and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Clean the back buffer and assign the new color to it
	    glClear(GL_COLOR_BUFFER_BIT);
		// Tell OpenGL which Shader Program we want to use
		shaderProgram.Activate();
		
		// Handles camera inputs
		camera.Inputs(window);
		// Updates and exports the camera matrix to the Vertex Shader
		camera.Matrix(45.0f, 0.1f, 100.0f, shaderProgram, "camMatrix");
		
		// Bind the VAO so OpenGL knows to use it
		VAO1.Bind();
		
		mesh.draw(shaderProgram, sw_render_tools.color);
		
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		
		glfwSwapBuffers(window);
	}
#ifdef __EMSCRIPTEN__
    EMSCRIPTEN_MAINLOOP_END;
#endif

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
	// Delete all the objects we've created
	VAO1.Delete();
	VBO1.Delete();
	EBO1.Delete();
	shaderProgram.Delete();
	// Delete window before ending the program
	
	
	glfwDestroyWindow(window);

	glfwTerminate();
	
	std::remove("feedback.bin");
	std::remove(".par");
    return 0;
}
