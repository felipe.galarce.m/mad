#ifndef SUBWINDOW_RENDER
#define SUBWINDOW_RENDER

#include "subwindowClass.hpp"

class swRenderTools : public SubWindow {
public:
	swRenderTools(std::vector<int> aiGroups);
	~swRenderTools();
	void setContent() override;
	bool bReloadMesh = false;
	bool bWireframe = false;
	std::vector<int> aiGroups;
	std::vector<float> color; //(96,0.0);
};

#endif