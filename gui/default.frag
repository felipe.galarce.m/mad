#version 330 core
// Outputs colors in RGBA
out vec4 FragColor;
uniform vec3 my_color;


void main()
{
	//FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	FragColor = vec4(my_color, 1.0f);
}

