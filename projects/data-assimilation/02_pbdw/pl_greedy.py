import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

# load regular
r2 = np.loadtxt('./phase2/error.txt', skiprows=0);
r3 = np.loadtxt('./phase3/error.txt', skiprows=0);
r4 = np.loadtxt('./phase4/error.txt', skiprows=0);

# load greedy
g2 = np.loadtxt('../04_pbdw_sensorPlacement/phase2/error.txt', skiprows=0);
g3 = np.loadtxt('../04_pbdw_sensorPlacement/phase3/error.txt', skiprows=0);
g4 = np.loadtxt('../04_pbdw_sensorPlacement/phase4/error.txt', skiprows=0);

# full measures
full = np.loadtxt('./phase/error.txt', skiprows=0);

pl.style.use(['science', 'nature'])
error = np.zeros(4)
errorGreedy = np.zeros(4)

error[0] = 1/float(len(r2))*np.sum(r2)
error[1] = 1/float(len(r3))*np.sum(r3)
error[2] = 1/float(len(r4))*np.sum(r4)
error[3] = 1/float(len(r2))*np.sum(full)

errorGreedy[0] = 1/float(len(r2))*np.sum(g2)
errorGreedy[1] = 1/float(len(r3))*np.sum(g3)
errorGreedy[2] = 1/float(len(r4))*np.sum(g4)
errorGreedy[3] = 1/float(len(r2))*np.sum(full)

m = np.array([56, 162, 266, 352])
pl.plot(m, error, "b", label="Regular")
pl.plot(m, errorGreedy, "r", label="Greedy")
pl.plot(m, error, "b*")
pl.plot(m, errorGreedy, "r*")

pl.xlabel("m")
pl.ylabel("Error")
pl.yscale("log")
pl.legend(loc='best')
#pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
