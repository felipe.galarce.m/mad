import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

# load regular
r2 = np.loadtxt('./phase2/error.txt', skiprows=0);
r3 = np.loadtxt('./phase3/error.txt', skiprows=0);
r4 = np.loadtxt('./phase4/error.txt', skiprows=0);

# load greedy
g2 = np.loadtxt('../04_pbdw_sensorPlacement/phase2/error.txt', skiprows=0);
g3 = np.loadtxt('../04_pbdw_sensorPlacement/phase3/error.txt', skiprows=0);
g4 = np.loadtxt('../04_pbdw_sensorPlacement/phase4/error.txt', skiprows=0);

# full measures
full = np.loadtxt('./phase/error.txt', skiprows=0);

pl.style.use(['science', 'nature'])
time = np.loadtxt('/Users/fgalarce/home/01_research/phase_change/manifold/timeSet.txt')
pl.plot(time/3600, r3, label='Regular')
pl.plot(time/3600, g3, label='Greedy')

pl.xlabel("Time [hrs]")
pl.ylabel("Error")
pl.yscale("log")
pl.legend(loc='best')
#pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
