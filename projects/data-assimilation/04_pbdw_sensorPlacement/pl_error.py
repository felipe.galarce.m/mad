import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

n=111
pod = np.loadtxt("/Users/fgalarce/home/01_research/phase_change/pod/eigenValues_re.txt")[0:n]
beta = np.loadtxt('./phase/svG.txt', skiprows=0)[0:n];
error = np.zeros(n)
for i in range(n):
  error[i] = np.sqrt(np.sum(pod[i:]) / np.sum(pod))

print(np.argmin(error/beta)+1)

#pl.style.use(['science', 'nature'])
#pl.style.use('dark_background')
pl.style.use(['science', 'nature'])
#pl.plot(np.arange(1,n+1), error/beta, label='$\\beta(V_n, W_m)^{-1} dist(u,V_n)$')
pl.plot(np.arange(1,n+1), error/beta)
#pl.plot(np.arange(1,n+1), beta, label='$\\beta(V_n, W_m)$')
#pl.plot(np.arange(1,n+1), error, label='$dist(u,V_n)$')
#pl.plot(np.arange(1,n+1), pod)
#pl.xlabel("$n$",fontsize=14)
pl.yscale("log")
pl.legend(loc='best')
#pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
