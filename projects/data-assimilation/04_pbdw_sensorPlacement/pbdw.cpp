/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017 - 2024,
    
     Felipe Galarce at WIAS

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MDMA. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

int main(int argc, char *argv[]){

  Parameters par = MADinitialize(argc, argv);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  PetscErrorCode code;

  /* Initialize MAD objects */
  IO io;
  io.initialize(par);

  Geometry geo;
  geo.initialize(par, io);

  Boundary bd;
  bd.initialize(par, geo);

  InnerProduct ip;
  ip.initialize(par, geo, bd);

  LinearAlgebra la;
  la.initialize(par, ip);

  Calculus calculus;
  calculus.initialize(par, geo, bd);

  ROM rom;
  rom.initialize(par, geo, io, calculus);

  Observer observer;
  observer.initialize(par, geo, ip, io, calculus, la, bd);

  /* Greedy sensor placement and cross-Gramian assembly */
  Mat G = observer.optimal_placement(266, rom.basis());

  ofstream errorFile(par.dirResults() + "/error.txt");

  vector<double> svG = getSingularValues(G, par.nbModes());
  exportData(par.dirResults() + "./svG.txt", svG);

  for (int t = par.start(); t < par.end(); t++){

    /* build coordinates from real data */
    observer.buildMeasures(t);

    /* print synthetic observation in ensight */
    vector<Vec> obs = observer.observations(t);
    for (int i = 0; i < par.measureIt().size(); i++){
      io.writeState(obs[i], "measures_" + par.variableName()[i], t);
    }

    MADprint("Computing least-squares\n");
    Vec u = vec(par.nbVariables()*io.nbVertices()); /* reconstruction */
    Vec c = vec(par.nbModes());
    Mat N = mat(par.nbModes(), par.nbModes());
    code = MatMatMult(transpose(G), G, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &N); CHKERR(code);
    Vec rhs = vec(par.nbModes());
    int filas, colum;
    code = MatMult(transpose(G), observer.measures(), rhs); CHKERR(code);
    KSP ksp;
    configureKSP(ksp, par);
    KSPSetOperators(ksp, N, N);
    KSPSolve(ksp, rhs, c);

    /* recover reconstruction from coordinates in ROM */
    VecZeroEntries(u);
    vector<double> c_stl = stl(c);
    for (int i = 0; i < par.nbModes(); i++){
      code = VecAXPY(u, c_stl[i], rom.basis(i)); CHKERR(code);
    }
    for (int i = 0; i < par.nbVariables(); i++){
      io.writeState(calculus.split(u)[i], par.variableName()[i] + "_star", t);
    }
    io.writeState(observer.u_gt, t);
    Vec error = zeros(geo.nbVertices);
    VecAXPY(error, 1.0, observer.u_gt);
    VecAXPY(error, -1.0, u);
    double err;
    VecDot(error, error, &err);
    err = sqrt(err)/norm(observer.u_gt);
    cout << err << endl;
    errorFile << err << endl;
  }

  MADfinalize(par);
}
