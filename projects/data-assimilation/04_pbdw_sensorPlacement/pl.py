import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

p = np.loadtxt('./phase_inrange/error.txt', skiprows=0);
q = np.loadtxt('./phase_outofrange/error.txt', skiprows=0);

pl.style.use(['science', 'nature'])
pl.plot(p, label="in-range")
pl.plot(q, label="out-of-range")
pl.xlabel("Time iteration")
pl.ylabel("$\\lVert T - T^* \\rVert$")
pl.legend(loc='best')
pl.tight_layout()
pl.grid("on")
pl.show()

