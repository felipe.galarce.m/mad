/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2024,
    
     Mauricio Portilla & Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

class DMD {
public:
	DMD(Parameters parameters, IO _io, InnerProduct _ip) {
		par = parameters;
		io = _io;
		ip = _ip;
		/* Computing number of snaps */
		jump = par.jump();
		nbSnapshots = 0;
		bool static_problem = false;
		if (static_problem){
			nbSnapshots = par.nbSimulations();
		} else {
			for (int simId = 0; simId < par.nbSimulations(); simId++){
				if (par.snaps_per_sim() == -1){
					nbSnapshots = nbSnapshots + floor(stoi(getParameter("nbIterations", par.maniFolder() + "sim" + wildcard(simId) + "/parameters"))/jump) - par.start();
				} else {
					nbSnapshots = nbSnapshots + par.snaps_per_sim();
				}
			}
		}
		nbDofs = 0;
		for (int i = 0; i < par.nbVariables(); i++){
			nbDofs = nbDofs + io.nbVertices()*par.nbDofsPerNode()[i];
		}
		X0 = mat(nbDofs, nbSnapshots-1, "dense");
		X1 = mat(nbDofs, nbSnapshots-1, "dense");
    V.resize(nbSnapshots-1);
    Phi.resize(par.nbModes());
    W.resize(par.nbModes());
    W_im.resize(par.nbModes());
    Psi.resize(par.nbModes());
		vec(sigma, par.nbModes());
		vec(sigma_inv, par.nbModes());
	}
	
	void loadSnapshots() {
		//Load snapshots into X0 and X1
		int m,n;
		code = MatGetOwnershipRange(X0, &m, &n); CHKERR(code);
		int nbDofsLocal = n - m;
		MADprint("POD: Assembling matrix with # snapshots : ", nbSnapshots); 
		int iterSnap = 0;
		for (int simId = 0; simId < par.nbSimulations(); simId++){
			int nbSnaps;
			if (static_problem){
				nbSnaps = 1;
			} else {
				if (par.snaps_per_sim() == -1){
					nbSnaps = floor(stoi(getParameter("nbIterations", par.maniFolder() + "sim" + wildcard(simId) + "/parameters")));
				} else {
					nbSnaps = par.snaps_per_sim() + par.start();
				}
			}
			for (int i = par.start(); i < nbSnaps; i = i + jump){ //nbSnaps=310
				vec(V[iterSnap], nbDofs);
				vector<string> to_load(par.nbVariables());
				for (int var = 0; var < par.nbVariables(); var++){
					string end_str;
					if (par.nbDofsPerNode()[var] == 1){
						to_load[var] = par.maniFolder() + "sim" + wildcard(simId) + "/" + par.variableName()[var] + "." + wildcard(i) + ".scl";
						if(i == par.start()){
							std::string current_file = par.maniFolder() + "sim" + wildcard(simId) + "/" + par.variableName()[var] + "." + wildcard(i) + ".scl";
							std::string command = "cp " +  current_file + " " + par.dirResults() + par.variableName()[var] + "t0.scl";
							std::system(command.c_str());
						}
					} else {
						to_load[var] = par.maniFolder() + "sim" + wildcard(simId) + "/" + par.variableName()[var] + "." + wildcard(i) + ".vct"; 
					}
				}
				io.loadVector(V[iterSnap], to_load); CHKERR(code);
				code = VecAssemblyBegin(V[iterSnap]); CHKERR(code);
				code = VecAssemblyEnd(V[iterSnap]); CHKERR(code);
				double snapshot[nbDofsLocal];
				VecGetValues(V[iterSnap], nbDofsLocal, &range(m, n)[0], snapshot);
        if (i < nbSnaps-1){
				  code = MatSetValues(X0, nbDofsLocal, &range(m, n)[0], 1, &iterSnap, snapshot, INSERT_VALUES); CHKERR(code);
        }
        if (i > 0){
          int iterSnapShift = iterSnap - 1;
					MatSetValues(X1, nbDofsLocal, &range(m, n)[0], 1, &(iterSnapShift), snapshot, INSERT_VALUES); CHKERR(code);
        }
				iterSnap = iterSnap + 1;
			}
			
		}
		code = MatAssemblyBegin(X0, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyEnd(X0, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyBegin(X1, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyEnd(X1, MAT_FINAL_ASSEMBLY);
		MADprint("POD: Total number of snapshots: ", iterSnap);
		double normSnapshots;
		code = MatNorm(X0, NORM_FROBENIUS, &normSnapshots);
    cout << "X0 norm: " << normSnapshots << endl;
		code = MatNorm(X1, NORM_FROBENIUS, &normSnapshots);
    cout << "X1 norm: " << normSnapshots << endl;
	}
	
	void computePOD() {
		MADprint("POD: Creating POD basis.");
		MADprint("POD: assembling X0^T M X0");
		Mat covMatrix = mat(nbSnapshots-1, nbSnapshots-1, "dense");
		int cov_m, cov_n;
		code = MatGetOwnershipRange(covMatrix, &cov_m, &cov_n);
		for (int i = 0; i < nbSnapshots-1; i++){
			for (int j = i; j < nbSnapshots-1; j++){
				double iprod = ip(V[i], V[j]);
				if (i >= cov_m && i < cov_n){
					if(i == j){
						iprod = iprod / 2.0; // this is necessary because the loop pass by here two times
					}
					code = MatSetValue(covMatrix, i, j, iprod, INSERT_VALUES);
					cout << "     row-col: " << scientific << i + 1 << " - " << j + 1 << " / " << nbSnapshots << ". ip = " << iprod << "   \r";
				}
			}
		}
		MADprint("\n");
		code = MatAssemblyBegin(covMatrix, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyEnd(covMatrix, MAT_FINAL_ASSEMBLY);

		/* Only upper triangular matrix was assembled, so C = 0.5 ( C + C^T) */
		code = MatAXPY(covMatrix, 1.0, transpose(covMatrix), DIFFERENT_NONZERO_PATTERN);
 
		double normCov;
		code = MatNorm(covMatrix, NORM_FROBENIUS, &normCov);
		MADprint("POD: norm covariance matrix: ", normCov);

		/* Compute eigen-vectors */
		vector<double> eValues, eValues_i;
		eigen(covMatrix, par.nbModes(), eValues, eValues_i, W, W_im);

		for (int i=0; i<par.nbModes(); i++){
			double singValue = sqrt(eValues[i]);
			code = VecSetValue(sigma, i, singValue, INSERT_VALUES); CHKERR(code);
			code = VecSetValue(sigma_inv, i, 1.0/singValue, INSERT_VALUES); CHKERR(code);

			/* Computes the modes */
			vec(Phi[i], nbDofs);
			code = MatMult(X0, W[i], Phi[i]); CHKERR(code);
			code = VecScale(Phi[i], 1.0/singValue);  CHKERR(code);
		}
		code = VecAssemblyBegin(sigma); CHKERR(code);
		code = VecAssemblyEnd(sigma); CHKERR(code);
		code = VecAssemblyBegin(sigma_inv); CHKERR(code);
		code = VecAssemblyEnd(sigma_inv); CHKERR(code);

		exportData(par.dirResults() + "./eigenValues.txt", eValues);
		MatDestroy(&covMatrix);
	}
	
	void computeA() {
		Phimat = buildProjector(Phi); cout << "ComputeA...\n";
		Matrix Smat_exp(sigma_inv);
		Matrix Wmat_exp(buildProjector(W));
		Matrix X1_mat(X1);
		Matrix phitrans(transpose(Phimat));
		Ap = phitrans * X1_mat * Wmat_exp * Smat_exp;
	}

	void solveDMD() {
		vector<double> eValues, eValues_i;
		eValues.resize(par.nbModes());
		eValues_i.resize(par.nbModes());
		vector<Vec> V_re, V_im;
		eigen(Ap.petsc(), par.nbModes(), eValues, eValues_i, V_re, V_im, "nonHermitian");
		
		vector<string> modeNames(par.nbVariables());
		for (int i = 0; i < par.nbVariables(); i++){
			modeNames[i] = par.variableName()[i] + "_mode";
		}
		
		for (int i = 0; i < par.nbModes(); i++){
			/* Compute mode */
			Vec mode = vec(nbDofs);
			code = MatMult(Phimat, V_re[i], mode); CHKERR(code);
			io.writeState(mode, i, modeNames);
			VecDestroy(&mode);
			VecDestroy(&V_re[i]);
			VecDestroy(&V_im[i]);
		}
		
		exportData(par.dirResults() + "/LambdaValues_re.txt", eValues);
		exportData(par.dirResults() + "/LambdaValues_im.txt", eValues_i);
	}
	
	int nbSnapshots;
	Mat X0; //Snapshots matrix 0 -> nbSnapshots-1
	Mat X1; //Snapshots matrix 1 -> nbSnapshots
	Mat Phimat;
	vector<Vec> Phi; //POD modes of X0
	vector<Vec> V; //???
	vector<Vec> W; //Eigenvectors of POD
	vector<Vec> W_im; 
	vector<Vec> Psi;
	Vec sigma; //Eigen values of POD
	Vec sigma_inv;
  Matrix Ap;
	PetscErrorCode code;
	bool static_problem = false;
	
private:
	Parameters par;
	IO io;
	InnerProduct ip;
	int nbDofs;
	int jump;
};
