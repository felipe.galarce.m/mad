/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2025,
    
     Mauricio Portilla & Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <pod.hpp>

int main(int argc, char *argv[], char *envp[]){

  /* Parse input data */
  Parameters par = MADinitialize(argc, argv); 

  PetscErrorCode code;

  /* Initialize MDMA */
  IO io;
  io.initialize(par);

  Geometry geo;
  geo.initialize(par, io);

  Boundary bd;
  bd.initialize(par, geo);

  InnerProduct ip; 
  ip.initialize(par, geo, bd);

  POD pod(par, io, ip);
  pod.loadSnapshots();
  pod.computePOD();

  pod.printEigenValues();
  exportData(par.dirResults() + "/eigenValues_re.txt", pod.eValues);
  exportData(par.dirResults() + "/eigenValues_im.txt", pod.eValues_i);

  vector<string> modeNames(par.nbVariables());
  for (int i = 0; i < par.nbVariables(); i++){
    modeNames[i] = par.variableName()[i] + "_mode";
  }
  for (int i = 0; i < par.nbModes(); i++){
    io.writeState(pod.U[i], i, modeNames);
  } 

  MADfinalize(par);
}
