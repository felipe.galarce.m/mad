/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2024,
    
     Mauricio Portilla & Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

class POD {
public:
	POD(Parameters parameters, IO _io, InnerProduct _ip) {
		par = parameters;
		io = _io;
		ip = _ip;
		/* Computing number of snaps */
		jump = par.jump();
		nbSnapshots = 0;
		bool static_problem = false;
		if (static_problem){
			nbSnapshots = par.nbSimulations();
		} else {
			for (int simId = 0; simId < par.nbSimulations(); simId++){
				if (par.snaps_per_sim() == -1){
					nbSnapshots = nbSnapshots + floor(stoi(getParameter("nbIterations", par.maniFolder() + "sim" + wildcard(simId) + "/parameters"))/jump) - par.start();
				} else {
					nbSnapshots = nbSnapshots + par.snaps_per_sim();
				}
			}
		}
		nbDofs = 0;
		for (int i = 0; i < par.nbVariables(); i++){
			nbDofs = nbDofs + io.nbVertices()*par.nbDofsPerNode()[i];
		}
		A = mat(nbDofs, nbSnapshots, "dense");
    U.resize(par.nbModes());
    V.resize(nbSnapshots);
    V_re.resize(par.nbModes());
    V_im.resize(par.nbModes());
	}
	
	void loadSnapshots() {
		int m,n;
		code = MatGetOwnershipRange(A, &m, &n); CHKERR(code);
		int nbDofsLocal = n - m;
		MADprint("POD: Assembling matrix with # snapshots : ", nbSnapshots); 
		int iterSnap = 0;
		for (int simId = 0; simId < par.nbSimulations(); simId++){
			int nbSnaps;
			if (static_problem){
				nbSnaps = 1;
			} else {
				if (par.snaps_per_sim() == -1){
					nbSnaps = floor(stoi(getParameter("nbIterations", par.maniFolder() + "sim" + wildcard(simId) + "/parameters")));
				} else {
					nbSnaps = par.snaps_per_sim() + par.start();
				}
			} 
			for (int i = par.start(); i < nbSnaps; i = i + jump){ //nbSnaps=310
				vec(V[iterSnap], nbDofs);
				vector<string> to_load(par.nbVariables());
				for (int var = 0; var < par.nbVariables(); var++){
					string end_str;
					if (par.nbDofsPerNode()[var] == 1){
						to_load[var] = par.maniFolder() + "sim" + wildcard(simId) + "/" + par.variableName()[var] + "." + wildcard(i) + ".scl"; 
					} else {
						to_load[var] = par.maniFolder() + "sim" + wildcard(simId) + "/" + par.variableName()[var] + "." + wildcard(i) + ".vct"; 
					}
				}
				io.loadVector(V[iterSnap], to_load); CHKERR(code);
				code = VecAssemblyBegin(V[iterSnap]); CHKERR(code);
				code = VecAssemblyEnd(V[iterSnap]); CHKERR(code);
				double snapshot[nbDofsLocal];
				VecGetValues(V[iterSnap], nbDofsLocal, &range(m, n)[0], snapshot);
				code = MatSetValues(A, nbDofsLocal, &range(m, n)[0], 1, &iterSnap, snapshot, INSERT_VALUES); CHKERR(code);
				iterSnap = iterSnap + 1;
			}
		}
		code = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
		MADprint("POD: Total number of snapshots: ", iterSnap);
		double normSnapshots;
		code = MatNorm(A, NORM_FROBENIUS, &normSnapshots);
    MADprint("POD: Snapshots matrix norm: ", normSnapshots);
	}
	
	void computePOD(){
		MADprint("POD: Creating POD basis.");
		MADprint("POD: assembling A^T M A");
		Mat covMatrix = mat(nbSnapshots, nbSnapshots, "dense");
		int cov_m, cov_n;
		code = MatGetOwnershipRange(covMatrix, &cov_m, &cov_n);
		for (int i = 0; i < nbSnapshots; i++){
			for (int j = i; j < nbSnapshots; j++){
//				double iprod = ip(V[i], V[j]);
				double iprod;
        VecDot(V[i], V[j], &iprod);
				if (i >= cov_m && i < cov_n){
					if(i == j){
						iprod = iprod / 2.0; // this is necessary because the loop pass by here two times
					}
					code = MatSetValue(covMatrix, i, j, iprod, INSERT_VALUES);
					cout << "     row-col: " << scientific << i + 1 << " - " << j + 1 << " / " << nbSnapshots << ". ip = " << iprod << "   \r";
				}
			}
		}
		MADprint("\n");
		code = MatAssemblyBegin(covMatrix, MAT_FINAL_ASSEMBLY);
		code = MatAssemblyEnd(covMatrix, MAT_FINAL_ASSEMBLY);

		/* Only upper triangular matrix was assembled, so C = 0.5 ( C + C^T) */
		code = MatAXPY(covMatrix, 1.0, transpose(covMatrix), DIFFERENT_NONZERO_PATTERN);
 
		double normCov;
		code = MatNorm(covMatrix, NORM_FROBENIUS, &normCov);
		MADprint("POD: norm covariance matrix: ", normCov);

		/* Compute eigen-vectors */
    eigen(covMatrix, par.nbModes(), eValues, eValues_i, V_re, V_im);

		for (int i=0; i<par.nbModes(); i++){
			/* Computes the modes */
			vec(U[i], nbDofs);
			code = MatMult(A, V_re[i], U[i]); CHKERR(code);
			code = VecScale(U[i], 1.0/sqrt(eValues[i]));  CHKERR(code);
		}
		MatDestroy(&covMatrix);  
	}

public:	
	vector<Vec> U; 
  vector<double> eValues, eValues_i;

  void printEigenValues(){
    for (int i = 0; i < par.nbModes(); i++){
      cout << scientific << eValues[i] << " " << eValues_i[i] << "i" << endl;
    }
  }
	
private:
	int nbSnapshots;
	Mat A; 
	vector<Vec> V_re, V_im, V; 
	PetscErrorCode code;
	bool static_problem = false;
	Parameters par;
	IO io;
	InnerProduct ip;
	int nbDofs;
	int jump;
};
