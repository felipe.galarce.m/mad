#include <mad.hpp>

int main(int argc, char *argv[]){

	Parameters par = MADinitialize(argc, argv);

	/* Initialize MAD objects */
	IO io;
	io.initialize(par);

  vector<double> pressure = importdata1D(par.dirSyntheticField());

  io.writeState(pressure, "pressure");

	MADfinalize(par);
}
