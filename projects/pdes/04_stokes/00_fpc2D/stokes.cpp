/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2021-2024,
    
     Felipe Galarce at INRIA/WIAS/PUCV 

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

vector<double> inlet(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 1.0);
  return bc;
}

vector<double> noslip(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  return bc;
}


int main(int argc, char *argv[]){

  Parameters par = MADinitialize(argc, argv);

  /* Initialize MAD objects */
  IO io;
  io.initialize(par);
    
  Geometry geo;
  geo.initialize(par, io);

  Stokes stokes;
  stokes.initialize(par, geo);

  stokes.assembleLHS();

  /* Set boundary conditions */
  stokes.bd.Dirichlet(par.inlet(), inlet, 0);
  stokes.bd.Dirichlet(par.inlet(), noslip, 1);
  for (int i : par.walls()){
    for (int comp = 0; comp < par.nbVariables()-1; comp++){
      stokes.bd.Dirichlet(i, noslip, comp);
    }
  }

  Vec u = stokes.solve();

  double normSOL = norm(u);
  MADprint("Navier Stokes: norm solution = ", normSOL);

  io.writeState(u);

  stokes.finalize();
  MADfinalize(par);
}
