from subprocess import *
from numpy import *
from multiprocessing import Pool
import fileinput

def getParameter(dataFilePath, variableName):
  datafile = open(dataFilePath, 'r')
  line = datafile.readline()
  while line:
    if (line.split(" = ")[0] == variableName):
      return line.split("=")[1]
    line = datafile.readline()

def setParameter(parameter, value, fileName):
  datafile = open(fileName, 'r+')
  succes = False
  for line in fileinput.input( fileName ):
    if (line.split("=")[0] == parameter):
      datafile.write(line.replace(line[0:len(line)-1], line.split("=")[0] + "=" +  str(value)))
      succes = True
    else:
      datafile.write(line)
  if not succes:
    datafile.write(parameter +"=" +  str(value))
  datafile.close()

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

# Define the parser
import argparse
parser = argparse.ArgumentParser(description='parser')
parser.add_argument('-n', action="store", dest='n_procs', default=1)
args = parser.parse_args()

n_procs =int(args.n_procs)

nbSim=128
n = arange(0.4, 1.0, 0.6/nbSim)
amplitude = arange(0.25, 0.75, 0.5/nbSim)
dis_res = arange(5000, 7000, 2000/nbSim)
print(n)
print(amplitude)
print(dis_res)

def loop(j):

  dirResults="./results/sim" + str(j) + "/"
  call("mkdir -p " + dirResults, shell=True)
  
  dataFile = dirResults + "data"
  
  call("cp par " + dataFile, shell=True)
  
  setParameter("dirResults", dirResults, dataFile) 
  setParameter("power_law_n", n[j], dataFile) 
  setParameter("amplitude", amplitude[j], dataFile) 
  setParameter("distalResistances", str(dis_res[j]) + " 6001.2" , dataFile)
  
  if n_procs == 1:
      cmd = "./ns.exe " + dataFile
  else:
      cmd = "./ns.exe " + dataFile + " > " + dirResults + "/<++>.log"
  
  print(cmd)
  call(cmd, shell=True) 
  
if n_procs == 1:
  for j in range(nbSim):
    loop(j)

else:
  p = Pool(processes = n_procs)
  p.map(loop, range(nbSim)) 
