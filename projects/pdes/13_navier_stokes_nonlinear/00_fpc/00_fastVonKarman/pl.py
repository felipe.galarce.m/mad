import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

time=np.arange(300,400)
p = np.loadtxt('p_front.csv', skiprows=1)[time,:];
q = np.loadtxt('p_back.csv', skiprows=1)[time,:];

pl.style.use(['science', 'nature'])
pl.plot(p[:,1],p[:,0]-q[:,0])
pl.ylabel("$\\Delta p$ [Pa]")
pl.xlabel("Time [s]")
pl.tight_layout()
pl.grid("on")
pl.show()
