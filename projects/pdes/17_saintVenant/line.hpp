/*=============================================================================
  This file is part of the code MAD 
  Multi-physics for mechanicAl engineering and Data assimilation
  Copyright (C) 2021-2023,
    
     Felipe Galarce at INRIA

  MDMA is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MDMA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MDMA. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

vector<double> ul(vector<double> x, double t, Parameters par){
  vector<double> bc(par.nbDofsPerNode()[0]);
  double T = 10.0;
  bc[0] = 1.0 - 0.4 * sin(2*PI*t/T) - 0.4 * sin(2*PI*t/T)  - 0.2*cos(2*PI*t/T);
  return bc;
}

vector<double> Al(vector<double> x, double t, Parameters par){
  vector<double> bc(par.nbDofsPerNode()[0]);
  bc[0] = 1.0; 
  return bc;
}

vector<double> ur(vector<double> x, double t, Parameters par){
  vector<double> bc(par.nbDofsPerNode()[0]);
  bc[0] = 1.0;
  return bc;
}

vector<double> Ar(vector<double> x, double t, Parameters par){
  vector<double> bc(par.nbDofsPerNode()[0]);
  bc[0] = 1.0; 
  return bc;
}

class BoundaryCondition{

  public:

    BoundaryCondition(){}
    ~BoundaryCondition(){};

    void initialize(Boundary & boundary, Geometry & geometry, Parameters parameters){
      par = parameters;
      geo = geometry;
      bd = boundary;
    }

    /* update time, declare and impose boundary conditions */
    void applyBC(Mat A, Vec b, double t){
      bd.time(t); 
      bd.Dirichlet(0, ul, 0);
      bd.Dirichlet(0, Al, 1);
      bd.Dirichlet(1, ur, 0);
      bd.Dirichlet(1, Ar, 1);
      bd.block(A, "symmetric");
      bd.block(b, "symmetric");
    }

    void solve(Vec u, double t, SV & sv){
      Vec b = sv.assembleRHS();
      Mat LHS = sv.assembleLHS(); 
      applyBC(LHS, b, t); 
      sv.setLHS(LHS);
      sv.solve(b, u);
    }

  private:
    Boundary bd;
    Geometry geo;
    Parameters par;

};
