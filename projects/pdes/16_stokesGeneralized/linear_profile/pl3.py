import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

h = np.array([0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625])

pl.style.use(['science', 'nature'])
hh = np.arange(1,8)

error = np.zeros(len(hh))
errorpspg = np.zeros(len(hh))
for i in range(len(hh)):
  p = np.loadtxt('./fine/bvs/g_1000/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  q = np.loadtxt('./fine/pspg/g_1000/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  error[i] = p[2];
  errorpspg[i] = q[2];

pl.loglog(h,error, "-", label="BVS")
pl.loglog(h,errorpspg, "--", label="pspg")

print("slope BVS")
print(np.log(error[6]/error[5])/np.log(2))
print("slope PSPG")
print(np.log(errorpspg[6]/errorpspg[5])/np.log(2))

pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
