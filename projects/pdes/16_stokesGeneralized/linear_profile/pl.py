import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

h = np.array([0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625])

pl.style.use(['science', 'nature'])

p = np.loadtxt('./error_BVS_delta100.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".--r",label="BVS $\\gamma = 100$")

p = np.loadtxt('./error_BVS_delta10.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".--b",label="BVS $\\gamma = 10$")

p = np.loadtxt('./error_BVS_delta1.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".--g",label="BVS $\\gamma = 1$")

print("BVS")
print(np.log(error[6]/error[5])/np.log(2))

p = np.loadtxt('./error_PSPG_delta100.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".-r", label="PSPG $\\gamma = 100$")

p = np.loadtxt('./error_PSPG_delta10.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".-b", label="PSPG $\\gamma = 10$")

p = np.loadtxt('./error_PSPG_delta1.txt', skiprows=0);
error = p[:,2]
pl.loglog(h, error, ".-g", label="PSPG $\\gamma = 1$")

pl.loglog(h, 6*h, "k:", label="$O(h)$")
pl.loglog(h, 2*h**2, "y:", label="$O(h^2)$")

#pl.ylabel("$\\lVert (u,p) - (u_h, p_h) \\rVert$")
#pl.ylabel("$\\lVert \\nabla u - \\nabla u_h, \\rVert$")
pl.ylabel("$\\lVert p - p_h \\rVert$")

pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
