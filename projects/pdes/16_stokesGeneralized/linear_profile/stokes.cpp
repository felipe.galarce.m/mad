/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2021-2023,
    
     Felipe Galarce at INRIA/WIAS/PUCV 

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

vector<double> analytic_ux(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  double H = 1.0;
  double num = 2.0 * (x[1] + par.viscosity())/pow(par.viscosity(), 3) + 1.0/(x[1] + par.viscosity())/(x[1] + par.viscosity());
  double den = 2.0 * (H + par.viscosity()) / pow(par.viscosity(), 3) + 1.0 / (H + par.viscosity()) / (H + par.viscosity());
  bc[0] = par.kappa()/2.0 * (1.0 - num/den);
  return bc;
}

vector<double> analytic_uy(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  return bc;
}

vector<double> analytic_p(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  double L = 5.0;
  bc[0] = par.kappa() * (L/2.0 - x[0]);
  return bc;
}

double viscosity(vector<double> x, Parameters par){
  return (x[1] + par.viscosity())*(x[1] + par.viscosity());
}

double sigma(vector<double> x, Parameters par){
  return par.sigma();
}

vector<double> grad_viscosity(vector<double> x, Parameters par){
  vector<double> dnu(2, 0.0);
  dnu[0] = 0.0;
  dnu[1] = 2.0*(x[1] + par.viscosity());
  return dnu;
}

int main(int argc, char *argv[]){

  Parameters par = MADinitialize(argc, argv);

  /* Initialize MAD objects */
  IO io;
  io.initialize(par);
    
  Geometry geo;
  geo.initialize(par, io);

  Boundary bd;
  bd.initialize(par, geo);

  StokesGeneralized sg;
  sg.initialize(par, geo, bd, viscosity, grad_viscosity, sigma);

  /* Assemble discretization matrix */
  sg.assembleLHS_nonNewtonian(viscosity, grad_viscosity, sigma);

  /* Declare boundary conditions */
  for (int i : par.walls()){
    sg.bd.Dirichlet(i, analytic_ux, 0);
    sg.bd.Dirichlet(i, analytic_uy, 1);
  }

  /* Build right hand side and solve */
  Vec rhs = zeros(geo.nbVertices*(geo.dimension()+1));
  Vec up = sg.solve(rhs, analytic_p);

  /* Write solutions */
  double normSol;
  VecNorm(up, NORM_2, &normSol);
  cout << "Norm solution: " << normSol << endl;
  io.writeState(up);

  /* Analitic solution */
  vector<Vec> analitical_sol(3);
  for (int k = 0; k < par.nbVariables(); k++){
    analitical_sol[k] = vec(geo.nbVertices);
    for (int idPoint = 0; idPoint < geo.coordinates().size(); idPoint++){
    vector<double> coor = geo.coordinates()[idPoint];
      if (k == 0){
        vecSet(analitical_sol[0], idPoint, analytic_ux(coor, 0.0, par)[0]);
      } else if (k == 1){
        vecSet(analitical_sol[1], idPoint, analytic_uy(coor, 0.0, par)[0]);
      } else {
        vecSet(analitical_sol[2], idPoint, analytic_p(coor, 0.0, par)[0]);
      }
    }
    VecAssemblyBegin(analitical_sol[k]);
    VecAssemblyEnd(analitical_sol[k]);
  }

  io.writeState(analitical_sol[0], "ux_an");
  io.writeState(analitical_sol[1], "uy_an");
  io.writeState(analitical_sol[2], "p_an");

  InnerProduct ip;
  ip.initialize(par, geo, bd);
  ip.assembleMassAndStiffness();

  Calculus calculus;
  calculus.initialize(par, geo, bd);

  Vec solution = calculus.join(analitical_sol, io);
  Vec error = zeros(par.nbVariables() * geo.nbVertices);
  VecAXPY(error,  1.0, solution);
  VecAXPY(error, -1.0, up);
  double error_norm = sqrt(ip(error, error));

  Vec error_ux = zeros(geo.nbVertices);
  VecAXPY(error_ux,  1.0, calculus.split(up)[0]);
  VecAXPY(error_ux, -1.0, analitical_sol[0]);
  Vec error_uy = zeros(geo.nbVertices);
  VecAXPY(error_uy,  1.0, calculus.split(up)[1]);
  VecAXPY(error_uy, -1.0, analitical_sol[1]);
  double error_norm_H1_semi_u = sqrt(ip(error_ux, error_ux, "H1_semi") + ip(error_uy, error_uy, "H1_semi"));

  /* Ensure zero-mean pressure */
  Vec press = calculus.split(up)[2];
  Vec Mp = vec(geo.nbVertices);
  double pressure_mean;
  MatMult(geo.massMatrix(), press, Mp);
  VecDot(Mp, ones(geo.nbVertices), &pressure_mean);

  double domain_length;
  MatMult(geo.massMatrix(), ones(geo.nbVertices), Mp);
  VecDot(Mp, ones(geo.nbVertices), &domain_length);

  VecAXPY(press, -pressure_mean/domain_length, ones(geo.nbVertices));

  io.writeState(press, "p_zeroMean");

  Vec error_p = zeros(geo.nbVertices);
  VecAXPY(error_p,  1.0, press);
  VecAXPY(error_p, -1.0, analitical_sol[2]);
  double error_norm_L2_p = sqrt(ip(error_p, error_p, "L2"));
  
  Vec errorP_bd = vec(geo.nbVertices);
  MatMult(bd.geoMass(), error_p, errorP_bd);
  double errorP_trace;
  VecDot(errorP_bd, error_p, &errorP_trace);

  cout << error_norm << " " << error_norm_H1_semi_u << " " << error_norm_L2_p <<
       " " << errorP_trace << endl; 
  ofstream errorFile(par.dirResults() + "/error.txt");
  errorFile << error_norm << " " << error_norm_H1_semi_u << " " << error_norm_L2_p << " "; 
  errorFile << sqrt(ip(solution, solution))                   << " " 
            << sqrt(ip(analitical_sol[0], analitical_sol[0], "H1_semi") 
                  + ip(analitical_sol[1], analitical_sol[1], "H1_semi")) << " " 
            << sqrt(ip(analitical_sol[2], analitical_sol[2], "L2")) << endl;
  errorFile.close();
  MADfinalize(par);
}
