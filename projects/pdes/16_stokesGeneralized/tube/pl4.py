import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

h = np.array([0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625])

pl.style.use(['science', 'nature'])
hh = np.arange(1,10)
var = 1

gamma=1e-3
C=1.0

error = np.zeros(len(hh))
errorpspg = np.zeros(len(hh))
for i in range(len(hh)):
  p = np.loadtxt('./bvs/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  q = np.loadtxt('./pspg/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  error[i] = p[var];
  errorpspg[i] = q[var];

pl.loglog(h,error, "b.-", label="PSPG 1e-3")
pl.loglog(h,errorpspg, "b.--", label="PSPG 1e-3")

gamma=1.0
C=1.0
error = np.zeros(len(hh))
errorpspg = np.zeros(len(hh))
for i in range(len(hh)):
  p = np.loadtxt('./bvs/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  q = np.loadtxt('./pspg/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  error[i] = p[var];
  errorpspg[i] = q[var];
pl.loglog(h,error, ".--", color="orange", label="PSPG 1e-3")
pl.loglog(h,errorpspg, ".-", color="orange", label="PSPG 1e-3")

gamma=1e3
C=1.0
error = np.zeros(len(hh))
errorpspg = np.zeros(len(hh))
for i in range(len(hh)):
  p = np.loadtxt('./bvs/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  q = np.loadtxt('./pspg/delta_' + str(int(1000*gamma)) +'/C_' + str(int(1000*C)) + '/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  error[i] = p[var];
  errorpspg[i] = q[var];
pl.loglog(h,error, ".--", color="green", label="PSPG 1e-3")
pl.loglog(h,errorpspg, ".-", color="green", label="PSPG 1e-3")

pl.loglog(h,h, "--k", label="$\\mathcal{O}(n)$")
pl.loglog(h,h**2, "--", color="grey", label="$\\mathcal{O}(n^2)$")

print("slope BVS")
print(np.log(error[6]/error[5])/np.log(2))
print("slope PSPG")
print(np.log(errorpspg[6]/errorpspg[5])/np.log(2))

#pl.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=20)
pl.tight_layout()
pl.grid("on")
pl.show()
