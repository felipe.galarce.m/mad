/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2021-2025,
    
     Felipe Galarce at INRIA/WIAS/PUCV 

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

vector<double> analytic_ux(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  return bc;
}

vector<double> analytic_uy(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  return bc;
}

vector<double> analytic_uz(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  double A = par.kappa() / par.alpha() / par.alpha() * 
    ( par.inner_radius() * exp(par.inner_radius())      - par.outer_radius() * exp( par.outer_radius())) / 
                          (exp( 2.0*par.outer_radius()) -                      exp( 2.0*par.inner_radius()));
  double B = par.kappa() / par.alpha() / par.alpha() * 
    ( par.inner_radius() * exp(-par.inner_radius())     - par.outer_radius() * exp(-par.outer_radius())) / 
                          (exp(-2.0*par.outer_radius()) -                      exp(-2.0*par.inner_radius()));
  bc[0] = A*exp( par.alpha()*sqrt(x[0]*x[0] + x[1]*x[1])) 
        + B*exp(-par.alpha()*sqrt(x[0]*x[0] + x[1]*x[1])) 
        + par.kappa()/par.alpha()/par.alpha() * sqrt(x[0]*x[0]+x[1]*x[1]);
  return bc;
}

vector<double> analytic_p(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  bc[0] = par.kappa() * (par.length()/2.0 - x[2]);
  return bc;
}

double viscosity(vector<double> x, Parameters par){
  return par.alpha()*par.alpha()/sqrt(x[0]*x[0] + x[1]*x[1]);
}

double sigma(vector<double> x, Parameters par){
  return par.alpha()*par.alpha()/sqrt(x[0]*x[0] + x[1]*x[1]);
}

vector<double> grad_viscosity(vector<double> x, Parameters par){
  vector<double> dnu(3, 0.0);
  dnu[0] = -x[0]*par.alpha()*par.alpha()*pow(x[0]*x[0] + x[1]*x[1] ,-1.5);
  dnu[1] = -x[1]*par.alpha()*par.alpha()*pow(x[0]*x[0] + x[1]*x[1] ,-1.5);
  dnu[2] =  0.0;
  return dnu;
}

int main(int argc, char *argv[]){

  Parameters par = MADinitialize(argc, argv);

  /* Initialize MAD objects */
  IO io;
  io.initialize(par);
    
  Geometry geo;
  geo.initialize(par, io);

  Boundary bd;
  bd.initialize(par, geo);

  StokesGeneralized sg;
  sg.initialize(par, geo, bd, viscosity, grad_viscosity, sigma);

  /* Assemble discretization matrix */
  sg.assembleLHS_nonNewtonian(viscosity, grad_viscosity, sigma);

  /* Declare boundary conditions */
  for (int i : par.walls()){
    sg.bd.Dirichlet(i, analytic_ux, 0);
    sg.bd.Dirichlet(i, analytic_uy, 1);
    sg.bd.Dirichlet(i, analytic_uz, 2);
  }

  /* Build right hand side and solve */
  Vec rhs = zeros(geo.nbVertices*(geo.dimension()+1));
  Vec up = sg.solve(rhs, analytic_p);

  /* Write solutions */
  double normSol;
  VecNorm(up, NORM_2, &normSol);
  cout << "Norm solution: " << normSol << endl;
  io.writeState(up);

  /* Analitic solution */
  vector<Vec> analitical_sol(4);
  for (int k = 0; k < par.nbVariables(); k++){
    analitical_sol[k] = vec(geo.nbVertices);
    for (int idPoint = 0; idPoint < geo.coordinates().size(); idPoint++){
    vector<double> coor = geo.coordinates()[idPoint];
      if (k == 0){
        vecSet(analitical_sol[0], idPoint, analytic_ux(coor, 0.0, par)[0]);
      } else if (k == 1){
        vecSet(analitical_sol[1], idPoint, analytic_uy(coor, 0.0, par)[0]);
      } else if (k == 2){
        vecSet(analitical_sol[2], idPoint, analytic_uz(coor, 0.0, par)[0]);
      } else {
        vecSet(analitical_sol[3], idPoint, analytic_p(coor, 0.0, par)[0]);
      }
    }
    VecAssemblyBegin(analitical_sol[k]);
    VecAssemblyEnd(analitical_sol[k]);
  }

  io.writeState(analitical_sol[0], "ux_an");
  io.writeState(analitical_sol[1], "uy_an");
  io.writeState(analitical_sol[2], "uz_an");
  io.writeState(analitical_sol[3], "p_an");

  InnerProduct ip;
  ip.initialize(par, geo, bd);
  ip.assembleMassAndStiffness();

  Calculus calculus;
  calculus.initialize(par, geo, bd);

  /* error grad u */
  Vec error_ux = zeros(geo.nbVertices);
  VecAXPY(error_ux,  1.0, calculus.split(up)[0]);
  VecAXPY(error_ux, -1.0, analitical_sol[0]);
  Vec error_uy = zeros(geo.nbVertices);
  VecAXPY(error_uy,  1.0, calculus.split(up)[1]);
  VecAXPY(error_uy, -1.0, analitical_sol[1]);
  Vec error_uz = zeros(geo.nbVertices);
  VecAXPY(error_uz,  1.0, calculus.split(up)[2]);
  VecAXPY(error_uz, -1.0, analitical_sol[2]);
  double error_norm_H1_semi_u = sqrt(ip(error_ux, error_ux, "H1_semi") + ip(error_uy, error_uy, "H1_semi") + ip(error_uz, error_uz, "H1_semi"));

  /* Ensure zero-mean pressure */
  Vec press = calculus.split(up)[geo.dimension()];
  Vec Mp = vec(geo.nbVertices);
  double pressure_mean;
  MatMult(geo.massMatrix(), press, Mp);
  VecDot(Mp, ones(geo.nbVertices), &pressure_mean);

  double domain_length;
  MatMult(geo.massMatrix(), ones(geo.nbVertices), Mp);
  VecDot(Mp, ones(geo.nbVertices), &domain_length);
  VecAXPY(press, -pressure_mean/domain_length, ones(geo.nbVertices));

  io.writeState(press, "p_zeroMean");

  /* Error p */
  Vec error_p = zeros(geo.nbVertices);
  VecAXPY(error_p,  1.0, press);
  VecAXPY(error_p, -1.0, analitical_sol[geo.dimension()]);
  double error_norm_L2_p = sqrt(ip(error_p, error_p, "L2"));

  /* Error p boundary */
  Vec errorP_bd = vec(geo.nbVertices);
  MatMult(bd.geoMass(), error_p, errorP_bd);
  double errorP_trace;
  VecDot(errorP_bd, error_p, &errorP_trace);

  cout << "| grad u - grad u* |        |p - p*|           |p - p*|_BD" << endl;
  cout <<  error_norm_H1_semi_u << " " << error_norm_L2_p << " " << errorP_trace << endl; 
  ofstream errorFile(par.dirResults() + "/error.txt");
  errorFile << error_norm_H1_semi_u << " " << error_norm_L2_p << " " << errorP_trace << endl; 
  errorFile.close();
  MADfinalize(par);
}
