import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

h = np.array([0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625])

p = np.loadtxt('./error_BVS_delta100.txt', skiprows=0);
pl.style.use(['science', 'nature'])
error = p[:,0]
pl.loglog(h, error, label="$\\lVert (u,p) - (u_h, p_h) \\rVert$")
pl.loglog(h, error, "*")
error = p[:,1]
pl.loglog(h, error, "r", label="$\\lVert \\nabla u - \\nabla u_h \\rVert$")
pl.loglog(h, error, "r*")
error = p[:,2]
pl.loglog(h, error, "b", label="$\\lVert p - p_h \\rVert$")
pl.loglog(h, error, "b*")

print("BVS")
print(np.log(error[5]/error[4])/np.log(2))

pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()

