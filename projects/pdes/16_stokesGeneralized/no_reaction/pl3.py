import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

h = np.array([0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625])
#h = np.array([0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125])

pl.style.use(['science', 'nature'])
hh = np.arange(1,len(h)+1)

delta=1e3

error = np.zeros(len(hh))
errorpspg = np.zeros(len(hh))
for i in range(len(hh)):
#  p = np.loadtxt('./fine/bvs/g_1000/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
#  q = np.loadtxt('./fine/pspg/g_1000/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  p = np.loadtxt('./bvs/delta_' + str(int(delta*1000)) + '/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  q = np.loadtxt('./pspg/delta_' + str(int(delta*1000)) + '/C_1000/sim_h' + str(hh[i]) +  '/error.txt', skiprows=0);
  error[i] = p[1];
  errorpspg[i] = q[1];

pl.loglog(h,error, ".-", color="orange", label="BVS")
pl.loglog(h,errorpspg, ".-", color="green", label="pspg")
pl.loglog(h,h, "--k", label="$\\mathcal{O}(n)$")
pl.loglog(h,h**2, "--", color="grey", label="$\\mathcal{O}(n^2)$")

last = len(error)
print("slope BVS")
print(np.log(error[last-1]/error[last-2])/np.log(2))
print("slope PSPG")
print(np.log(errorpspg[last-1]/errorpspg[last-2])/np.log(2))

#pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
