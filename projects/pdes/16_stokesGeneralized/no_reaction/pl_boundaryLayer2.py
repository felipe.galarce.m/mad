import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

method="bvs"
pl.style.use(['science', 'nature'])
g = np.loadtxt('./layer_fine/' + str(method)+ '_delta10.txt', skiprows=1, delimiter=",")[:,2];
pmax = max(g)
p = np.loadtxt('./layer_fine/' + str(method)+ '_delta1.txt', skiprows=1, delimiter=",")[:,1];
q = np.loadtxt('./layer_fine/' + str(method)+ '_delta10.txt', skiprows=1, delimiter=",")[:,1];
r = np.loadtxt('./layer_fine/' + str(method)+ '_delta100.txt', skiprows=1, delimiter=",")[:,1];
s = np.loadtxt('./layer_fine/' + str(method)+ '_delta1000.txt', skiprows=1, delimiter=",")[:,1];

mesh = np.arange(0, 5, 5/len(p))
pl.plot(mesh, p/max(p), linewidth=2.0,label="$\\gamma = 10^0$")
pl.plot(mesh, q/max(q), linewidth=1.0,label="$\\gamma = 10^1$")
pl.plot(mesh, r/max(r), "--", linewidth=1.0,label="$\\gamma = 10^2$")
pl.plot(mesh, s/max(s), "--", linewidth=1.0,label="$\\gamma = 10^3$")
pl.plot(mesh, g/max(g), "--",linewidth=1.5,  label="Analytic")
#pl.xlim([0, 0.4])
#pl.ylim([0.8, 1.0])
pl.xlim([-0.001, 0.04])
pl.ylim([0.98, 1.001])
pl.grid("on")
pl.legend(loc='best')
pl.xlabel("x")
pl.ylabel("$p/p_{max}$")
pl.tight_layout()
pl.show()
