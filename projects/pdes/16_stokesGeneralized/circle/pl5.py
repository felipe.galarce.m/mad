import numpy as np
import scienceplots
import matplotlib.pyplot as pl
from matplotlib import rc
import os
from matplotlib import ticker

gamma = np.array([1e-2, 1e-1, 1e0, 1e1, 1e2])
C = np.array([1e-2, 1e-1,1e0, 1e1,1e2])
position = 1

error = np.zeros((len(gamma),len(C)))
pl.style.use(['science', 'nature'])
dir_results = "./bvs/"

for i in range(len(C)):
  for j in range(len(gamma)):
    p = np.loadtxt('./bvs/g_' + str(int(1000*gamma[j])) +'/C_' + str(int(1000*C[i])) + '/sim_h7/error.txt', skiprows=0);
    error[i,j] = p[position]

print(error)
corner_masks = [False, True]
x,y = np.meshgrid(gamma, C)
cs = pl.contourf(x, y, error, levels=20, locator=ticker.LogLocator())

#pl.yaxis.set_major_locator(ticker.LogLocator())

pl.colorbar()
pl.xlabel("$T_c^*$")
pl.ylabel("Bi")
pl.tight_layout()
pl.show()
