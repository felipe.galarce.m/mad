import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
import scienceplots
import os
dir_results = os.environ['MAD_RESULTS']

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

pl.style.use(['science', 'nature'])
g = np.loadtxt('./bvs_1000.csv', skiprows=1, delimiter=",")[:,1];
p = np.loadtxt('./bvs_1000.csv', skiprows=1, delimiter=",")[:,0];
q = np.loadtxt('./bvs_1.csv', skiprows=1, delimiter=",")[:,0];
r = np.loadtxt('./bvs_100.csv', skiprows=1, delimiter=",")[:,0];
s = np.loadtxt('./bvs_10.csv', skiprows=1, delimiter=",")[:,0];

mesh = np.arange(0, 5, 5/len(g))
pl.plot(mesh, g/max(g), "--", label="Analytic")
pl.plot(mesh, q/max(q), linewidth=2.5,label="BVS $\\gamma = 10^0$")
pl.plot(mesh, s/max(s), linewidth=2.0,label="BVS $\\gamma = 10^1$")
pl.plot(mesh, r/max(r), linewidth=1.5,label="BVS $\\gamma = 10^2$")
pl.plot(mesh, p/max(p), "--", linewidth=1.0,label="BVS $\\gamma = 10^3$")
pl.xlim([0, 0.04])
pl.ylim([0.98, 1.001])
pl.grid("on")
pl.legend(loc='best')
pl.xlabel("x")
pl.ylabel("$p/p_{max}$")
pl.tight_layout()
pl.show()
