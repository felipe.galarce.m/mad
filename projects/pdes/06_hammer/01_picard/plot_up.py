import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
from numpy import linalg as la
import scienceplots
import os

def getParameter(dataFilePath, variableName):
  datafile = open(dataFilePath, 'r')
  line = datafile.readline()
  while line:
    if (line.split("=")[0] == variableName):
      return line.split("=")[1]
    line = datafile.readline()

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

firstTime = 0
timeSteps = int(getParameter("./par","nbIterations"))
nbVertices = 501
dt=float(getParameter("./par", "timeStep"))

pl.style.use(['science', 'nature'])
dirResults = "./critical_close/"

Cv = float(getParameter("./par", "concentration"))
rhofluid = float(getParameter("./par", "density_fluid"))
rhosolid = float(getParameter("./par", "density_solid"))
rhomix = rhosolid*0.3 + (1.0 - Cv)* rhofluid
viscosity=float(getParameter("./par", "viscosity"))
length=float(getParameter("./par", "length"))
heigth=float(getParameter("./par", "heigth"))
diameter=float(getParameter("./par", "diameter"))
wave_velocity=839
dx = 0.002*length
mesh = np.arange(0,200+dx,dx)

itera = 10000
pl.figure(num=None, figsize=(2.5, 2))
Pr=heigth*rhomix*9.81
Ur=1/32*diameter*diameter*Pr/length/viscosity
ca=length/wave_velocity
v = np.loadtxt(dirResults + 'sol.' + wildcard(itera) + '.txt');
pl.plot(mesh, v[0:501]*Ur)
pl.xlabel("x [m]")
pl.ylabel("Velocity [m/s]")
#pl.xlim([0, timeSteps*dt*ca])
#pl.legend()
pl.tight_layout()
#pl.savefig("pl_dimensional_u.pdf", format="pdf", bbox_inches="tight")
pl.show()

pl.figure(num=None, figsize=(2.5, 2))
pl.plot(mesh, v[501:1002]*Pr)
pl.xlabel("x [m]")
pl.ylabel("Pressure [Pa]")
#pl.xlim([0, timeSteps*dt*ca])
pl.tight_layout()
#pl.legend()
#pl.savefig("pl_dimensional_p.pdf", format="pdf", bbox_inches="tight")
pl.show()
