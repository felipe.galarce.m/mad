import numpy as np
import matplotlib.pyplot as pl

He = np.array([1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7])
Xc = np.zeros(7)
Rec = np.zeros(7)

k = 0
for h in He:
    a = h/16800
    coeff = [a, -3*a, 3*a+1, -a]
    rt = np.roots(coeff)
    print(rt)
#    if np.isreal(rt[0]):
#      Xc[k] = rt[0]
#    if np.isreal(rt[1]):
#      Xc[k] = rt[1]
#    if np.isreal(rt[2]):
    Xc[k] = rt[2]
    Rec[k] = He[k] / Xc[k] * (1.0 - 4.0/3.0 * Xc[k] + 1.0/3.0 * Xc[k]**4)
    print(rt)
    k = k + 1

fontSize=16
#pl.loglog(He,Xc)
pl.loglog(He,Rec)
pl.xlabel("He")
pl.ylabel("Re critico")
pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.tight_layout()
pl.grid("on")
pl.show()
