import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
from numpy import linalg as la
import scienceplots
import os

def getParameter(dataFilePath, variableName):
  datafile = open(dataFilePath, 'r')
  line = datafile.readline()
  while line:
    if (line.split("=")[0] == variableName):
      return line.split("=")[1]
    line = datafile.readline()

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

firstTime = 0
timeSteps = int(getParameter("./par","nbIterations"))
nbVertices = 501
dt=float(getParameter("./par", "timeStep"))

pl.style.use(['science', 'nature'])

Cv = float(getParameter("./par", "concentration"))
rhofluid = float(getParameter("./par", "density_fluid"))
rhosolid = float(getParameter("./par", "density_solid"))
rhomix = rhosolid*0.3 + (1.0 - Cv)* rhofluid
viscosity=float(getParameter("./par", "viscosity"))
length=float(getParameter("./par", "length"))
heigth=float(getParameter("./par", "heigth"))
diameter=float(getParameter("./par", "diameter"))
wave_velocity=839

#pl.figure(num=None, figsize=(4, 1))
Pr=heigth*rhomix*9.81
Ur=1/32*diameter*diameter*Pr/length/viscosity
ca=length/wave_velocity
dirResults = "./with_conv/"
v = np.loadtxt(dirResults + 'ctrlU.txt');
pl.plot(np.arange(timeSteps)*dt*ca, v[:,0]*Ur, "--", label="conv")
dirResults = "./no_conv/"
v = np.loadtxt(dirResults + 'ctrlU.txt');
pl.plot(np.arange(timeSteps)*dt*ca, v[:,0]*Ur, "--", label="no conv")
#pl.plot(np.arange(timeSteps)*dt*ca, v[:,1]*Ur, label="$x=L/2$")
#pl.plot(np.arange(timeSteps)*dt*ca, v[:,2]*Ur, label="$x=L$")
pl.xlabel("Time [s]")
pl.ylabel("Velocity [m/s]")
pl.xlim([0, timeSteps*dt*ca])
pl.legend()
pl.tight_layout()
#pl.savefig("pl_dimensional_u.pdf", format="pdf", bbox_inches="tight")
pl.show()

#pl.figure(num=None, figsize=(4, 1))
dirResults = "./with_conv/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pl.plot(np.arange(timeSteps)*dt*ca, v[:,2]*Pr, label="conv")
dirResults = "./no_conv/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pl.plot(np.arange(timeSteps)*dt*ca, v[:,2]*Pr, label="no conv")
pl.xlabel("Time [s]")
pl.ylabel("Pressure [Pa]")
pl.xlim([0, timeSteps*dt*ca])
pl.tight_layout()
pl.legend()
#pl.savefig("pl_dimensional_p.pdf", format="pdf", bbox_inches="tight")
pl.show()

print(np.max(v[:,2])*Pr)
