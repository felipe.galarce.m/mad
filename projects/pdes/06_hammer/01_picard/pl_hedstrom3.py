import numpy as np
import matplotlib.pyplot as pl
from matplotlib import rc
from numpy import linalg as la
import scienceplots
import os

def getParameter(dataFilePath, variableName):
  datafile = open(dataFilePath, 'r')
  line = datafile.readline()
  while line:
    if (line.split("=")[0] == variableName):
      return line.split("=")[1]
    line = datafile.readline()

def wildcard(idIter):
  if (idIter < 10):
   iteration = "0000" + str(idIter);
  elif (idIter < 100):
    iteration = "000" + str(idIter);
  elif (idIter < 1000):
    iteration = "00" + str(idIter);
  elif (idIter < 10000):
    iteration = "0" + str(idIter);
  elif (idIter < 100000):
    iteration = str(idIter);
  return iteration

firstTime = 0
timeSteps = int(getParameter("./par","nbIterations"))
nbVertices = 501
dt=float(getParameter("./par", "timeStep"))

pl.style.use(['science', 'nature'])

Cv = float(getParameter("./par", "concentration"))
rhofluid = float(getParameter("./par", "density_fluid"))
rhosolid = float(getParameter("./par", "density_solid"))
rhomix = rhosolid*0.3 + (1.0 - Cv)* rhofluid
viscosity=float(getParameter("./par", "viscosity"))
length=float(getParameter("./par", "length"))
heigth=float(getParameter("./par", "heigth"))
diameter=float(getParameter("./par", "diameter"))
wave_velocity=839

#pl.figure(num=None, figsize=(4, 1))
Pr=heigth*rhomix*9.81
Ur=1/32*diameter*diameter*Pr/length/viscosity
ca=length/wave_velocity

pmax = np.zeros(6)

He = np.array([1e3, 1e4, 1e5, 1e7, 1e8, 1e9])

pl.figure(num=None, figsize=(5, 3))
dirResults = "./hedstrom/He100000000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[5] = max(v[:,2])

dirResults = "./hedstrom/He10000000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[4] = max(v[:,2])

dirResults = "./hedstrom/He1000000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[3] = max(v[:,2])

dirResults = "./hedstrom/He100000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[2] = max(v[:,2])

dirResults = "./hedstrom/He10000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[1] = max(v[:,2])

dirResults = "./hedstrom/He1000/"
v = np.loadtxt(dirResults + 'ctrlP.txt');
pmax[0] = max(v[:,2])

fontSize=16
pl.style.use(['science', 'nature'])
pl.rcParams.update({'font.size': fontSize})
pl.plot(He, pmax*Pr)
pl.plot(He, pmax*Pr, "*")
pl.xscale("log")

pl.xlabel("Hedstrom number He")
pl.ylabel("Peak Pressure [Pa]")
pl.tight_layout()
pl.legend()
#pl.savefig("pl_dimensional_p.pdf", format="pdf", bbox_inches="tight")
pl.show()

