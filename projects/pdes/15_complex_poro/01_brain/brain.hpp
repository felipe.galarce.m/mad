/*=============================================================================
  This file is part of the code MAD 
  Multi-physics for mechanicAl engineering and Data assimilation
  Copyright (C) 2021,
    
     Felipe Galarce at INRIA

  MDMA is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MDMA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MDMA. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include <mad.hpp>

vector<double> MRE2(vector<double> x, double t, Parameters par){
  vector<double> bc(3, 0.0); 
  double lengthY = 16.86;
  bc[1] = par.amplitude()*(1.0-x[1]/lengthY);
  return bc;
}

vector<double> zero(vector<double> x, double t, Parameters par){
  vector<double> bc(3, 0.0);
  return bc;
}

vector<double> zero_scalar(vector<double> x, double t, Parameters par){
  vector<double> bc(1, 0.0);
  return bc;
}

class Brain{

  public:

    Brain(){}
    ~Brain(){};

    void initialize(Boundary & boundary, Parameters parameters){
      par = parameters;
      bd = boundary;
    }

//    void applyBC(Mat A, Vec b){
//      for (int i : par.walls()){
//        bd.Dirichlet(i, zero_scalar, 2);
//        bd.Dirichlet(i, zero_scalar, 3);
//        bd.Neumann(i, MRE2, 0);
//      }
//      bd.Dirichlet(3, zero_scalar, 2);
//      bd.Dirichlet(3, zero_scalar, 3);
//      bd.Dirichlet(par.fixed_boundary(), zero, 0);
//      bd.Dirichlet(par.fixed_boundary(), zero, 1);
//      bd.block(A, "symmetric");
//      bd.block(b, "symmetric");
//    }

    void applyBC(Mat A, Vec b){
      for (int i : par.walls()){
        bd.Dirichlet(i, zero_scalar, 2);
        bd.Dirichlet(i, zero_scalar, 3);
        bd.Neumann(i, MRE2, 0);
      }
      bd.Dirichlet(par.fixed_boundary(), zero, 0);
      bd.Dirichlet(par.fixed_boundary(), zero, 1);
      bd.block(A, "symmetric");
      bd.block(b, "symmetric");
    }
    
  private:
    Boundary bd;
    Parameters par;
    PetscErrorCode code;
};
