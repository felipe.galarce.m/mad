/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2020,
    
     Felipe Galarce at INRIA

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#ifndef CFD
#define CFD

#include <iostream>
#include <math.h>
#include <vector>
#include <slepc.h>
#include <STLvectorUtils.hpp>
#include <petscWrapper.hpp>
#include <tools.hpp>
#include <parameters.hpp>
#include <io.hpp>
#include <geometry.hpp>
#include <innerProduct.hpp>
#include <interpolate.hpp>

using namespace std;

class CFD{

  public:
    CFD(){}
    ~CFD(){}
    void finalize();

    void initialize(Parameters parameters);
    void initialize(Parameters parameters, const Geometry & geometry);
    void initialize(Parameters parameters, const Geometry & geometry, const IO & inputOutput);
//    void assembleMassAndStiffnessMatrix();
//    void assembleMassBoundary();

//    vector<double> computeVectorIntegralOverSurface(Vec u);
//    vector<double> computeVectorIntegralOverSurface(int labelBD, Vec u);

    Vec piola(Vec uu, Vec dd);
    Vec piola(string dir_u, string dir_d); /* wrapper */
    vector<Vec> decompose_vector_field(Vec u);
    Vec harmonicExtension(Vec u);
    Vec harmonicExtensionNeumann(Vec u);
    Vec trace(Vec u);

//    Vec wallShearStress(string velocity_path); /* wrapper */
//    Vec wallShearStress(Vec u);
//    Vec wallShearStress2D(Vec u);
//    Vec wallShearStress2D(string velocity_path); /* wrapper */
//
//    Vec vorticity(string velocity_path);
    Vec vorticity(Vec u);

    Vec vorticity2D(string velocity_path);
    Vec vorticity2D(Vec u);

//    Vec divergence(string velocity_path); /* wrapper */
//    Vec divergence(Vec u);

    /* compute gradient of a SCALAR field */
    Vec gradient(string velocity_path); /* wrapper */
    Vec gradient(Vec u);

//    double kinetic_energy(Vec u);
//    double kinetic_energy(string velocity_path); /* wrapper */

//    double viscous_energy(Vec u);
//    double viscous_energy(string velocity_path); /* wrapper */

//    double viscous_energyBD(Vec u);
//    double viscous_energyBD(string velocity_path); /* wrapper */

//    double convective_energy(Vec u);
//    double convective_energy(string velocity_path); /* wrapper */

//    double flow(Vec u, int labelBD);
//
//    vector<double> pressureDropVirtualWork(string velocity_path_u, string velocity_path_u0); /* wrapper */
//    vector<double> pressureDropVirtualWork(Vec u, Vec u0);

//    double integralSurface(Vec p, int labelBD);

//    Vec interpolateP0_P1_boundary(vector<double> u);
//    Vec interpolateP0_P1(vector<double> u);
//
    /* Acces functions */
    inline const Mat & mass() const {
      return m_mass;}

    inline const Mat & massBD() const {
      return m_massBD;}

    inline const Mat & stiffness() const {
      return m_stiff;}

//    void assembleNSstatic(Mat & M, Mat & NS);
//    Mat & assembleNS(const Vec & solution);
    Vec solve(const Mat & NS, const Vec & rhs);
    void configureSolver();
    void time(double t);

    IO io; 
    Geometry geo;

  private:
    int m_dimension;
    double m_time = 0.0;
    Vec gradient_scalar(Vec u);

    Parameters par;

    Mat m_mass, m_stiff, m_massBD;
//    MasterTetrahedron m_tetrahedron;
//    MasterTriangle m_triangle_mass;

    int m_nbDofs;
    int m_nbVertices;

    KSP m_solver;
    PC m_pc;

    vector<int> m_indexes; 
    vector<int> m_indexesBoundary; 

    PetscErrorCode code;
    int m_verbose = 4;

    int m_world_rank;

    /* Navier-Stokes */
    int m_nbDofsVel;
    int m_nbDofsPress;
    KSP m_ksp;

    INT m_interpolator;
};  

#endif
