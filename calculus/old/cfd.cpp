/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2022,
    
     Felipe Galarce at INRIA

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<cfd.hpp>

void CFD::finalize(){
  cout << "MEMORY: releasing cfd memory." << endl;
  MatDestroy(&m_massBD);
  MatDestroy(&m_mass);
  MatDestroy(&m_stiff);
}

void CFD::initialize(Parameters parameters){
  cout << "CFD: Initializing." << endl;
  par = parameters;
  io.initialize(par);
  geo.initialize(par, io);
  m_nbDofs = geo.dimension()*io.nbVertices();
  m_nbVertices = io.nbVertices();
//  assembleMassAndStiffnessMatrix();
//  assembleMassBoundary();
  /* Useful indexes */
  m_indexes.resize(m_nbDofs);
  for (int i = 0; i < m_nbDofs; i++){
    m_indexes[i] = i;
  }
  for (int partId = 0; partId < geo.boundaryNodes().size(); partId++){
    for (int j = 0; j < geo.boundaryNodes()[partId].size(); j++){
      m_indexesBoundary.push_back(geo.boundaryNodes()[partId][j]);
    }
  }
  /* Configure solver */
  code = KSPCreate(PETSC_COMM_WORLD, &m_solver); CHKERR(code); 
  code = KSPSetOperators(m_solver, m_stiff, m_stiff); CHKERR(code); 
  code = KSPSetType(m_solver, KSPPREONLY); CHKERR(code); 
  code = KSPGetPC(m_solver, &m_pc); CHKERR(code); 
  code = PCSetType(m_pc, PCLU); CHKERR(code); 
  code = KSPSetFromOptions(m_solver); CHKERR(code); 
  KSPMonitorSet(m_solver, MyKSPMonitor, NULL, 0);
}

void CFD::initialize(Parameters parameters, const Geometry & geometry, const IO & inputOutput){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "CFD: Initializing." << endl;
  par = parameters;
  geo = geometry;
  io = inputOutput;
  m_dimension = io.dimension();
  m_interpolator.initialize(par, geo);
  assert(m_dimension == 2 or m_dimension == 3);
  m_nbDofs = m_dimension*io.nbVertices();
  m_nbVertices = io.nbVertices();
}

void CFD::initialize(Parameters parameters, const Geometry & geometry){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "CFD: Initializing." << endl;
  par = parameters;
  geo = geometry;
  m_nbDofs = (geo.dimension()+1)*geo.nbVertices;
  m_nbDofsVel = geo.dimension()*geo.nbVertices;
  m_nbDofsPress = geo.nbVertices;
  m_nbVertices = geo.nbVertices;
}

//void CFD::assembleMassAndStiffnessMatrix(){
//  /* Initialize finite element */
//  m_tetrahedron.initialize(par);
//  mat(m_mass, m_nbDofs, m_nbDofs);
//  mat(m_stiff, m_nbDofs, m_nbDofs);
//  if (par.mass_matrix_path() == "" || par.stiffness_matrix_path() == ""){
//    cout << "CFD: Assembling mass and stiffness matrix\n";
//    Tic tic;
//    int tetraId = 0; 
//    for (vector<int> tetra : geo.tetrahedron()[0]){ /* loop on tetra */
//      /* get finite element coordinates */
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[tetra[0]];
//      coordinates[1] = geo.coordinates()[tetra[1]];
//      coordinates[2] = geo.coordinates()[tetra[2]];
//      coordinates[3] = geo.coordinates()[tetra[3]];
//      m_tetrahedron.setCoordinates(coordinates);
//      if (tetraId % (geo.tetrahedron()[0].size() / m_verbose) == 0){
//        cout << "  * * * * * * * * * * * * * * * " << endl;
//        cout << "  Nodes: " << endl << tetra << endl;
//        cout << "  Coordinates: " << endl << coordinates << endl;
//        cout << "  Elementary matrix for tetra: " << tetraId << " of " << (geo.tetrahedron()[0].size() - 1) / m_verbose << endl;
//        cout << "  det(J) = " << m_tetrahedron.detJacobian() << endl; 
//        cout << "  Mass(1,2) = " << m_tetrahedron.mass(1,2) << endl;
//        cout << "  Stiff(1,2) = " << m_tetrahedron.stiffness(1,2) << endl;
//      }
//      /* Assemble elementary matrices */
//      for (int j = 0; j < 4; j++){
//        for (int i = 0; i < 4; i++){
//          /* mass */
//          double Mij = m_tetrahedron.mass(i,j);
//          /* stiffness */
//          double Kij = m_tetrahedron.stiffness(i,j) ;
//          for (int comp = 0; comp < 3; comp++){
//            code = MatSetValue(m_mass, 3*tetra[i]+comp, 3*tetra[j]+comp, Mij, ADD_VALUES); CHKERR(code);
//            code = MatSetValue(m_stiff, 3*tetra[i]+comp, 3*tetra[j]+comp, Kij, ADD_VALUES); CHKERR(code);
//          }
//        }
//      }
//      tetraId++;
//    }
//    cout << endl << "Time elapsed: " << tic.toc() << " sec. "<< endl;
//    MatAssemblyBegin(m_mass, MAT_FINAL_ASSEMBLY); 
//    MatAssemblyEnd(m_mass, MAT_FINAL_ASSEMBLY);
//    MatAssemblyBegin(m_stiff, MAT_FINAL_ASSEMBLY);
//    MatAssemblyEnd(m_stiff, MAT_FINAL_ASSEMBLY);
//    saveMat(m_mass, par.dirResults() + "/mass.bin");
//    saveMat(m_stiff, par.dirResults() + "/stiff.bin");
//  } else {
//    loadMat(m_mass, par.mass_matrix_path());
//    loadMat(m_stiff, par.stiffness_matrix_path());
//  }
//}
//
//Vec CFD::divergence(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  return divergence(velocity);
//}

//Vec CFD::divergence(Vec u){
//  cout << "CFD: Computing divergence." << endl;
//  vector<double> divergence_P0(geo.nbTetrahedra(), 0.0);
//  MasterTetrahedron tet;
//  tet.initialize(par);
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){
//    cout << "  Part " << partId << endl;
//    for (int tetraId=0; tetraId<geo.tetrahedron()[partId].size(); tetraId++){
//      if (tetraId % (geo.tetrahedron()[partId].size() / m_verbose) == 0){
//        cout << "  Tetra " << tetraId << " of " << geo.tetrahedron()[partId].size() << endl;
//      }
//      /* Set finite element */
//      vector<int> tetra = geo.tetrahedron()[partId][tetraId];
//      vector<vector<double>> coord(4);
//      coord[0] = geo.coordinates()[tetra[0]];
//      coord[1] = geo.coordinates()[tetra[1]];
//      coord[2] = geo.coordinates()[tetra[2]];
//      coord[3] = geo.coordinates()[tetra[3]];
//      tet.setCoordinates(coord);
//      /* Loop on tetra nodes  */
//      for (int dof_i = 0; dof_i < 4; dof_i++){
//        vector<double> u_node(3); 
//        code = VecGetValues(u, 3, &rango(3*tetra[dof_i], 3*tetra[dof_i]+2)[0], &u_node[0]); CHKERR(code);
//        divergence_P0[tetraId] += dot(u_node, (transpose(tet.jacobianInverse())*tet.grad_phi(dof_i)) );
//      }
//    }
//  }
//  return m_interpolator.interpolateP0_P1_scalar(divergence_P0);
//}

//Vec CFD::gradient(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  return gradient(velocity);
//}

Vec CFD::gradient_scalar(Vec u){

  if (m_world_rank == 0 ) cout << "Computing gradient." << endl;
  MasterElement fe;
  fe.initialize(par, geo.dimension());

  vector<double> grad_P0; 
  vector<vector<vector<int>>> elements;
  if (geo.dimension() == 2){
    elements = geo.triangles();
    grad_P0.resize(geo.dimension() * geo.nbTriangles());
  } else {
    elements = geo.tetrahedron();
    grad_P0.resize(geo.dimension() * geo.nbTetrahedra());
  }
  int nbNodesPerElement = geo.dimension()+1;
  Vec u_seq = getSequential(u);
  for (int partId = 0; partId < elements.size(); partId++){
    for (int feId=0; feId < elements[partId].size(); feId++){
      if (feId % (elements[partId].size()/m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Element " << feId << " of " << elements[partId].size() << ".\n";
      }
      /* Set finite element */
      vector<int> simplex = elements[partId][feId];
      vector<vector<double>> coord(nbNodesPerElement);
      for (int idNode = 0; idNode < nbNodesPerElement; idNode++){
        coord[idNode] = geo.coordinates()[simplex[idNode]];
      }

      fe.setCoordinates(coord);
      /* Get element velocity */
      vector<double> u_element(nbNodesPerElement);
      vector<int> u_element_id(nbNodesPerElement);
      for (int nodeId = 0; nodeId < nbNodesPerElement; nodeId++){
        u_element_id[nodeId] = simplex[nodeId];
      }
      code = VecGetValues(u_seq, nbNodesPerElement, &u_element_id[0], &u_element[0]); CHKERR(code);
      /* Compute gradient */
      vector<double> grad_u(geo.dimension(), 0.0);
      for (int dof_i = 0; dof_i < nbNodesPerElement; dof_i++){
        grad_u = grad_u + u_element[dof_i] * fe.grad_phi(dof_i);
      }

      grad_u = transpose(fe.jacobianInverse()) * grad_u;
      for (int comp = 0; comp < geo.dimension(); comp++){
        grad_P0[geo.dimension()*feId+comp] = grad_u[comp];
      }
    }
  }
  return m_interpolator.interpolateP0_P1(grad_P0);
}

//Vec CFD::wallShearStress(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  if (m_dimension == 3){
//    return wallShearStress(velocity);
//  } else {
//    return wallShearStress2D(velocity);
//  }
//}
//
//Vec CFD::wallShearStress(Vec velocity){
//  /* wall shear stress P0 and P1 */
//  vector<double> wss_P0(3*geo.nbElementsBoundary());
//  vector<double> normal_field_P0(3*geo.nbElementsBoundary());
//  Vec wss = vec(m_nbDofs);
//  Vec normal_field = vec(m_nbDofs);
//
//  MasterTetrahedron tetra_wss;
//  tetra_wss.initialize(par);
//
//  MasterTriangle tria_wss;
//  tria_wss.initialize(par);
//
//  for (int partId = 0; partId < geo.triangles().size(); partId++){ /* loop over boundary parts */
//
//    cout << "CFD: Computing wall shear stress over part: " << partId << endl;
//    for (int i = 0; i < geo.triangles()[partId].size(); i++ ){ /* loop over boundary elements */
//      /* get id of associated tetrahedra*/ 
//      vector<int> tetra = geo.getTetraFromTriangle(geo.triangles()[partId][i]);
// 
//      /* get velocity field on tetrahedra */ 
//      vector<double> velocity_element(12);
//      vector<PetscInt> velocity_element_id(12);
//      for (int dof_i = 0; dof_i < 4; dof_i++){
//        for (int comp = 0; comp < 3; comp++){
//          velocity_element_id[3*dof_i+comp] = 3*geo.tetrahedron()[tetra[0]][tetra[1]][dof_i] + comp;
//        }
//      }
//      VecGetValues(velocity, 12, &velocity_element_id[0], &velocity_element[0]);
//
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][0]];
//      coordinates[1] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][1]];
//      coordinates[2] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][2]];
//      coordinates[3] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][3]];
//      tetra_wss.setCoordinates(coordinates);
//
//      /* Compute P0 gradient */
//      vector<vector<double>> grad_u(3);
//      grad_u[0].resize(3, 0.0);
//      grad_u[1].resize(3, 0.0);
//      grad_u[2].resize(3, 0.0);
//      for (int dof_i = 0; dof_i < 4; dof_i++){
//        for (int comp = 0; comp < 3; comp++){
//          grad_u[0][comp] += velocity_element[3*dof_i]   * dot(getRow(tetra_wss.jacobianInverse(), comp), tetra_wss.grad_phi(dof_i));
//          grad_u[1][comp] += velocity_element[3*dof_i+1] * dot(getRow(tetra_wss.jacobianInverse(), comp), tetra_wss.grad_phi(dof_i));
//          grad_u[2][comp] += velocity_element[3*dof_i+2] * dot(getRow(tetra_wss.jacobianInverse(), comp), tetra_wss.grad_phi(dof_i));
//        }
//      }
//      grad_u = transpose(grad_u);
//
//      /* Compute stress tensor */
//      vector<vector<double>> stress;
//      stress = (grad_u + transpose(grad_u))/2;
//
//      /* Compute normal */
//      vector<vector<double>> tria_coordinates(3);
//      tria_coordinates[0] = geo.coordinates()[ geo.triangles()[partId][i][0] ];
//      tria_coordinates[1] = geo.coordinates()[ geo.triangles()[partId][i][1] ];
//      tria_coordinates[2] = geo.coordinates()[ geo.triangles()[partId][i][2] ];
//      tria_wss.setCoordinates(tria_coordinates);     
//      tria_wss.computeNormal();
//      vector<double> n = tria_wss.normal();
//
//      /* Compute WSS P0 */
//      int id_tria;
//      if (partId == 0){
//        id_tria = i;
//      } else {
//        id_tria = i + geo.triangles()[partId-1].size();
//      }
//
//      wss_P0[3*id_tria]   = (1 - n[0]*n[0]) * ( stress[0][0]*n[0] + stress[0][1]*n[1] + stress[0][2]*n[2] )
//                          + n[0]*n[1]       * ( stress[1][0]*n[0] + stress[1][1]*n[1] + stress[1][2]*n[2] )
//                          + n[0]*n[2]       * ( stress[2][0]*n[0] + stress[2][1]*n[1] + stress[2][2]*n[2] );
//
//      wss_P0[3*id_tria+1] = n[1]*n[0]       * ( stress[0][0]*n[0] + stress[0][1]*n[1] + stress[0][2]*n[2] )
//                          + (1 - n[1]*n[1]) * ( stress[1][0]*n[0] + stress[1][1]*n[1] + stress[1][2]*n[2] )
//                          + n[1]*n[2]       * ( stress[2][0]*n[0] + stress[2][1]*n[1] + stress[2][2]*n[2] );
//
//      wss_P0[3*id_tria+2] = n[2]*n[0]       * ( stress[0][0]*n[0] + stress[0][1]*n[1] + stress[0][2]*n[2] )
//                          + n[2]*n[1]       * ( stress[1][0]*n[0] + stress[1][1]*n[1] + stress[1][2]*n[2] )
//                          + (1 - n[2]*n[2]) * ( stress[2][0]*n[0] + stress[2][1]*n[1] + stress[2][2]*n[2] );
//
//      normal_field_P0[3*id_tria]   = n[0];
//      normal_field_P0[3*id_tria+1] = n[1];
//      normal_field_P0[3*id_tria+2] = n[2];
//
//    }
//
//  }
//  /* Interpolate WSS to P1 */
//  return m_interpolator.interpolateP0_P1_boundary(wss_P0, geo.dimension());
//}

//Vec CFD::wallShearStress2D(Vec velocity){
//  exit(1);
//}
//
//void CFD::assembleMassBoundary(){
//  m_triangle_mass.initialize(par);
//  mat(m_massBD, m_nbDofsVel, m_nbDofsVel);
//  if (par.mass_boundary_matrix_path() == ""){
//    /* Compute matrices only if they are not provided in the parameters file */
//    cout << "CFD: Assembling mass matrix on the boundary \n";
//    for (int partId = 0; partId < geo.triangles().size(); partId++) { /* loop on parts */
//      cout << "  Part: " << partId << endl;
//      int triangleId = 0; 
//      for (vector<int> tria : geo.triangles()[partId]){ /* loop on triangles */
//        if ((triangleId % (geo.triangles()[partId].size() / m_verbose)) == 0){
//          cout << "  Triangle " << triangleId << " of " << geo.triangles()[partId].size() << endl;
//        }
//        /* get finite element coordinates */
//        vector<vector<double>> coordinates(3);
//        coordinates[0] = geo.coordinates()[tria[0]];
//        coordinates[1] = geo.coordinates()[tria[1]];
//        coordinates[2] = geo.coordinates()[tria[2]];
//
//        m_triangle_mass.setCoordinates(coordinates);
//        m_triangle_mass.computeJacobian();
//        m_triangle_mass.computeElementMass();
//
//        /* set values from local to global */
//        for (int i = 0; i < 3; i++){
//          for (int j = 0; j < 3; j++){
//            for (int comp = 0; comp < 3; comp++){
//              MatSetValue(m_massBD, 3*tria[i]+comp, 3*tria[j]+comp, m_triangle_mass.elementMatrix()[3*i+comp][3*j+comp], ADD_VALUES);
//            }
//          }
//        }
//        triangleId++;
//      }
//    }
//    cout << "CFD: Assembling global matrix" << endl;
//    MatAssemblyBegin(m_massBD, MAT_FINAL_ASSEMBLY); 
//    MatAssemblyEnd(m_massBD, MAT_FINAL_ASSEMBLY);
////    saveMat(m_massBD, par.dirResults() + "/massBD.bin");
//  } else {
//    loadMat(m_massBD, par.mass_boundary_matrix_path());
//  }
//}
//
//vector<double> CFD::computeVectorIntegralOverSurface(Vec u){
//  vector<double> integral(3, 0.0);
//  for (int i = 0; i < geo.triangles().size(); i++){
//    integral = integral + computeVectorIntegralOverSurface(i, u);
//  }
//  return integral;
//}
//
//vector<double> CFD::computeVectorIntegralOverSurface(int labelBD, Vec u){
//  vector<double> integral(3, 0.0);
//  vector<PetscInt> loc2glob(9);
//  vector<PetscReal> u_element(9);
//  MasterTriangle triangle;
//  triangle.initialize(par);
//
//  cout << "CFD: Computing integral over part " << labelBD << endl;
//  vector<vector<double>> feNodes(3); 
//  for (vector<int> tria : geo.triangles()[labelBD]){ /* loop on tria */
//
//    geo.coordinates();
//    feNodes[0] = geo.coordinates()[tria[0]];
//    feNodes[1] = geo.coordinates()[tria[1]];
//    feNodes[2] = geo.coordinates()[tria[2]];
//
//    triangle.setCoordinates(feNodes);
//    triangle.computeJacobian();
//
//    loc2glob[0] = 3*tria[0];
//    loc2glob[1] = loc2glob[0] + 1;
//    loc2glob[2] = loc2glob[0] + 2;
//
//    loc2glob[3] = 3*tria[1];
//    loc2glob[4] = loc2glob[3] + 1;
//    loc2glob[5] = loc2glob[3] + 2;
//
//    loc2glob[6] = 3*tria[2];
//    loc2glob[7] = loc2glob[6] + 1;
//    loc2glob[8] = loc2glob[6] + 2;
//
//    VecGetValues(u, 9, &loc2glob[0], &u_element[0]);
//
//    integral = integral + triangle.computeVectorIntegral(u_element);
//  }
//  cout << "CFD: Int: " << integral << endl;
//  return integral;
//}
//
//double CFD::integralSurface(Vec p, int labelBD){
//
//  double integral = 0.0;
//
//  vector<PetscInt> loc2glob(3);
//  vector<PetscReal> p_element(3);
//
//  MasterTriangle triangle;
//  triangle.initialize(par);
//
//  cout << "CFD: Computing integral over part " << labelBD << endl;
//  vector<vector<double>> feNodes(3); 
//  for (vector<int> tria : geo.triangles(labelBD)){ /* loop on tria */
//
//    feNodes[0] = geo.coordinates()[tria[0]];
//    feNodes[1] = geo.coordinates()[tria[1]];
//    feNodes[2] = geo.coordinates()[tria[2]];
//
//    triangle.setCoordinates(feNodes);
//    triangle.computeJacobian();
//
//    loc2glob[0] = tria[0];
//    loc2glob[1] = tria[1];
//    loc2glob[2] = tria[2];
//
//    VecGetValues(p, 3, &loc2glob[0], &p_element[0]);
//
//    integral = integral + triangle.computeScalarIntegral(p_element);
//  }
//
//  cout << "CFD: int: " << scientific << integral << endl;
// 
//  return integral;
//}

//double CFD::flow(Vec phi, int labelBD){
//
//  int phi_size;
//  code = VecGetSize(phi, &phi_size); CHKERR(code);
//
//  Vec phi_seq;
//  VecCreateSeq(PETSC_COMM_SELF, phi_size, &phi_seq);
//  code = VecSetFromOptions(phi_seq); CHKERR(code); 
//  VecScatter inctx;
//  code = VecScatterCreateToAll(phi, &inctx, NULL); CHKERR(code);
//  code = VecScatterBegin(inctx, phi, phi_seq, INSERT_VALUES, SCATTER_FORWARD); CHKERR(code);
//  code = VecScatterEnd(inctx, phi, phi_seq, INSERT_VALUES, SCATTER_FORWARD); CHKERR(code);
//  code = VecScatterDestroy(&inctx); CHKERR(code);
//
//  Vec u;
//  VecCreateSeq(PETSC_COMM_SELF, geo.nbVertices*3, &u);
//  code = VecSetFromOptions(u); CHKERR(code); 
//
//  int nbDofsVel = geo.nbVertices * 3;
//
//  if (phi_size == 4*geo.nbVertices){
//    double u_array[nbDofsVel];
//    code = VecGetValues(phi_seq, nbDofsVel, &range(nbDofsVel)[0], u_array); CHKERR(code);
//    code = VecSetValues(u, nbDofsVel, &range(nbDofsVel)[0], u_array, INSERT_VALUES); CHKERR(code);
//    code = VecAssemblyBegin(u); CHKERR(code);
//    code = VecAssemblyEnd(u); CHKERR(code);
//  } else {
//    VecCopy(phi_seq, u);
//  }
//
//  double integral = 0.0;
//  
//  vector<int> loc2glob(9);
//  vector<double> u_element(9);
//
//  MasterTriangle triangle;
//  triangle.initialize(par);
//
//  vector<vector<double>> feNodes(3); 
//  for (vector<int> tria : geo.triangles(labelBD)){ /* loop on tria */
//
//    geo.coordinates();
//    feNodes[0] = geo.coordinates()[tria[0]];
//    feNodes[1] = geo.coordinates()[tria[1]];
//    feNodes[2] = geo.coordinates()[tria[2]];
//
//    triangle.setCoordinates(feNodes);
//    triangle.computeJacobian();
//    triangle.computeNormal();
//
//    loc2glob[0] = 3*tria[0];
//    loc2glob[1] = loc2glob[0] + 1;
//    loc2glob[2] = loc2glob[0] + 2;
//
//    loc2glob[3] = 3*tria[1];
//    loc2glob[4] = loc2glob[3] + 1;
//    loc2glob[5] = loc2glob[3] + 2;
//
//    loc2glob[6] = 3*tria[2];
//    loc2glob[7] = loc2glob[6] + 1;
//    loc2glob[8] = loc2glob[6] + 2;
//
//    code = VecGetValues(u, 9, &loc2glob[0], &u_element[0]); CHKERR(code);
//    integral += triangle.computeFlow(u_element);
//  }
//  
//
//  return integral;
//}

///* from P0 field defined on boundary triangles to P1 */
//Vec CFD::interpolateP0_P1_boundary(vector<double> u){
//
//  Vec v = vec(m_nbDofs);
//
//  MasterTriangle triaFE;
//  triaFE.initialize(par);
//
//  for (int partId = 0; partId < geo.boundaryNodes().size(); partId++){ /* loop over boundary parts */
//    for (int i = 0; i < geo.boundaryNodes()[partId].size(); i++){ /* loop over boundary nodes */
//
//      vector<vector<int>> triangles = geo.getTriaNeighbourds(geo.boundaryNodes()[partId][i]); 
//
//      vector<double> u_el(3);
//      u_el[0] = 0.0;
//      u_el[1] = 0.0;
//      u_el[2] = 0.0;
//      double area_neighbourds = 0.0;
//
//      for (vector<int> neighbourd : triangles){
//
//        /* compute triangle area */
//        vector<vector<double>> tria_coordinates(3);
//        tria_coordinates[0] = geo.coordinates()[ geo.triangles()[neighbourd[0]][neighbourd[1]][0] ];
//        tria_coordinates[1] = geo.coordinates()[ geo.triangles()[neighbourd[0]][neighbourd[1]][1] ];
//        tria_coordinates[2] = geo.coordinates()[ geo.triangles()[neighbourd[0]][neighbourd[1]][2] ];
//
//        triaFE.setCoordinates(tria_coordinates);
//        triaFE.computeArea();
//
//        int nodeId;
//        if (neighbourd[0] == 0){
//          nodeId = neighbourd[1];
//        } else {
//          nodeId = neighbourd[1] + geo.triangles()[neighbourd[0]-1].size();
//        }
//        u_el[0] += triaFE.area()*u[3*nodeId];
//        u_el[1] += triaFE.area()*u[3*nodeId+1];
//        u_el[2] += triaFE.area()*u[3*nodeId+2];
//        area_neighbourds += triaFE.area();
//      }
//      u_el[0] = u_el[0]/area_neighbourds;
//      u_el[1] = u_el[1]/area_neighbourds;
//      u_el[2] = u_el[2]/area_neighbourds;
//      vector<int> indexes(3);
//      indexes[0] = 3*geo.boundaryNodes()[partId][i];
//      indexes[1] = 3*geo.boundaryNodes()[partId][i]+1;
//      indexes[2] = 3*geo.boundaryNodes()[partId][i]+2;
//      VecSetValues(v, 3, &indexes[0], &u_el[0], INSERT_VALUES);
//    }
//  }
//  VecAssemblyBegin(v);
//  VecAssemblyEnd(v);
//  return v;
//}
//
//
//Vec CFD::interpolateP0_P1(vector<double> u){
//  cout << "CFD: Interpolating field to P1" << endl;
//  if (u.size() == geo.nbTetrahedra()){
//    return interpolateP0_P1_scalar(u);
//  } else if (u.size() == (geo.nbTetrahedra() * 3)){
//    return interpolateP0_P1_vector(u);
//  } else {
//    errorMessage("interpolateP0_P1", "Size of the vector does not match scalar nor vector field");
//  }
//}
//
//Vec CFD::interpolateP0_P1_2D(vector<double> u){
//  cout << "CFD: Interpolating field to P1" << endl;
//  if (u.size() == geo.nbTriangles()){
//    return interpolateP0_P1_scalar_2D(u);
//  } else if (u.size() == (geo.nbTriangles() * 2)){
//    return interpolateP0_P1_vector_2D(u);
//  } else if (u.size() == (geo.nbTriangles() * 3)){
//    return interpolateP0_P1_vector_2D_3D(u);
//  } else {
//    errorMessage("interpolateP0_P1", "Size of the vector does not match scalar nor vector field");
//  }
//}
//
//Vec CFD::interpolateP0_P1_scalar(vector<double> u){
//  Vec v = vec(m_nbVertices);
//  for (int i = 0; i < geo.coordinates().size(); i++){ 
//    if (i % (geo.coordinates().size() / m_verbose ) == 0){
//      cout << "  Node " << i << " of " << geo.coordinates().size() << endl;
//    }
//    vector<vector<int>> neighbours = geo.getTetraNeighbourhood(i);
//    vector<int> tetraSet = neighbours[0];
//    vector<int> partIds = neighbours[2];
//    double u_node = 0.0;
//    double volume_neighbourds = 0.0;
//    for (int j = 0; j < tetraSet.size(); j++){ /* Loop over tetrahedras */
//      vector<vector<double>> coord(4);
//      coord[0] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][0]];
//      coord[1] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][1]];
//      coord[2] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][2]];
//      coord[3] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][3]];
//      double volume = 1.0/6.0 * abs(dot(coord[0] - coord[3], cross((coord[1] - coord[3]), (coord[2] - coord[3]))));
//      u_node += volume*u[tetraSet[j]];
//      volume_neighbourds += volume;
//    }
//    u_node = u_node/volume_neighbourds;
//    VecSetValue(v, i, u_node, INSERT_VALUES);
//  }
//  VecAssemblyBegin(v);
//  VecAssemblyEnd(v);
//  return v;
//}
//
//Vec CFD::interpolateP0_P1_vector(vector<double> u){
//  Vec v = vec(m_nbDofs);
//  for (int i = 0; i < geo.coordinates().size(); i++){ 
// 
//    if (i % (geo.coordinates().size()/m_verbose) == 0){
//      cout << "  Node " << i << " of " << geo.coordinates().size() -1 << endl;
//    }
//    vector<vector<int>> neighbours = geo.getTetraNeighbourhood(i);
//    vector<int> tetraSet = neighbours[0];
//    vector<int> partIds = neighbours[2];
//
//    vector<double> u_el(3);
//    u_el[0] = 0.0;
//    u_el[1] = 0.0;
//    u_el[2] = 0.0;
//    double volume_neighbourds = 0.0;
//    for (int j = 0; j < tetraSet.size(); j++){ /* Loop over tetrahedras */
//      vector<vector<double>> coord(4);
//      coord[0] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][0]];
//      coord[1] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][1]];
//      coord[2] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][2]];
//      coord[3] = geo.coordinates()[geo.tetrahedron(partIds[j])[tetraSet[j]][3]];
//      double volume = 1.0/6.0 * abs(dot(coord[0] - coord[3], cross((coord[1] - coord[3]), (coord[2] - coord[3]))));
//
//      u_el[0] += volume*u[3*tetraSet[j]];
//      u_el[1] += volume*u[3*tetraSet[j]+1];
//      u_el[2] += volume*u[3*tetraSet[j]+2];
//      volume_neighbourds += volume;
//  
//    }
//
//    u_el[0] = u_el[0]/volume_neighbourds;
//    u_el[1] = u_el[1]/volume_neighbourds;
//    u_el[2] = u_el[2]/volume_neighbourds;
//
//    VecSetValue(v, 3*i, u_el[0], INSERT_VALUES);
//    VecSetValue(v, 3*i+1, u_el[1], INSERT_VALUES);
//    VecSetValue(v, 3*i+2, u_el[2], INSERT_VALUES);
//  }
//  VecAssemblyBegin(v);
//  VecAssemblyEnd(v);
//  return v;
//}
//
//Vec CFD::interpolateP0_P1_vector_2D_3D(vector<double> u){
//  Vec v = vec(geo.coordinates().size()*3);
//  for (int i = 0; i < geo.coordinates().size(); i++){ 
// 
//    if (i % (geo.coordinates().size()/m_verbose) == 0){
//      cout << "  Node " << i << " of " << geo.coordinates().size() -1 << endl;
//    }
//    vector<vector<int>> neighbours = geo.getTriaNeighbourhood(i);
//    vector<int> triaSet = neighbours[0];
//    vector<int> partIds = neighbours[2];
//
//    vector<double> u_el(3);
//    u_el[0] = 0.0;
//    u_el[1] = 0.0;
//    u_el[2] = 0.0;
//    double area_neighbourds = 0.0;
//    for (int j = 0; j < triaSet.size(); j++){ /* Loop over tetrahedras */
//      vector<vector<double>> coord(3);
//      coord[0] = geo.coordinates()[geo.triangles(partIds[j])[triaSet[j]][0]];
//      coord[1] = geo.coordinates()[geo.triangles(partIds[j])[triaSet[j]][1]];
//      coord[2] = geo.coordinates()[geo.triangles(partIds[j])[triaSet[j]][2]];
////      double area = 1.0/6.0 * abs(dot(coord[0] - coord[3], cross((coord[1] - coord[3]), (coord[2] - coord[3]))));
//      double area = 0.5 * cross(coord[0] - coord[1], coord[0] - coord[2]);
//
//      u_el[0] += area*u[3*triaSet[j]];
//      u_el[1] += area*u[3*triaSet[j]+1];
//      u_el[2] += area*u[3*triaSet[j]+2];
//      area_neighbourds += area;
//  
//    }
//
//    u_el[0] = u_el[0]/area_neighbourds;
//    u_el[1] = u_el[1]/area_neighbourds;
//    u_el[2] = u_el[2]/area_neighbourds;
//
//    VecSetValue(v, 3*i, u_el[0], INSERT_VALUES);
//    VecSetValue(v, 3*i+1, u_el[1], INSERT_VALUES);
//    VecSetValue(v, 3*i+2, u_el[2], INSERT_VALUES);
//  }
//  VecAssemblyBegin(v);
//  VecAssemblyEnd(v);
//  return v;
//}

/* 
    \Delta phi = 0
    phi = u on \partial \Omega
*/
Vec CFD::harmonicExtension(Vec u){
  vector<PetscInt> bdNodes;
  for (int i = 0; i < geo.boundaryNodes().size(); i++){
    for (int j = 0; j < geo.boundaryNodes()[i].size(); j++){
      bdNodes.push_back( 3*geo.boundaryNodes()[i][j] );
      bdNodes.push_back( 3*geo.boundaryNodes()[i][j]+1 );
      bdNodes.push_back( 3*geo.boundaryNodes()[i][j]+2 );
    }
  }
  MatZeroRows(m_stiff, bdNodes.size(), &bdNodes[0], 1.0, NULL, NULL);
  /* RHS*/
  cout << "CFD: Computing harmonic extension." << endl;
  Vec x = vec(m_nbDofs);
  configureKSP(m_solver, m_stiff, "preonly", "lu");
  KSPSolve(m_solver, u, x);
  return x;
}

/* 
      \Delta phi = 0 
      \grad phi\cdot n = g on \partial \Omega
*/
//Vec CFD::harmonicExtensionNeumann(Vec g){
//  
//  /* Force solution to live in a zero mean space */
//  vector<double> intGonBoundary = computeVectorIntegralOverSurface(g);
//  Vec average = vec(m_nbDofs);
//  PetscInt indexes_average[m_nbDofs];
//  PetscReal array_average[m_nbDofs];
//  for (int i = 0; i <  m_nbVertices; i++){
//    for (int comp = 0; comp < 3; comp++){
//      indexes_average[3*i+comp] = 3*i+comp;
//      array_average[3*i+comp] = intGonBoundary[comp];
//    }
//  }
//  VecSetValues(average, m_nbDofs, indexes_average, array_average, INSERT_VALUES);
//  VecAssemblyBegin(average); 
//  VecAssemblyEnd(average); 
//
//  /* only keep the trace */
//  average = trace(average);
//
//  PetscReal measureBoundary = geo.computeBoundaryArea();
//  VecAXPY(g, -1.0/measureBoundary, average);
//
//  /* RHS */
//  cout << "CFD: Computing harmonic extension Neumann." << endl;
//  Vec rhs = vec(m_nbDofs);
//  Vec x = vec(m_nbDofs);
//  MatMult(m_massBD, g, rhs);
//  KSPSolve(m_solver, rhs, x);
//   
//  VecDestroy(&rhs);
//  VecDestroy(&average);
//
//  return x;
//}

Vec CFD::trace(Vec u){

  cout << "CFD: Computing trace" << endl;

  vector<double> u_array(m_nbDofs);
  VecGetValues(u, m_indexes.size(), &m_indexes[0], &u_array[0]);

  Vec tr_u = zeros(m_nbDofs);
  vector<double> tr_u_array;
  for (int partId = 0; partId < geo.boundaryNodes().size(); partId++){
    for (int j = 0; j < geo.boundaryNodes()[partId].size(); j++){
      tr_u_array.push_back(u_array[geo.boundaryNodes()[partId][j]]); 
    }
  }
  VecSetValues(tr_u, m_indexesBoundary.size(), &m_indexesBoundary[0], &tr_u_array[0], INSERT_VALUES);
  VecAssemblyBegin(tr_u);
  VecAssemblyEnd(tr_u);

  return tr_u;
}

//Vec CFD::vorticity(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  if (m_dimension == 3){
//    return vorticity(velocity);
//  } else {
////    return vorticity2D(velocity);
//  }
//}
//
//Vec CFD::vorticity2D(Vec u){
//
//  vector<double> vort_P0(3*geo.nbTriangles());
//
//  vector<vector<double>> diagU(3);
//  diagU[0].resize(2, 0.0);
//  diagU[1].resize(2, 0.0);
//
//  MasterTriangle2D tria;
//  tria.initialize(par, 2);
//
//  vector<vector<vector<double>>> grad_phi(3);
//  for (int dof_i = 0; dof_i < 3; dof_i++){
//    grad_phi[dof_i].resize(2);
//    grad_phi[dof_i][0] = tria.grad_phi(dof_i);
//    grad_phi[dof_i][1] = tria.grad_phi(dof_i);
//    grad_phi[dof_i] = transpose(grad_phi[dof_i]); 
//  }
//
//  int triaId = 0;
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){
//
//    cout << "CFD: computing vorticity over part: " << geo.triangleLabels()[ partId ] << endl;
//
//    for (vector<int> triangle : geo.triangles()[partId]){
//
//      cout << "CFD: vorticity triangle " << triaId << " / " << geo.triangles()[partId].size() << "\r";
//
//      /* Set finite element */
//      vector<vector<double>> coord(3);
//      coord[0] = geo.coordinates()[triangle[0]];
//      coord[1] = geo.coordinates()[triangle[1]];
//      coord[2] = geo.coordinates()[triangle[2]];
//      tria.setCoordinates(coord);
//
//      /* Get element velocity */
//      vector<double> u_element(6);
//      vector<PetscInt> u_element_id(6);
//      for (int dof_i = 0; dof_i < 3; dof_i++){
//        for (int comp = 0; comp < 2; comp++){
//          u_element_id[2*dof_i+comp] = 2*triangle[dof_i] + comp;
//        }
//      }
//      VecGetValues(u, 6, &u_element_id[0], &u_element[0]);
//
//      /* Compute gradient */
//      vector<vector<double>> grad_u(2);
//      grad_u[0].resize(2, 0.0);
//      grad_u[1].resize(2, 0.0);
//      for (int dof_i = 0; dof_i < 3; dof_i++){
//        for (int comp = 0; comp < 2; comp++){
//          diagU[comp][comp] = u_element[2*dof_i+comp];
//        }
//        grad_u = grad_u + grad_phi[dof_i]*diagU;
//      }
//      grad_u = transpose(tria.jacobianInverse()) * grad_u;
//
//      vort_P0[3*triaId]   = 0.0;
//      vort_P0[3*triaId+1] = 0.0;
//      vort_P0[3*triaId+2] = grad_u[0][1] - grad_u[1][0];
//
//      triaId++;
//    }
//    cout << endl;
//  }
//  return m_interpolator.interpolateP0_P1_2D(vort_P0);
//}

Vec CFD::vorticity(Vec u){

  vector<vector<vector<int>>> elements;
  vector<double> vort_P0;
  int nbComp;
  int nbNodesPerElement;

  if (geo.dimension() == 2){
    elements = geo.triangles();
    nbComp = 2;
    nbNodesPerElement = 3;
    vort_P0.resize(elements.size());
  } else {
    elements = geo.tetrahedron();
    nbComp = 3;
    nbNodesPerElement = 4;
    vort_P0.resize(elements.size()*3);
  }
  
  vector<vector<double>> diagU(nbComp);
  for (int comp = 0; comp < nbComp; comp++){
    diagU[comp].resize(nbComp, 0.0);
  }

  MasterElement fe;
  fe.initialize(par, geo.dimension());
  vector<vector<vector<double>>> grad_phi(nbNodesPerElement);
  for (int dof_i = 0; dof_i < nbNodesPerElement; dof_i++){
    grad_phi[dof_i].resize(nbComp);
    for (int comp = 0; comp < nbComp; comp++){
      grad_phi[dof_i][comp] = fe.grad_phi(dof_i);
    }
    grad_phi[dof_i] = transpose(grad_phi[dof_i]); 
  }
  Vec u_seq = getSequential(u);

  int feId = 0;
  for (int partId = 0; partId < elements.size(); partId++){

    for (vector<int> simplex : elements[partId]){

//      if (feId % (elements[partId].size() / 5) == 0 && m_world_rank == 0) cout << "CFD: vorticity simplex " << feId << " / " << elements[partId].size() << "\n";
      if (m_world_rank == 0) cout << "CFD: vorticity simplex " << feId << " / " << elements[partId].size() << "\n";
      /* Set finite element */
      vector<vector<double>> coord(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coord[i] = geo.coordinates()[simplex[i]];
      }
      fe.setCoordinates(coord);

      /* Get element velocity */
      int nbDofsPerElement = nbComp*nbNodesPerElement;
      vector<double> u_element(nbDofsPerElement);
      vector<int> u_element_id(nbDofsPerElement);
      for (int dof_i = 0; dof_i < nbNodesPerElement; dof_i++){
        for (int comp = 0; comp < nbComp; comp++){
          u_element_id[nbComp*dof_i+comp] = nbComp*simplex[dof_i] + comp;
        }
      }
      code = VecGetValues(u_seq, nbDofsPerElement, &u_element_id[0], &u_element[0]); CHKERR(code);

      /* Compute gradient */
      vector<vector<double>> grad_u(nbComp);
      for (int comp = 0; comp < nbComp; comp++){
        grad_u[comp].resize(nbComp, 0.0);
      }

      for (int dof_i = 0; dof_i < nbNodesPerElement; dof_i++){
        for (int comp = 0; comp < nbComp; comp++){
          diagU[comp][comp] = u_element[nbComp*dof_i+comp];
        }
        grad_u = grad_u + grad_phi[dof_i]*diagU;
      }
      grad_u = transpose(fe.jacobianInverse()) * grad_u;

      if (geo.dimension() == 3){
        vort_P0[3*feId]   = grad_u[1][2] - grad_u[2][1];
        vort_P0[3*feId+1] = grad_u[0][2] - grad_u[2][0];
        vort_P0[3*feId+2] = grad_u[0][1] - grad_u[1][0];
      } else {
        vort_P0[feId] = grad_u[0][1] - grad_u[1][0];
      }

      feId++;
    }
    cout << endl;
  }
  return m_interpolator.interpolateP0_P1(vort_P0);
}
//
//vector<double> CFD::pressureDropVirtualWork(string velocity_path_u, string velocity_path_u0){
//  /* load velocity field */ 
//  Vec u = vec(m_nbDofs);
//  Vec u0 = vec(m_nbDofs);
//  loadVec(u, velocity_path_u);
//  loadVec(u0, velocity_path_u0);
//  return pressureDropVirtualWork(u, u0);
//}
//
//vector<double> CFD::pressureDropVirtualWork(Vec u, Vec u0){
//
//  cout << "CFD: computing pressure drop from virtual work principle." << endl;
//
//  int inlet = par.bdLabels(0);
//  vector<int> outlets(par.bdLabels().size()-1);
//  for (int i = 1; i < par.bdLabels().size(); i++){
//    outlets[i-1] = par.bdLabels(i);
//  }
//
//  /* load test functions */
//  vector<Vec> v(outlets.size());
//  for (int i = 0; i < outlets.size(); i++){
//    vec(v[i], m_nbDofs);
//    loadVec(v[i], par.dirModel() + "/test." + wildcard(i) + ".vct");   
//  }
//
//  cout << "  Assembling flux matrix." << endl;
//  Mat F = mat(outlets.size(), outlets.size());
//  for (int i = 0; i < outlets.size(); i++){ 
//    for (int j = 0; j < outlets.size(); j++){ 
//      code = MatSetValue(F, i, j, flow(v[i], outlets[j]), INSERT_VALUES); CHKERR(code);
//    }
//  }
//  MatAssemblyBegin(F, MAT_FINAL_ASSEMBLY);
//  MatAssemblyEnd(F, MAT_FINAL_ASSEMBLY);
//
//  /* Mid-point evaluation in time */
//  Vec u_m = zeros(m_nbDofs);
//  VecAXPY(u_m, 0.5, u);
//  VecAXPY(u_m, 0.5, u0);
//
//  MasterTetrahedron tetrahedron;
//  tetrahedron.initialize(par);
//  MasterTetrahedron tetra_mat;
//  tetra_mat.initialize(par);
//
//  Tic tic;
//  
//  vector<double> energy(outlets.size());
//
//  vector<vector<double>> element_mat(12);
//  for (int i = 0; i < 12; i++){
//    element_mat[i].resize(12);
//  }
//  vector<vector<double>> element_mass(12);
//  for (int i = 0; i < 12; i++){
//    element_mass[i].resize(12);
//  }
//
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){ /* loop on parts */
//
//    int tetraId = 0;
//    for (vector<int> tetra : geo.tetrahedron()[partId]){ /* loop on tetra */
//
//      cout << tetraId << "/" << geo.tetrahedron()[partId].size() << "\t\r";
//
//      /* get finite element coordinates */
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[tetra[0]];
//      coordinates[1] = geo.coordinates()[tetra[1]];
//      coordinates[2] = geo.coordinates()[tetra[2]];
//      coordinates[3] = geo.coordinates()[tetra[3]];
//
//      tetrahedron.setCoordinates(coordinates);
//     
//      /* loc2glob dof mapping */
//      vector<int> loc2glob(12);
//      for (int i = 0; i < 4; i++){
//        for (int comp = 0; comp < 3; comp++){
//          loc2glob[3*i+comp] = 3*tetra[i]+comp;
//        }
//      }
//
//      /* velocity in the element */
//      vector<double> u_el(12);
//      VecGetValues(u, 12, &loc2glob[0], &u_el[0]);
//      vector<double> um_el(12);
//      VecGetValues(u_m, 12, &loc2glob[0], &um_el[0]);
//      vector<double> u0_el(12);
//      VecGetValues(u0, 12, &loc2glob[0], &u0_el[0]);
//
//      for (int j = 0; j < 4; j++){
//        vector<double> u_node_j(3);
//        u_node_j[0] = u_el[3*j];
//        u_node_j[1] = u_el[3*j+1];
//        u_node_j[2] = u_el[3*j+2];
//        for (int i = 0; i < 4; i++){
//          double Cij = par.density()*tetrahedron.convection(i,j, u_node_j) + par.viscosity()*tetrahedron.stiffness(i,j);
//          double Mij = par.density()*tetrahedron.mass(i,j);
//          for (int comp = 0; comp < 3; comp++){
//            element_mat[3*i+comp][3*j+comp] = Cij;
//            element_mass[3*i+comp][3*j+comp] = Mij;
//          }
//        }
//      }
//
//      for (int idTest = 0; idTest < v.size(); idTest++){
//        /* test function in the element */
//        vector<double> v_el(12);
//        VecGetValues(v[idTest], 12, &loc2glob[0], &v_el[0]);
//        energy[idTest] = energy[idTest] - dot(element_mat * v_el, um_el) - dot((u_el - u0_el) / par.timeStep(), (element_mass * v_el)); 
//      }
//  
//      tetraId++;
//    }
//  }
//
//  MasterTriangle triangle;
//  triangle.initialize(par);
//  
//  for (int partId = 0; partId < geo.triangles().size(); partId++){ /* loop over boundary parts */
//
//    for (int i = 0; i < geo.triangles()[partId].size(); i++ ){ /* loop over boundary elements */
//      /* get id of associated tetrahedra*/ 
//      vector<int> tetra = geo.getTetraFromTriangle(geo.triangles()[partId][i]);
// 
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][0]];
//      coordinates[1] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][1]];
//      coordinates[2] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][2]];
//      coordinates[3] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][3]];
//      tetrahedron.setCoordinates(coordinates);
//
//      /* Compute normal */
//      vector<vector<double>> tria_coordinates(3);
//      tria_coordinates[0] = geo.coordinates()[ geo.triangles()[partId][i][0] ];
//      tria_coordinates[1] = geo.coordinates()[ geo.triangles()[partId][i][1] ];
//      tria_coordinates[2] = geo.coordinates()[ geo.triangles()[partId][i][2] ];
//      triangle.setCoordinates(tria_coordinates);     
//      triangle.computeNormal();
//      vector<double> n = triangle.normal();
//
//      tetrahedron.gradient_phi_i_dot_n_phi_j(n);
//
//      /* Compute viscous energy */
//      vector<double> u_el(12);
//      vector<int> loc2glob(12);
//      for (int dof_i = 0; dof_i < 4; dof_i++){
//        for (int comp = 0; comp < 3; comp++){
//          loc2glob[3*dof_i+comp] = 3*geo.tetrahedron()[tetra[0]][tetra[1]][dof_i] + comp;
//        }
//      }
//
//      VecGetValues(u_m, 12, &loc2glob[0], &u_el[0]);
//      for (int idTest = 0; idTest < v.size(); idTest++){
//        /* test function in the element */
//        vector<double> v_el(12);
//        VecGetValues(v[idTest], 12, &loc2glob[0], &v_el[0]);
//        energy[idTest] += par.viscosity() * dot(tetrahedron.matrix() * v_el, u_el); 
//      }
//      tetrahedron.clear();
//    }
//  }
//  cout << "Time elapsed in energy assembly: " << tic.toc() / 60.0 << " minutes." << endl;
//
//  /* RHS */
//  Vec H = vec(outlets.size());
//  VecSetValues(H, outlets.size(), &range(outlets.size())[0], &energy[0], INSERT_VALUES);
//  VecAssemblyBegin(H);
//  VecAssemblyEnd(H);
//  
//  KSP ksp;
//  code = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERR(code); 
//  code = KSPSetOperators(ksp, F, F); CHKERR(code); 
//  code = KSPSetType(ksp, KSPPREONLY); CHKERR(code); 
//  PC pc;
//  code = KSPGetPC(ksp, &pc); CHKERR(code); 
//  code = PCSetType(pc, PCLU); CHKERR(code); 
//  code = KSPSetFromOptions(ksp); CHKERR(code); 
//  
//  Vec p = vec(outlets.size()); 
//  KSPSolve(ksp, H, p); 
//
//  vector<double> pDrop(outlets.size());
//  VecGetValues(p, outlets.size(), &range(outlets.size())[0], &pDrop[0]); 
//  return pDrop;
//}

void CFD::time(double t){
  m_time = t;
}

Vec CFD::piola(string dir_u, string dir_d){
  Vec u = vec(m_nbDofs);
  Vec d = vec(m_nbDofs);
  loadVec(u, dir_u);
  loadVec(d, dir_d);
  return piola(u,d);

}

Vec CFD::piola(Vec uu, Vec dd){

//  Vec u = getSequential(uu);
  vector<double> u = stl(getSequential(uu));
  Vec d = getSequential(dd);

  IO io_factor;
  io_factor.initialize(par);

  Vec Piola = zeros(m_nbDofs);
  MasterElement fe;
  fe.initialize(par, geo.dimension());

  vector<Vec> displacement = decompose_vector_field(d);
  vector<Vec> grad_d(geo.dimension());
  io.writeState(displacement[0], "d0", 0);
  io.writeState(displacement[1], "d1", 0);
  for (int comp = 0; comp < geo.dimension(); comp++){
    Vec grad_sca = gradient_scalar(displacement[comp]);
    grad_d[comp] = getSequential(gradient_scalar(displacement[comp]));
    io.writeState(grad_d[comp], "grad" + wildcard(comp), 0.0);
    io.writeState(grad_sca, "grad" + wildcard(comp), 0.0);
  }
  int low, high;
  code = VecGetOwnershipRange(Piola, &low, &high); CHKERR(code);

  if (m_world_rank == 0) cout << "CFD: Computing Piola transform." << endl;
  Vec factor = vec(geo.nbVertices);
  int lowfactor, highfactor;
  code = VecGetOwnershipRange(factor, &lowfactor, &highfactor); CHKERR(code);
  for (int i = 0; i < geo.nbVertices; i++){

    if (i % (geo.nbVertices / m_verbose) == 0)
      if (m_world_rank == 0) cout << "  Node " << i << " of " << geo.nbVertices << endl;
    
    vector<vector<double>> gradient_of_disp(geo.dimension());
    for (int comp = 0; comp < geo.dimension(); comp++){
      gradient_of_disp[comp].resize(geo.dimension());
      fill(gradient_of_disp[comp].begin(), gradient_of_disp[comp].end(), 0.0);
    }

    for (int comp = 0; comp < geo.dimension(); comp++){
      code = VecGetValues(grad_d[comp], geo.dimension(), &range(geo.dimension()*i, geo.dimension()*i+geo.dimension())[0], &gradient_of_disp[comp][0]); CHKERR(code);
    }
    gradient_of_disp = transpose(gradient_of_disp);

    /* \grad A = I + \grad d_v*/
    for (int comp = 0; comp < geo.dimension(); comp++){
      gradient_of_disp[comp][comp] = gradient_of_disp[comp][comp] + 1.0;
    }
    double detF = determinant(gradient_of_disp);
    if (i >= lowfactor && i < highfactor){
      code = VecSetValues(factor, 1, &i, &detF, INSERT_VALUES); CHKERR(code);
    }

    vector<double> u_nodal(geo.dimension(), 0.0);
    for (int comp = 0; comp < geo.dimension(); comp++){
      u_nodal[comp] = u[geo.dimension()*i+comp];
    }
   
//    vector<double> piola_node = (gradient_of_disp * u_nodal) /detF;
    vector<double> piola_node = gradient_of_disp * u_nodal;

    for (int comp = 0; comp < geo.dimension(); comp++){ 
      vecSetInsert(Piola, geo.dimension()*i+comp, piola_node[comp]);
    }
  }

  code = VecAssemblyBegin(Piola); CHKERR(code);
  code = VecAssemblyEnd(Piola); CHKERR(code);

  code = VecAssemblyBegin(factor); CHKERR(code);
  code = VecAssemblyEnd(factor); CHKERR(code);

  io.writeState(u, "u", m_time);
  io.writeState(factor, "factor", m_time);
  io.writeState(Piola, "piola", m_time);
exit(1);
  return Piola;
}

//double CFD::kinetic_energy(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  cout << "CFD: computing kinetic energy for: " << velocity_path << endl;
//  return kinetic_energy(velocity);
//}

//double CFD::kinetic_energy(Vec u){
//
//  MasterTetrahedron tetrahedron;
//  tetrahedron.initialize(par);
//  
//  double energy = 0.0;
//
//  Tic tic;
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){ /* loop on parts */
//
//    int tetraId = 0;
//    for (vector<int> tetra : geo.tetrahedron()[partId]){ /* loop on tetra */
//
//      /* get finite element coordinates */
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[tetra[0]];
//      coordinates[1] = geo.coordinates()[tetra[1]];
//      coordinates[2] = geo.coordinates()[tetra[2]];
//      coordinates[3] = geo.coordinates()[tetra[3]];
//
//      tetrahedron.setCoordinates(coordinates);
//      tetrahedron.computeElementMass(par.density());
//     
//      /* loc2glob dof mapping */
//      vector<int> loc2glob(12);
//      for (int i = 0; i < 4; i++){
//        for (int comp = 0; comp < 3; comp++){
//          loc2glob[3*i+comp] = 3*tetra[i]+comp;
//        }
//      }
//
//      /* Gather advection field on element */
//      vector<double> u_el(12);
//      VecGetValues(u, 12, &loc2glob[0], &u_el[0]);
//      energy = energy + dot(u_el, tetrahedron.matrix() * u_el); 
//
//      
//      tetraId++;
//    }
//    tetrahedron.clear();
//  }
//  cout << "Time elapsed: " << tic.toc() / 60.0 << " minutes." << endl;
//
//  return energy;
//}

//double CFD::viscous_energy(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  cout << "CFD: computing viscous energy for: " << velocity_path << endl;
//  return viscous_energy(velocity);
//}
//double CFD::viscous_energy(Vec u){
//
//  MasterTetrahedron tetrahedron;
//  tetrahedron.initialize(par);
//  
//  double energy = 0.0;
//
//  vector<vector<double>> stiff_mat(12);
//  for (int i = 0; i < 12; i++){
//    stiff_mat[i].resize(12);
//  }
//
//  Tic tic;
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){ /* loop on parts */
//
//    int tetraId = 0;
//    for (vector<int> tetra : geo.tetrahedron()[partId]){ /* loop on tetra */
//
//      /* get finite element coordinates */
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[tetra[0]];
//      coordinates[1] = geo.coordinates()[tetra[1]];
//      coordinates[2] = geo.coordinates()[tetra[2]];
//      coordinates[3] = geo.coordinates()[tetra[3]];
//
//      tetrahedron.setCoordinates(coordinates);
//     
//      /* loc2glob dof mapping */
//      vector<int> loc2glob(12);
//
//      for (int i = 0; i < 4; i++){
//        for (int j = 0; j < 4; j++){
//          double Kij = tetrahedron.stiffness(i,j);
//          for (int comp = 0; comp < 3; comp++){
//            stiff_mat[3*i+comp][3*j+comp] = Kij;
//          }
//        }
//        for (int comp = 0; comp < 3; comp++){
//          loc2glob[3*i+comp] = 3*tetra[i]+comp;
//        }
//      }
//
//      /* Gather advection field on element */
//      vector<double> u_el(12);
//      VecGetValues(u, 12, &loc2glob[0], &u_el[0]);
//      energy = energy + dot(u_el, stiff_mat * u_el); 
//      tetraId++;
//    }
//  }
//  cout << "Time elapsed: " << tic.toc() / 60.0 << " minutes." << endl;
//  return energy;
//}
//
//double CFD::convective_energy(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  cout << "CFD: computing convective energy for: " << velocity_path << endl;
//  return convective_energy(velocity);
//}
//double CFD::convective_energy(Vec u){
//
//  MasterTetrahedron tetrahedron;
//  tetrahedron.initialize(par);
//  
//  double energy = 0.0;
//  vector<vector<double>> convec_mat(12);
//  for (int i = 0; i < 12; i++){
//    convec_mat[i].resize(12);
//  }
//
//  Tic tic;
//  for (int partId = 0; partId < geo.tetrahedron().size(); partId++){ /* loop on parts */
//
//    int tetraId = 0;
//    for (vector<int> tetra : geo.tetrahedron()[partId]){ /* loop on tetra */
//
//      /* get finite element coordinates */
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[tetra[0]];
//      coordinates[1] = geo.coordinates()[tetra[1]];
//      coordinates[2] = geo.coordinates()[tetra[2]];
//      coordinates[3] = geo.coordinates()[tetra[3]];
//      tetrahedron.setCoordinates(coordinates);
//     
//      /* loc2glob dof mapping */
//      vector<int> loc2glob(12);
//      for (int i = 0; i < 4; i++){
//        for (int comp = 0; comp < 3; comp++){
//          loc2glob[3*i+comp] = 3*tetra[i]+comp;
//        }
//      }
//
//      /* Gather advection field on element */
//      vector<double> u_el(12);
//      VecGetValues(u, 12, &loc2glob[0], &u_el[0]);
//
//      for (int j = 0; j < 4; j++){
//        vector<double> u_node_j(3);
//        u_node_j[0] = u_el[3*j];
//        u_node_j[1] = u_el[3*j+1];
//        u_node_j[2] = u_el[3*j+2];
//        for (int i = 0; i < 4; i++){
//          double Cij = tetrahedron.convection(i,j, u_node_j);
//          for (int comp = 0; comp < 3; comp++){
//            convec_mat[3*i+comp][3*j+comp] = Cij;
//          }
//        }
//      }
//      energy = energy + dot(u_el, convec_mat * u_el); 
//      tetraId++;
//    }
//  }
//  cout << "Time elapsed: " << tic.toc() / 60.0 << " minutes." << endl;
//  return energy;
//}
//
//double CFD::viscous_energyBD(string velocity_path){
//  /* load velocity field */ 
//  Vec velocity = vec(m_nbDofs);
//  loadVec(velocity, velocity_path);
//  cout << "CFD: computing viscous energy in the boundaries for: " << velocity_path << endl;
//  return viscous_energy(velocity);
//}
//double CFD::viscous_energyBD(Vec u){
//
//  MasterTetrahedron tetrahedron;
//  tetrahedron.initialize(par);
//  
//  MasterTriangle triangle;
//  triangle.initialize(par);
//  
//  double energy = 0.0;
//
//  Tic tic;
//  for (int partId = 0; partId < geo.triangles().size(); partId++){ /* loop over boundary parts */
//
//    for (int i = 0; i < geo.triangles()[partId].size(); i++ ){ /* loop over boundary elements */
//      /* get id of associated tetrahedra*/ 
//      vector<int> tetra = geo.getTetraFromTriangle(geo.triangles()[partId][i]);
// 
//      vector<vector<double>> coordinates(4);
//      coordinates[0] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][0]];
//      coordinates[1] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][1]];
//      coordinates[2] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][2]];
//      coordinates[3] = geo.coordinates()[geo.tetrahedron()[tetra[0]][tetra[1]][3]];
//      tetrahedron.setCoordinates(coordinates);
//
//      /* Compute normal */
//      vector<vector<double>> tria_coordinates(3);
//      tria_coordinates[0] = geo.coordinates()[ geo.triangles()[partId][i][0] ];
//      tria_coordinates[1] = geo.coordinates()[ geo.triangles()[partId][i][1] ];
//      tria_coordinates[2] = geo.coordinates()[ geo.triangles()[partId][i][2] ];
//      triangle.setCoordinates(tria_coordinates);     
//      triangle.computeNormal();
//      vector<double> n = triangle.normal();
//
//      tetrahedron.gradient_phi_i_dot_n_phi_j(n);
//
//      /* Compute viscous energy */
//      vector<double> u_el(12);
//      vector<int> u_el_id(12);
//      for (int dof_i = 0; dof_i < 4; dof_i++){
//        for (int comp = 0; comp < 3; comp++){
//          u_el_id[3*dof_i+comp] = 3*geo.tetrahedron()[tetra[0]][tetra[1]][dof_i] + comp;
//        }
//      }
//      VecGetValues(u, 12, &u_el_id[0], &u_el[0]);
//      energy += dot(u_el,  tetrahedron.matrix() * u_el);
//
//      tetrahedron.clear();
//    }
//  }
//  return energy;
//}
//
vector<Vec> CFD::decompose_vector_field(Vec u){
  vector<Vec> u_comps(geo.dimension());
  for (int i = 0; i < geo.dimension(); i++){
    u_comps[i] = vec(m_nbVertices);
  }
  for (int i = 0; i < geo.coordinates().size(); i++){
    vector<double> u_node(geo.dimension());
    code = VecGetValues(u, geo.dimension(), &range(geo.dimension()*i,geo.dimension()*i+geo.dimension())[0], &u_node[0]); CHKERR(code);
    for (int j = 0; j < geo.dimension(); j++){
      code = VecSetValue(u_comps[j], i, u_node[j], INSERT_VALUES); CHKERR(code);
    }
  }
  return u_comps;
}

//void CFD::assembleNSstatic(Mat & M, Mat & NS){
//
//  if (m_world_rank) cout << "CFD: Assembling static Navier-Stokes matrix." << endl;
//
//  mat(NS, m_nbDofs, m_nbDofs);
//  mat(M, m_nbDofs, m_nbDofs);
//
//  int m,n;
//  code = MatGetOwnershipRange(NS, &m, &n); CHKERR(code);
//
//  MasterTetrahedron tet;
//  tet.initialize(par);
//
//  Tic tic;
//  for (int tetraId = 0; tetraId < geo.tetrahedron()[0].size(); tetraId++){ /* loop on tetra */
//
//    vector<int> tetra = geo.tetrahedron()[0][tetraId];
//    /* get finite element coordinates */
//    vector<vector<double>> coordinates(4);
//    coordinates[0] = geo.coordinates()[tetra[0]];
//    coordinates[1] = geo.coordinates()[tetra[1]];
//    coordinates[2] = geo.coordinates()[tetra[2]];
//    coordinates[3] = geo.coordinates()[tetra[3]];
//
//    tet.setCoordinates(coordinates);
//    tet.computeSize();
//
//    if (tetraId % (geo.tetrahedron()[0].size() / m_verbose) == 0){
//      if (m_world_rank == 0) cout << "  \n * * * * * * * * * * * * * * * " << endl;
//      if (m_world_rank == 0) cout << "  CFD: Elementary matrix for tetra: " << tetraId << "/" << geo.tetrahedron()[0].size() - 1 << endl;
//      if (m_world_rank == 0) cout << "    Labels: " << tetra << endl; 
//      if (m_world_rank == 0) cout << "    Size: " << tet.size() << endl;
//      if (m_world_rank == 0) cout << "    det(J) = " << tet.detJacobian() << endl; 
//      if (m_world_rank == 0) cout << "    volume = " << tet.volume() << " cms^3." << endl;
//      if (m_world_rank == 0) cout << "    Mass(1,1) = " << tet.mass(1,1) << endl;
//      if (m_world_rank == 0) cout << "    Stiff(1,2) = " << tet.stiffness(1,2) << endl;
//      if (m_world_rank == 0) cout << "    Mixed(2,2) = " << tet.mixed(2,2) << endl;
//    }
//
//    /* Assemble elementary matrices */
//    for (int i = 0; i < 4; i++){
//      for (int j = 0; j < 4; j++){
//        for (int comp = 0; comp < 3; comp++){
//          if (3*tetra[i]+comp >= m && 3*tetra[i]+comp < n){
//            /* M */
//            double Mij = par.density()/par.timeStep()*tet.mass(i,j);
//            /* stiffness */
//            double Kij = tet.stiffness(i,j);
//            code = MatSetValue(M, 3*tetra[i]+comp, 3*tetra[j]+comp, Mij, ADD_VALUES); CHKERR(code);
//            code = MatSetValue(NS, 3*tetra[i]+comp, 3*tetra[j]+comp, par.viscosity() * Kij, ADD_VALUES); CHKERR(code);
//            /* div_phi_i phi_j */
//            double Bij = tet.mixed(i,j,comp);
//            code = MatSetValue(NS, 3*tetra[i]+comp, m_nbDofsVel + tetra[j], -1.0*Bij, ADD_VALUES); CHKERR(code); /* -B*/
//          }
//
//          if (m_nbDofsVel + tetra[i] >= m && m_nbDofsVel + tetra[i] < n){
//            /* div_phi_i phi_j */
////            double Bij = tet.mixed(i,j,comp);
//            double Bij = tet.mixed(j,i,comp);
//            code = MatSetValue(NS, m_nbDofsVel + tetra[i], 3*tetra[j]+comp, 1.0*Bij, ADD_VALUES); CHKERR(code); /* B^T */
//          }
//        }
//        if (m_nbDofsVel + tetra[i] >= m && m_nbDofsVel + tetra[i] < n){
//          double Kij = tet.stiffness(i,j);
//          /* Brezzi-Pitkaranta stabilization for P1-P1 elements */
//          code = MatSetValue(NS, m_nbDofsVel + tetra[i], m_nbDofsVel + tetra[j], tet.size()*tet.size() * Kij, ADD_VALUES); CHKERR(code);
//        }
//      }
//    }
//  }
//  if (m_world_rank == 0) cout << endl << "Time elapsed: " << tic.toc() << " sec. " << endl;
//
//  code = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERR(code); 
//  code = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY); CHKERR(code);
//  code = MatAssemblyBegin(NS, MAT_FINAL_ASSEMBLY); CHKERR(code); 
//  code = MatAssemblyEnd(NS, MAT_FINAL_ASSEMBLY); CHKERR(code);
//}

//Mat & CFD::assembleNS(const Vec & solution){
//
//  /* Assemble time dependent matrix */
//  Mat m_mat = mat(m_nbDofs, m_nbDofs);
//
//  int m,n;
//  code = MatGetOwnershipRange(m_mat, &m, &n); CHKERR(code);
//  MasterTetrahedron tet;
//  tet.initialize(par);
//
//  Vec phi0_seq;
//  Vec phi0_copy = zeros(m_nbDofs);
//  code = VecAXPY(phi0_copy, 1.0, solution);
//
//  code = VecCreateSeq(PETSC_COMM_SELF, m_nbDofs, &phi0_seq); CHKERR(code);
//  code = VecSetFromOptions(phi0_seq); CHKERR(code); 
//  VecScatter inctx;
//  code = VecScatterCreateToAll(phi0_copy, &inctx, NULL); CHKERR(code);
//  code = VecScatterBegin(inctx, phi0_copy, phi0_seq, INSERT_VALUES, SCATTER_FORWARD); CHKERR(code);
//  code = VecScatterEnd(inctx, phi0_copy, phi0_seq, INSERT_VALUES, SCATTER_FORWARD); CHKERR(code);
//  code = VecScatterDestroy(&inctx); CHKERR(code);
//  code = VecDestroy(&phi0_copy); CHKERR(code);
//
//  if (m_world_rank == 0) cout << "CFD: Computing convection matrix. " << endl;
//
//  Tic tic;
//  for (int tetraId = 0; tetraId < geo.tetrahedron()[0].size(); tetraId++){ /* loop on tetra */
//
//    vector<int> tetra = geo.tetrahedron()[0][tetraId];
//    /* get finite element coordinates */
//    vector<vector<double>> coordinates(4);
//    coordinates[0] = geo.coordinates()[tetra[0]];
//    coordinates[1] = geo.coordinates()[tetra[1]];
//    coordinates[2] = geo.coordinates()[tetra[2]];
//    coordinates[3] = geo.coordinates()[tetra[3]];
//
//    tet.setCoordinates(coordinates);
//    tet.computeSize();
//
//    /* loc2glob dof mapping */
//    vector<int> loc2glob(12);
//    for (int i = 0; i < 4; i++){
//      for (int comp = 0; comp < 3; comp++){
//        loc2glob[3*i+comp] = 3*tetra[i]+comp;
//      }
//    }
//
//    /* Gather advection field on element */
//    vector<double> u_el(12);
//    code = VecGetValues(phi0_seq, 12, &loc2glob[0], &u_el[0]); CHKERR(code);
//
//    /* Set values from local to global */
//    for (int j = 0; j < 4; j++){
//      vector<double> u_node_j(3);
//      for (int i = 0; i < 4; i++){
//        u_node_j[0] = u_el[3*j];
//        u_node_j[1] = u_el[3*j+1];
//        u_node_j[2] = u_el[3*j+2];
//        for (int comp = 0; comp < 3; comp++){
//          if (3*tetra[i]+comp >= m && 3*tetra[i]+comp < n){
//            code = MatSetValue(m_mat, 3*tetra[i]+comp, 3*tetra[j]+comp, tet.advection(i, j, u_node_j), ADD_VALUES); CHKERR(code);
//          }
//        }
//      }
//    }
//  }
//
//  code = MatAssemblyBegin(m_mat, MAT_FINAL_ASSEMBLY); CHKERR(code);
//  code = MatAssemblyEnd(m_mat, MAT_FINAL_ASSEMBLY); CHKERR(code);
//
//  VecDestroy(&phi0_seq);
//
//  return m_mat;
//
//}

void CFD::configureSolver(){
  KSP m_ksp;
  code = KSPCreate(PETSC_COMM_WORLD, &m_ksp); CHKERR(code); 
}

Vec CFD::solve(const Mat & NS, const Vec & rhs){
  KSP m_ksp;
  code = KSPCreate(PETSC_COMM_WORLD, &m_ksp); CHKERR(code); 
  Vec solution = vec(m_nbDofs);
  PetscBool reuse_prec = PETSC_TRUE;
  code = KSPSetOperators(m_ksp, NS, NS); CHKERR(code); 
  code = KSPSetType(m_ksp, KSPGMRES); CHKERR(code); 
  PC pc;
  code = KSPGetPC(m_ksp, &pc); CHKERR(code); 
  code = PCSetType(pc, PCASM); CHKERR(code); 
  code = KSPMonitorSet(m_ksp, krylovMonitor, NULL, NULL); CHKERR(code);
//  code = KSPSetReusePreconditioner(m_ksp, reuse_prec); CHKERR(code);
  code = KSPSetFromOptions(m_ksp); CHKERR(code); 
  code = KSPSolve(m_ksp, rhs, solution); CHKERR(code); 
  return solution;
}
