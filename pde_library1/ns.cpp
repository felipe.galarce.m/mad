/*=============================================================================
  This file is part of the code MAD
  Multy-physics simulAtions for engineering and Data assimilation.
  Copyright (C) 2017-2025,
    
     Felipe Galarce at INRIA/WIAS/PUCV

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<ns.hpp>

void NS::initialize(Parameters parameters, const Geometry & geometry, const Boundary & boundary){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "Navier Stokes: Initializing" << endl;
  par = parameters;
  geo = geometry;
  bd = boundary;
  calculus.initialize(par, geo, bd);
  fe.initialize(par, geo.dimension());
  feBD.initialize(par, geo.dimension());
  m_verbose = par.verbose();
  m_interpolator.initialize(par, geo);

  nbDofs = 0;
  for (int i = 0; i < par.nbVariables(); i++){
    nbDofs += par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbDofsPress = geo.nbVertices;
  nbDofsVel = 0;
  for (int i = 0; i < par.nbVariables()-1; i++){
    nbDofsVel = nbDofsVel + par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbVertices = geo.nbVertices;
  nbDofsPerNode = par.nbDofsPerNode()[0]; 
  nbNodesPerElement = geo.dimension()+1; 

  if (par.constitutiveModel() != "Newtonian"){
    m_non_newtonian = true;
  }

  u0 = zeros(nbDofs);
  u00 = zeros(nbDofs);
  VecAssemblyBegin(u0);
  VecAssemblyEnd(u0);
  VecAssemblyBegin(u00);
  VecAssemblyEnd(u00);

  mat(A, nbDofs, nbDofs); 
  mat(M, nbDofs, nbDofs); 
  mat(C, nbDofs, nbDofs); 

  femStat.initialize(par, geo, bd, A);
  femMass.initialize(par, geo, bd, M);
  femConv.initialize(par, geo, bd, C);
  code = MatGetOwnershipRange(A, &m, &n); CHKERR(code);

  if (m_world_rank == 0) cout << "Navier Stokes: dofs splitting:" << endl;
  cout << "  Proc #"<< m_world_rank << " : " << m << " " << n << endl;

  computeJacobians();
  computeLocal2GlobalMappings();
}

void NS::finalize(){
  MatDestroy(&M);
  MatDestroy(&A);
  MatDestroy(&C);
  VecDestroy(&u0);
  if (par.timeIntegration() == "BDF2"){
    VecDestroy(&u00);
  }
  KSPDestroy(&ksp);
}

void NS::assembleSUparameters(){
  double normU0_2;
  vector<Vec> ucomp = calculus.split(u0);
  Vec Mu = vec(geo.nbVertices);
  for (int comp = 0; comp < geo.dimension(); comp++){
    code = MatMult(geo.massMatrix(), ucomp[comp], Mu); CHKERR(code);
    code = VecDot(Mu, ucomp[comp], &normU0_2);
  }
  double viscosity = par.viscosity();
  m_tau_pspg.resize(geo.elements().size());
  m_tau_supg.resize(geo.elements().size());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Assembling stabilization parameters. Part " << partId << endl;
    m_tau_pspg[partId].resize(geo.elements()[partId].size());
    m_tau_supg[partId].resize(geo.elements()[partId].size());

    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      femConv.setSimplex(simplex);

      double sigma = 1.0;
      double hk = femConv.feSize();
      if (par.timeIntegration() == "BDF2"){
        sigma = 2.0;
      }
      /* Tezduyar 1992 */
      double tau_1 = pow(sigma/par.timeStep(), 2.0);
      double tau_2 = pow(4.0 * viscosity, 2.0)/pow(hk, 4.0);
      double tau_3 = 4.0 * normU0_2 / hk / hk;
      m_tau_pspg[partId][feId] = pow(tau_1 + tau_3, -0.5);
      m_tau_supg[partId][feId] = pow(tau_1 + tau_2 + tau_3, -0.5);
    }
  }
}

void NS::assembleSUparametersNonLinear(){
  if (m_world_rank == 0) cout << "Navier Stokes: Assembling non-linear time-dependent stab parameters." << endl;

  double normU0_2 = 0.0;
  vector<Vec> ucomp = calculus.split(u0);
  Vec Mu = vec(geo.nbVertices);
  for (int comp = 0; comp < geo.dimension(); comp++){
    double normU0_2comp;
    code = MatMult(geo.massMatrix(), ucomp[comp], Mu); CHKERR(code);
    code = VecDot(Mu, ucomp[comp], &normU0_2comp);
    normU0_2 = normU0_2 + normU0_2comp;
  }
  m_tau_pspg.resize(geo.elements().size());
  m_tau_supg.resize(geo.elements().size());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    m_tau_pspg[partId].resize(geo.elements()[partId].size());
    m_tau_supg[partId].resize(geo.elements()[partId].size());

    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      femConv.setSimplex(simplex);

      double sigma = 1.0;
      double hk = femConv.feSize();
      if (par.timeIntegration() == "BDF2"){
        sigma = 2.0;
      }
      /* Tezduyar 1992 */
      double tau_1 = pow(sigma/par.timeStep(), 2.0);
      double tau_2 = pow(4.0 * m_viscosity[feId], 2.0)/pow(hk, 4.0);
      double tau_3 = 4.0 * normU0_2 / hk / hk;
      m_tau_pspg[partId][feId] = pow(tau_1 + tau_3, -0.5);
      m_tau_supg[partId][feId] = pow(tau_1 + tau_2 + tau_3, -0.5);
    }
  }
  for (int comp = 0; comp < geo.dimension(); comp++){
    VecDestroy(&ucomp[comp]);
  }
}

void NS::computeJacobians(){
  m_finiteElements.resize(geo.nbElements());
  int iter = 0;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Computing Jacobians. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      m_finiteElements[iter].initialize(par, geo.dimension());
      m_finiteElements[iter].setCoordinates(coordinates);
      m_finiteElements[iter].computeSize();
      iter = iter + 1;
    }
  }
}

void NS::computeLocal2GlobalMappings(){
  int iter = 0;
  m_loc2Global.resize(geo.nbElements());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Computing local2Global mappings." << endl;
    for (vector<int> simplex : geo.elements()[partId]){ /* loop on elements */
      int nbNodesPerElement = simplex.size();
      /* loc2glob dof mapping */
      m_loc2Global[iter].resize(nbNodesPerElement*nbDofsPerNode);
      for (int i = 0; i < nbNodesPerElement; i++){
        for (int comp = 0; comp < nbDofsPerNode; comp++){
          m_loc2Global[iter][nbDofsPerNode*i+comp] = nbDofsPerNode*simplex[i]+comp;
        }
      }
      iter = iter + 1;
    }
  }
}

Mat NS::assembleLHS_static(){
  int iter = 0;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Assembling discretization matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      femStat.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]);
      femMass.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]); 
      iter = iter + 1;

      if (feId % (geo.elements()[partId].size() / m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }

      /* u \cdot v */
      for (int var = 0; var < par.nbVariables()-1; var++){
        femMass.u_dot_v_scalar(par.density(), var, var);
      }

      if (!m_non_newtonian){
        if (par.viscousTerm() == "symmetric"){
          /* 2 eta grad^s u : grad^s v */
          for (int var = 0; var < par.nbVariables()-1; var++){
            for (int comp = 0; comp < geo.dimension(); comp++){
              if (comp == var){
                femStat.du_x_dv_y(2.0*par.viscosity(), comp, comp, var, var);
              } else {
                femStat.du_x_dv_y(par.viscosity(), comp, comp, var, var);
                femStat.du_x_dv_y(par.viscosity(), comp, var, var, comp);
              }
            }
          }
        } else {
          /* eta grad u : grad v */
          for (int var = 0; var < par.nbVariables()-1; var++){
            for (int comp = 0; comp < geo.dimension(); comp++){
              femStat.du_x_dv_y(par.viscosity(), comp, comp, var, var);
            }
          }
        }
      }

      /* -p div(v) */
      for (int comp = 0; comp < geo.dimension(); comp++){
        femStat.u_dv_y(-1.0, comp, comp, geo.dimension());
      }

      /* div(u) q */
      for (int comp = 0; comp < geo.dimension(); comp++){
        femStat.du_x_v(1.0, comp, geo.dimension(), comp);
      }
    }
  }
  code = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY); CHKERR(code);
  code = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERR(code);

  /* LHS static matrix */
  Mat S = mat(nbDofs, nbDofs);
  code = MatDuplicate(M, MAT_COPY_VALUES, &S); CHKERR(code);
  if (par.timeIntegration() == "BDF1"){
    code = MatScale(S, 1.0/par.timeStep());
  } else if (par.timeIntegration() == "BDF2"){
    code = MatScale(S, 3.0/2.0/par.timeStep());
  }
  code = MatAXPY(S, 1.0, A, DIFFERENT_NONZERO_PATTERN); CHKERR(code);

  double normLHS = norm(S);
  if (m_world_rank == 0) cout << "Navier Stokes: Norm LHS = " << normLHS << endl;

  return S;
}

Mat NS::assembleLHS(Mat LHS_static, Vec u0_custom){

  vector<Vec> u0_split;
  if (u0_custom == NULL){
    u0_split = calculus.split(u0);
  } else {
    u0_split = calculus.split(u0_custom);
  }
  vector<Vec> u0_seq(geo.dimension());
  for (int i = 0; i < geo.dimension(); i++){
    u0_seq[i] = getSequential(u0_split[i]);
  }
  code = MatZeroEntries(C); CHKERR(code);

  if (par.bc_method() == "brut"){ // Disable warning due to brut enforcement of bc and slight change in nz pattern
    code = MatSetOption(C, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE); CHKERR(code);
  }
  vector<vector<double>> u_element(geo.dimension());
  for (int comp = 0; comp < geo.dimension(); comp++){
    u_element[comp].resize(geo.dimension()+1);
  }
  int iter = 0;

  if (par.constitutiveModel() == "Newtonian"){
    assembleSUparameters();
  } else {
    assembleSUparametersNonLinear();
  }

  for (int partId = 0; partId < geo.elements().size(); partId++){

    if (m_world_rank == 0) cout << "Navier Stokes: Assembling time-dependent matrix." << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      double tau_pspg = m_tau_pspg[partId][feId];
      double tau_supg = m_tau_supg[partId][feId];

      if (feId % (geo.elements()[partId].size() / m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }
      femConv.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]); 
      iter = iter + 1;

      /* Get solution at previous time step per element */
      for (int comp = 0; comp < geo.dimension(); comp++){
        code = VecGetValues(u0_seq[comp], geo.dimension()+1, &femConv.simplex()[0], &u_element[comp][0]); CHKERR(code); CHKERR(code);
      }

      /* eta grad^s u : grad^s v */
      if (m_non_newtonian){
        for (int var = 0; var < par.nbVariables()-1; var++){
          for (int comp = 0; comp < geo.dimension(); comp++){
            if (comp == var){
              femConv.du_x_dv_y(2.0*m_viscosity[feId], comp, comp, var, var);
            } else {
              femConv.du_x_dv_y(m_viscosity[feId], comp, comp, var, var);
              femConv.du_x_dv_y(m_viscosity[feId], comp, var, var, comp);
            }
          }
        } 
      }  

      /* (grad u) u* v */
      for (int var = 0; var < par.nbVariables()-1; var++){
        for (int comp = 0; comp < geo.dimension(); comp++){
          femConv.a_du_x_v(par.density(), comp, u_element[comp], var, var); 
        }
      }

      // --------- stabilization ---------
      
      /* rho tau_supg (grad u u, u grad v) */
      for (int var = 0; var < par.nbVariables()-1; var++){
        for (int comp1 = 0; comp1 < geo.dimension(); comp1++){
          for (int comp2 = comp1; comp2 < geo.dimension(); comp2++){ 
            femConv.a_du_x_b_dv_y(par.density()*tau_supg, comp1, comp2, u_element[comp1], u_element[comp2], var, var); 
          }
        }
      }

      /* tau_pspg (grad p, grad q)*/
      for (int comp = 0; comp < geo.dimension(); comp++){
        femConv.du_x_dv_y(tau_pspg, comp, comp, geo.dimension(), geo.dimension()); 
      }

      /* rho tau_pspg (grad q, u \cdot u) */
      for (int comp1 = 0; comp1 < geo.dimension(); comp1++){
        for (int comp2 = 0; comp2 < geo.dimension(); comp2++){
          femConv.a_du_x_dv_y(par.density()*tau_pspg, u_element[comp1], comp2, comp1, geo.dimension(), comp2); 
        }
      }

      /* tau_supg (u \cdot grad v, grad p) */
      for (int comp1 = 0; comp1 < geo.dimension(); comp1++){
        for (int comp2 = 0; comp2 < geo.dimension(); comp2++){
          femConv.a_du_x_dv_y(tau_supg, u_element[comp1], comp1, comp2, comp2, geo.dimension()); 
        }
      }

      /* rho tau_supg / dt  (u \cdot grad v, u) */
      for (int comp1 = 0; comp1 < geo.dimension(); comp1++){
        for (int comp2 = 0; comp2 < geo.dimension(); comp2++){
          femConv.a_u_dv_y(par.density()*tau_supg/par.timeStep(), u_element[comp1], comp1, comp2, comp2); 
        }
      }

      /* rho tau_pspg / dt (u, grad q) */
      for (int comp = 0; comp < geo.dimension(); comp++){
        femConv.u_dv_y(par.density() * tau_pspg / par.timeStep(), comp, geo.dimension(), comp);
      }
    }
  }
  for (int i = 0; i < geo.dimension(); i++){
    VecDestroy(&u0_seq[i]);
    VecDestroy(&u0_split[i]);
  }
  code = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERR(code);
  code = MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY); CHKERR(code);

  /* assemble full LHS matrix */ 
  code = MatAXPY(C, 1.0, LHS_static, DIFFERENT_NONZERO_PATTERN); CHKERR(code);

  return C;
}

Vec NS::assembleRHS(Vec u0_custom, Vec u00_custom){
  Vec b = zeros(nbDofs);
  if (u0_custom == NULL){
    if (par.timeIntegration() == "BDF1"){
      code = MatMult(M, u0, b); CHKERR(code);
    } else if (par.timeIntegration() == "BDF2") {
      Vec b0 = zeros(nbDofs);
      code = VecAXPY(b0, 2.0, u0); CHKERR(code);
      code = VecAXPY(b0, -1.0/2.0, u00); CHKERR(code);
      code = MatMult(M, b0, b); CHKERR(code); CHKERR(code);
      VecDestroy(&b0);
    }
  } else {
    if (par.timeIntegration() == "BDF1"){
      code = MatMult(M, u0_custom, b); CHKERR(code);
    } else if (par.timeIntegration() == "BDF2"){
      Vec b0 = zeros(nbDofs);
      code = VecAXPY(b0, 2.0, u0_custom);
      code = VecAXPY(b0, -1.0/2.0, u00_custom);
      code = MatMult(M, b0, b); CHKERR(code);
      VecDestroy(&b0);
    }
  }

  vector<Vec> u0_split;
  if (u0_custom == NULL){
    u0_split = calculus.split(u0);
  } else {
    u0_split = calculus.split(u0_custom);
  }
  vector<Vec> u0_seq(geo.dimension());
  for (int i = 0; i < geo.dimension(); i++){
    u0_seq[i] = getSequential(u0_split[i]);
  }

  VecScale(b, 1.0/par.timeStep());

  /* Assembling elementary vectors */
  femConv.clearRHS();
  vector<vector<double>> u_el(geo.dimension());
  for (int comp = 0; comp < geo.dimension(); comp++){
    u_el[comp].resize(geo.dimension()+1);
  }
  int iter = 0;

  for (int partId = 0; partId < geo.elements().size(); partId++){

    if (m_world_rank == 0) cout << "Navier Stokes: Assembling time-dependent RHS." << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      double tau_pspg = m_tau_pspg[partId][feId];
      double tau_supg = m_tau_supg[partId][feId];

      femConv.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]); 
      iter = iter + 1;

      /* Get solution at previous time step per element */
      for (int comp = 0; comp < geo.dimension(); comp++){
        code = VecGetValues(u0_seq[comp], geo.dimension()+1, &femConv.simplex()[0], &u_el[comp][0]); CHKERR(code); CHKERR(code);
      }

      // --------- stabilization ---------
      /* rho tau_supg / dt (u0 \cdot grad v, grad p) */
      for (int comp1 = 0; comp1 < geo.dimension(); comp1++){
        for (int comp2 = 0; comp2 < geo.dimension(); comp2++){
          femConv.a_du_x(par.density()/par.timeStep()*tau_supg, u_el[comp1]*u_el[comp2], comp2, comp1); 
        }
      }

      /* rho tau_pspg / dt (u0, grad q) */
      for (int comp = 0; comp < geo.dimension(); comp++){
        femConv.a_du_x(par.density()/par.timeStep()*tau_pspg, u_el[comp], comp, geo.dimension()); 
      }
    }
  }

  femConv.assembleVector();
  VecAXPY(b, 1.0, femConv.getVector());

  for (int i = 0; i < geo.dimension(); i++){
    code = VecDestroy(&u0_seq[i]); CHKERR(code);
    code = VecDestroy(&u0_split[i]); CHKERR(code);
  }

  double normRHS = norm(b);
  if (m_world_rank == 0) cout << "Navier Stokes: Norm RHS = " << normRHS << endl;
  return b;
}

void NS::update(Vec u, double time){
  if (par.timeIntegration() == "BDF1"){
    code = VecZeroEntries(u0); CHKERR(code);
    code = VecAXPY(u0, 1.0, u); CHKERR(code);
  } else if (par.timeIntegration() == "BDF2"){
    code = VecZeroEntries(u00); CHKERR(code);
    code = VecAXPY(u00, 1.0, u0); CHKERR(code);
    code = VecZeroEntries(u0); CHKERR(code);
    code = VecAXPY(u0, 1.0, u); CHKERR(code);
  }
}

void NS::computeFlows(Vec u){
  double incompressibility_failure = 0.0;
  for (int i = 0; i < geo.bdLabels().size(); i++){
    double Q_bd = bd.flow(u, geo.bdLabels()[i]);
    if (m_world_rank == 0) cout << "Navier-Stokes: flow at boundary label " << geo.bdLabels()[i] << " : " << Q_bd << endl; 
    incompressibility_failure = incompressibility_failure + Q_bd;
  }
  if (m_world_rank == 0) cout << "Navier-Stokes: incompressibility failure = " << incompressibility_failure << endl; 
}

void NS::setSolver(){
  if (m_world_rank == 0) cout << "KSP: Configuring" << endl;
  if (par.solver() != "schur"){
    configureKSP(ksp, par);
  }
}

void NS::setLHS(Mat A, Mat P){
  if (m_world_rank == 0) cout << "KSP: Setting operators" << endl;
  if (par.solver() != "schur"){
    if (P == NULL){
      code = KSPSetOperators(ksp, A, A); CHKERR(code); 
    } else {
      code = KSPSetOperators(ksp, A, P); CHKERR(code); 
    }
    code = KSPSetUp(ksp); CHKERR(code);
  }
}

void NS::schur(Mat A, Vec u, Vec b){

  if (m_world_rank == 0) cout << "NS: Computing Schur complement" << endl;
  code = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERR(code); 
  code = KSPSetType(ksp, KSPGMRES); CHKERR(code); 

  PC pc;
  // Attach preconditioner
  KSPGetPC(ksp, &pc);
  PCSetType(pc, PCFIELDSPLIT); // Field-Split for velocity-pressure coupling

  // Configure the field split for Navier-Stokes
  PCFieldSplitSetType(pc, PC_COMPOSITE_SCHUR);
  PCFieldSplitSetSchurFactType(pc, PC_FIELDSPLIT_SCHUR_FACT_DIAG);

  // Create the IS for velocity and pressure
  IS is_velocity, is_pressure;
  ISCreateGeneral(PETSC_COMM_WORLD, nbDofsVel, &range(nbDofsVel)[0], PETSC_COPY_VALUES, &is_velocity);
  ISCreateGeneral(PETSC_COMM_WORLD, nbDofsPress, &range(nbDofsVel, nbDofsVel+nbDofsPress)[0], PETSC_COPY_VALUES, &is_pressure);

  // Preconditioner for velocity block
  PCFieldSplitSetIS(pc, "velocity", is_velocity);
  PCFieldSplitSetIS(pc, "pressure", is_pressure);

  // Set iterative solver for pressure block
  PCFieldSplitSetSchurPre(pc, PC_FIELDSPLIT_SCHUR_PRE_SELFP, NULL);

  code = KSPSetOperators(ksp, A, A); CHKERR(code); 
  code = KSPSetUp(ksp); CHKERR(code);

  KSP sub_ksp_x, sub_ksp_y;
  PC sub_pc_x, sub_pc_y;
  KSP *sub_ksp;

  PCFieldSplitGetSubKSP(pc, NULL, &sub_ksp);
  sub_ksp_x = sub_ksp[0]; // Solver for u
  sub_ksp_y = sub_ksp[1]; // Solver for p

  // Configure solver for u
  code = KSPSetType(sub_ksp_x, KSPGMRES); CHKERR(code);
  code = KSPGetPC(sub_ksp_x, &sub_pc_x); CHKERR(code);
  PCSetType(sub_pc_x, PCGAMG); CHKERR(code); 
//  PCAMGSetType(sub_pc_x, "boomeramg");

  // Configure solver for p
  code = KSPSetType(sub_ksp_y, KSPGMRES); CHKERR(code);
  code = KSPGetPC(sub_ksp_y, &sub_pc_y); CHKERR(code);
  PCSetType(sub_pc_y, PCGAMG); CHKERR(code);
//  PCAMGSetType(sub_pc_y, "boomeramg");

  if (par.use_solution_as_guess_KSP()){
    code = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERR(code);
  }
  if (par.monitorKSP()){
    PetscViewerAndFormat *vf;
    PetscErrorCode code;

    // Create a viewer for the monitor
    PetscViewerAndFormatCreate(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_DEFAULT, &vf);

    // Set the true residual monitor
    code = KSPMonitorSet(ksp, (PetscErrorCode (*)(KSP, PetscInt, PetscReal, void *))KSPMonitorTrueResidualNorm, vf, (PetscErrorCode (*)(void **))PetscViewerAndFormatDestroy);
    CHKERR(code);
  }

  // Set tolerances for convergence
  code = KSPSetTolerances(ksp, par.ksp_tolerance(), par.ksp_tolerance_absolute(), PETSC_DEFAULT, par.ksp_max_iterations());
  code = KSPSetFromOptions(ksp); CHKERR(code); 

  if (m_world_rank == 0) cout << "NS: Solving linear system" << endl;
  code = KSPSolve(ksp, b, u); CHKERR(code); 
  
  int its;
  double rnorm;
  code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
  code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
  if (m_world_rank == 0) cout << "NS: " << its << " iterations. Residual norm = " << rnorm << endl;
}

void NS::solve(Vec b, Vec u, Mat LHS){
  if (m_world_rank == 0) cout << "NS: Solving linear system" << endl;
  if (par.solver() == "schur"){
    schur(A, u, b);
  } else {
    code = KSPSolve(ksp, b, u); CHKERR(code); 
    int its;
    double rnorm;
    code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
    code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
    if (par.solver() == "gmres"){
      if (m_world_rank == 0) cout << "NS: " << its << " iterations. Residual norm = " << rnorm << endl;
    }
  }
}

/* compute \grad u \cdot n */
Vec NS::shearStress(Vec u, int bdLabel){
  if (m_world_rank == 0) cout << "Navier Stokes: computing shear stress at boundary label " << bdLabel << "." << endl;
  return calculus.gradientOnBoundary(u, bdLabel);
}

vector<double> NS::getViscosity(Vec u0_custom){

  Vec u0_seq;
  if (u0_custom == NULL){
    u0_seq = getSequential(u0);
  } else {
    u0_seq = getSequential(u0_custom);
  }

  m_viscosity.clear();
  m_viscosity.resize(geo.elements()[0].size());
  vector<Vec> u0_seq_percomponent(geo.dimension());

  vector<Vec> u_split = calculus.split(u0_seq);
  for (int comp = 0; comp < geo.dimension(); comp++){
    u0_seq_percomponent[comp] = getSequential(u_split[comp]);
  }

  int iter = 0;
  vector<vector<double>> u_element(geo.dimension());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Navier Stokes: Computing apparent viscosity. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
   
      /* Compute strain-rate magnitude */
      vector<vector<double>> grad_u(geo.dimension());
      for (int comp = 0; comp < geo.dimension(); comp++){
        u_element[comp].resize(nbNodesPerElement*nbDofsPerNode);
        code = VecGetValues(u0_seq_percomponent[comp], nbNodesPerElement*nbDofsPerNode, 
                            &m_loc2Global[iter][0], &u_element[comp][0]); CHKERR(code);
        grad_u[comp].resize(geo.dimension(), 0.0);
      }
      for (int nodeId = 0; nodeId < nbNodesPerElement; nodeId++){
        vector<double> grad_phi_i = transpose(m_finiteElements[iter].jacobianInverse()) * m_finiteElements[iter].grad_phi(nodeId);
        for (int comp = 0; comp < geo.dimension(); comp++){
          grad_u[comp] = grad_u[comp] + u_element[comp][nodeId] * grad_phi_i;
        }
      }
      grad_u = grad_u + transpose(grad_u);
      double eta = 0;
      double gamma_dot = sqrt(0.5*trace(grad_u * transpose(grad_u)));

      /* Compute apparent viscosity */
      if (par.constitutiveModel() == "power_law"){
        if (gamma_dot != 0){
          eta = par.power_law_m() * pow(gamma_dot, par.power_law_n() - 1.0);
        } 
        if (eta < par.power_law_m()/10000.0){
          eta = par.power_law_m()/10000.0;
        } else if (eta > 10000.0*par.power_law_m()){
          eta = par.power_law_m()*10000.0;
        }
      } else if (par.constitutiveModel() == "cross"){
        eta = par.mu_inf() + (par.mu0() - par.mu_inf())/(1.0 + pow(par.lambda() * gamma_dot, par.power_law_n()) );
      } else if (par.constitutiveModel() == "carreau"){
        eta = par.mu_inf() + (par.mu0() - par.mu_inf()) * pow(1.0 + pow(par.lambda() * gamma_dot, par.carreau_a()), (par.power_law_n() - 1.0)/par.carreau_a());
      }
      m_viscosity[feId] = eta;
      iter = iter + 1;
    }
  }

  VecDestroy(&u0_seq);
  for (int i = 0; i < geo.dimension(); i++){
    VecDestroy(&u0_seq_percomponent[i]);
    VecDestroy(&u_split[i]);
  }
  return m_viscosity;
}

double NS::innerProduct(Vec u, Vec v){
  int u_size; double ip;
  code = VecGetSize(u, &u_size); CHKERR(code);
  Vec Mv = vec(u_size);
  if (u_size == par.nbDofsPerNode()[0]*geo.nbVertices){
    code = MatMult(m_ip[0], v, Mv); CHKERR(code);
  } else if (u_size == par.nbDofsPerNode()[1]*geo.nbVertices){
    code = MatMult(m_ip[1], v, Mv); CHKERR(code);
  } else {
    errorMessage("innerProduct", "Wrong vector size");
  }
  code = VecDot(Mv, u, &ip); CHKERR(code);
  code = VecDestroy(&Mv); CHKERR(code);
  return ip;
}
