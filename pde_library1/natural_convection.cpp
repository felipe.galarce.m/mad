/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2023,
    
     Felipe Galarce at INRIA/WIAS/PUCV

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<natural_convection.hpp>

void NaturalConvection::initialize(Parameters parameters, const Geometry & geometry, const Boundary & boundary){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "Natural Convection: Initializing" << endl;
  par = parameters;
  geo = geometry;
  bd = boundary;
  calculus.initialize(par, geo, bd);
  fe.initialize(par, geo.dimension());
  feBD.initialize(par, geo.dimension());
  m_verbose = par.verbose();
  assert(par.nbDofsPerNode().size() == 3);
  m_interpolator.initialize(par, geo);

  nbDofs = (par.nbDofsPerNode()[0] + par.nbDofsPerNode()[1] + par.nbDofsPerNode()[2])*geo.nbVertices;
  nbDofsPress = geo.nbVertices;
  nbDofsTemp = geo.nbVertices;
  nbDofsVel = par.nbDofsPerNode()[0]*geo.nbVertices;
  nbVertices = geo.nbVertices;
  nbDofsPerNode = par.nbDofsPerNode()[0]; 
  nbNodesPerElement = geo.dimension()+1; 

  u0 = zeros(nbDofs);

  mat(A, nbDofs, nbDofs); 
  mat(M, nbDofs, nbDofs); 
  mat(C, nbDofs, nbDofs); 
  code = MatGetOwnershipRange(A, &m, &n); CHKERR(code);

  if (!(par.power_law_n() == 1)){
    m_non_newtonian = true;
    if (m_world_rank == 0) cout << "Natural Convection: The fluid is Non-Newtonian." << endl;
  }
}

void NaturalConvection::finalize(){
  MatDestroy(&M);
  MatDestroy(&A);
  MatDestroy(&C);
  VecDestroy(&u0);
  KSPDestroy(&ksp);
}

Mat NaturalConvection::assembleLHS_static(){

  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Natural Convection: Assembling discretization matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      vector<int> simplex = geo.elements()[partId][feId];
      /* get finite element coordinates */
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }

      fe.setCoordinates(coordinates);
      fe.computeSize();

      if (feId % (geo.elements()[partId].size() / m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }
      /* Assemble elementary matrices */
      for (int j = 0; j < nbNodesPerElement; j++){
        for (int i = 0; i < nbNodesPerElement; i++){
          for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
            if (par.nbDofsPerNode()[0]*simplex[i]+comp >= m && par.nbDofsPerNode()[0]*simplex[i]+comp < n){
              if (m_non_newtonian == false){
                double Kij = par.viscosity() * fe.stiffness(i,j);
                code = MatSetValue(A, par.nbDofsPerNode()[0]*simplex[i]+comp, par.nbDofsPerNode()[0]*simplex[j]+comp, Kij, ADD_VALUES); CHKERR(code);
              } 
              double Mij = par.density() * fe.mass(i,j);
              /* stiffness */
              code = MatSetValue(M, par.nbDofsPerNode()[0]*simplex[i]+comp, par.nbDofsPerNode()[0]*simplex[j]+comp, Mij, ADD_VALUES); CHKERR(code);
              /* div_phi_i phi_j */
              double Bij = fe.mixed(i,j,comp);
              code = MatSetValue(A, par.nbDofsPerNode()[0]*simplex[i]+comp, nbDofsVel + simplex[j], -1.0*Bij, ADD_VALUES); CHKERR(code); /* -B*/ 
              /* Boussinesq's model for termal expansion */
              double Tij = fe.mass(i,j)*par.gravity()[comp]*par.termalExpansionCoefficient();
              code = MatSetValue(A, par.nbDofsPerNode()[0]*simplex[i]+comp, nbDofsVel + nbDofsPress + simplex[j], Tij, ADD_VALUES); CHKERR(code);
            }

            if (nbDofsVel + simplex[i] >= m && nbDofsVel + simplex[i] < n){
              double Bij = fe.mixed(j,i,comp);
              code = MatSetValue(A, nbDofsVel + simplex[i], par.nbDofsPerNode()[0]*simplex[j]+comp, 1.0*Bij, ADD_VALUES); CHKERR(code); /* B^T */ 
            }
          }
          if (nbDofsVel + simplex[i] >= m && nbDofsVel + simplex[i] < n){
            double Kij = fe.stiffness(i,j);
            /* Brezzi-Pitkaranta stabilization for P1-P1 elements */
            code = MatSetValue(A, nbDofsVel + simplex[i], nbDofsVel + simplex[j], fe.size()*fe.size() * Kij, ADD_VALUES); CHKERR(code);
          }

          /* Temperature difussion and mass storage */
          if (nbDofsVel + nbDofsPress + simplex[i] >= m && nbDofsVel + nbDofsPress + simplex[i] < n){
            double Kij = par.diffusivity() * fe.stiffness(i,j);
            code = MatSetValue(A, nbDofsVel + nbDofsPress + simplex[i], nbDofsVel + nbDofsPress + simplex[j], Kij, ADD_VALUES); CHKERR(code);
            double Mij = par.capacitance() * par.density() * fe.mass(i,j);
            code = MatSetValue(M, nbDofsVel + nbDofsPress + simplex[i], nbDofsVel + nbDofsPress + simplex[j], Mij, ADD_VALUES); CHKERR(code);
          }
        }
      }
    }
  }
  code = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY); CHKERR(code);
  code = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERR(code);

  /* LHS static matrix */
  Mat K = mat(nbDofs, nbDofs);
  code = MatDuplicate(M, MAT_COPY_VALUES, &K); CHKERR(code);
  code = MatScale(K, 1.0/par.timeStep());
  code = MatAXPY(K, 1.0, A, DIFFERENT_NONZERO_PATTERN); CHKERR(code);

  double normLHS = norm(K);
  if (m_world_rank == 0) cout << "Natural Convection: Norm LHS = " << normLHS << endl;
  
  return K;
}

Vec NaturalConvection::assembleRHS(Vec u0_custom){

  /* Bousinesq constant temperature term */
  Vec tempRef = zeros(nbDofs);
  vector<double> tRefStl(nbDofsVel);
  for (int i=0; i<geo.nbVertices; i++){
    for (int comp=0; comp<par.nbDofsPerNode()[0]; comp++){
      tRefStl[par.nbDofsPerNode()[0]*i+comp] = par.termalExpansionCoefficient()*par.gravity()[comp]*par.referenceTemperature();
    }
  }
  code = VecSetValues(tempRef, nbDofsTemp, &range(nbDofsVel+nbDofsPress,nbDofsVel+nbDofsPress+nbDofsTemp)[0], &tRefStl[0], INSERT_VALUES); CHKERR(code);
  code = VecAssemblyBegin(tempRef); CHKERR(code);
  code = VecAssemblyEnd(tempRef); CHKERR(code);
  Vec bb = zeros(nbDofs);
  code = MatMult(M, tempRef, bb); CHKERR(code);

  Vec b = zeros(nbDofs);
  if (u0_custom == NULL){
    code = MatMult(M, u0, b); CHKERR(code);
  } else {
    code = MatMult(M, u0_custom, b); CHKERR(code);
  }
  VecScale(b, 1.0/par.timeStep());
  code = VecAXPY(b, -1.0, bb); CHKERR(code);

  double normRHS = norm(b);
  if (m_world_rank == 0) cout << "Natural Convection: Norm RHS = " << normRHS << endl;
  return b;
}

void NaturalConvection::update(Vec u, double time){
  code = VecZeroEntries(u0); CHKERR(code);
  code = VecAXPY(u0, 1.0, u); CHKERR(code);
}

void NaturalConvection::computeFlows(Vec u){
  double incompressibility_failure = 0.0;
  for (int i = 0; i < geo.bdLabels().size(); i++){
    double Q_bd = bd.flow(u, geo.bdLabels()[i]);
    if (m_world_rank == 0) cout << "Navier-Stokes: flow at boundary label " << geo.bdLabels()[i] << " : " << Q_bd << endl; 
    incompressibility_failure = incompressibility_failure + Q_bd;
  }
  if (m_world_rank == 0) cout << "Navier-Stokes: incompressibility failure = " << incompressibility_failure << endl; 
}

void NaturalConvection::setSolver(){
  if (m_world_rank == 0) cout << "KSP: Configuring" << endl;
//  configureKSP(ksp, par.solver(), par.preconditioner(), par.monitorKSP(), par.use_solution_as_guess_KSP(), par.reuse_preconditioner());
}

void NaturalConvection::setLHS(Mat A){
  if (m_world_rank == 0) cout << "KSP: Seting operators" << endl;
  code = KSPSetOperators(ksp, A, A); CHKERR(code); 
  code = KSPSetUp(ksp); CHKERR(code);
}

void NaturalConvection::solve(Vec b, Vec u){
//  if (!m_non_newtonian){
    if (m_world_rank == 0) cout << "NaturalConvection: Solving linear system" << endl;
    code = KSPSolve(ksp, b, u); CHKERR(code); 
    
    int its;
    double rnorm;
    code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
    code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
    if (par.solver() == "gmres"){
      if (m_world_rank == 0) cout << "NaturalConvection: " << its << " iterations. Residual norm = " << rnorm << endl;
    }
}

Mat NaturalConvection::assembleLHS(Mat LHS_static, Vec u0_custom){

  Vec u0_seq;
  if (u0_custom == NULL){
    u0_seq = getSequential(u0);
  } else {
    u0_seq = getSequential(u0_custom);
  }
  code = MatZeroEntries(C); CHKERR(code);
  double u0_norm;
  code = VecNorm(u0_seq, NORM_2, &u0_norm); CHKERR(code);

  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Natural Convection: Assembling convection matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      vector<int> simplex = geo.elements()[partId][feId];
      /* get finite element coordinates */
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      fe.setCoordinates(coordinates);
      fe.computeSize();

      if (feId % (geo.elements()[partId].size() / par.verbose()) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for simplex: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }

      vector<double> u_el = geo.getNodalValues(u0_seq, simplex, par.nbDofsPerNode()[0]);

      /* Set values from local to global */
      for (int j = 0; j < nbNodesPerElement; j++){
        vector<double> u_node(par.nbDofsPerNode()[0]);
        for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
          u_node[comp] = u_el[par.nbDofsPerNode()[0]*j+comp];
        }
        for (int i = 0; i < nbNodesPerElement; i++){
          for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
            if (par.nbDofsPerNode()[0]*simplex[i]+comp >= m && par.nbDofsPerNode()[0]*simplex[i]+comp < n){
              /* Convective stabilization */
              double Aij = par.density() * fe.advection(i, j, u_node);
              double C_k;
              if (m_non_newtonian){
                C_k = 30*m_viscosity[feId];
              } else {
                C_k = 30*par.viscosity();
              }
              double tau = pow(1.0/par.timeStep()/par.timeStep() + u0_norm/fe.size()/fe.size() + C_k*par.viscosity()/par.density()/(pow(fe.size(),4)), -0.5);
//              double SUPG = fe.size()*fe.size() / tau * fe.supg(i, j, u_node);
//              code = MatSetValue(C, par.nbDofsPerNode()[0]*simplex[i]+comp, par.nbDofsPerNode()[0]*simplex[j]+comp, Aij + SUPG, ADD_VALUES); CHKERR(code);

              if (m_non_newtonian){
                /* Viscosity term for Non-Newtonian fluid*/
                double Kij = m_viscosity[feId] * fe.stiffness(i,j);
                code = MatSetValue(C, par.nbDofsPerNode()[0]*simplex[i]+comp, par.nbDofsPerNode()[0]*simplex[j]+comp, Kij, ADD_VALUES); CHKERR(code);
              }
            }
          }
          if (nbDofsVel + nbDofsPress + simplex[i] >= m && nbDofsVel + nbDofsPress + simplex[i] < n){
            double T_ij = par.density() * par.capacitance() * fe.advection(i, j, u_node);
            code = MatSetValue(C, nbDofsVel + nbDofsPress + simplex[i], nbDofsVel + nbDofsPress + simplex[j], T_ij, ADD_VALUES); CHKERR(code);
          }
        }
      }
    }
  }
  code = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERR(code);
  code = MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY); CHKERR(code);

  /* assemble full LHS matrix */ 
  Mat K = mat(nbDofs, nbDofs);
  code = MatDuplicate(LHS_static, MAT_COPY_VALUES, &K); CHKERR(code);
  code = MatAXPY(K, 1.0, C, DIFFERENT_NONZERO_PATTERN); CHKERR(code);
  return K;
}

/* compute \grad u \cdot n */
Vec NaturalConvection::shearStress(Vec u, int bdLabel){
  if (m_world_rank == 0) cout << "Natural Convection: computing shear stress at boundary label " << bdLabel << "." << endl;
  return calculus.gradientOnBoundary(u, bdLabel);
}

vector<double> NaturalConvection::getViscosity(Vec u0_custom){

  Vec u0_seq;
  if (u0_custom == NULL){
    u0_seq = getSequential(u0);
  } else {
    u0_seq = getSequential(u0_custom);
  }

  m_viscosity.resize(geo.elements()[0].size());
  vector<Vec> u0_seq_percomponent(par.nbDofsPerNode()[0]);

  for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
    u0_seq_percomponent[comp] = getSequential(calculus.decompose_vector_field(u0_seq)[comp]);
  }

  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Natural Convection: Computing apparent viscosity. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      vector<int> simplex = geo.elements()[partId][feId];
      /* get finite element coordinates */
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      fe.setCoordinates(coordinates);
      fe.computeSize();

      if (feId % (geo.elements()[partId].size() / par.verbose()) == 0){
        if (m_world_rank == 0) cout << "  Elementary vector for simplex: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }
   
      vector<vector<double>> u_element(par.nbDofsPerNode()[0]);
      vector<vector<double>> grad_u(par.nbDofsPerNode()[0]);
      for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
        u_element[comp] = geo.getNodalValues(u0_seq_percomponent[comp], simplex, 1);;
        grad_u[comp].resize(par.nbDofsPerNode()[0]);
      }
      for (int comp = 0; comp < par.nbDofsPerNode()[0]; comp++){
        for (int nodeId = 0; nodeId < nbNodesPerElement; nodeId++){
          grad_u[comp] = grad_u[comp] + u_element[comp][nodeId] * transpose(fe.jacobianInverse()) * fe.grad_phi(nodeId);
        }
      }
      grad_u = transpose(grad_u);

      double eta;
      double gamma_dot = sqrt(trace(grad_u*transpose(grad_u)));
      if (gamma_dot != 0){
        eta = par.power_law_m() * pow(gamma_dot, par.power_law_n() - 1.0);
      } 
      if (eta < par.power_law_m()/10000){
        eta = par.power_law_m()/10000;
      } else if (eta > 10000*par.power_law_m()){
        eta = par.power_law_m()*10000;
      }
      m_viscosity[feId] = eta;
    }
  }
  m_p1Viscosity = m_interpolator.interpolateP0_P1(m_viscosity);

  return m_viscosity;
}
