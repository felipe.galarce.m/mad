/*=============================================================================
  This file is part of the code MAD
  Copyright (C) 2017-2023,
    
     Felipe Galarce

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#ifndef MAD_GeneralizedStokes
#define MAD_GeneralizedStokes

#include <iostream>
#include <math.h>
#include <vector>
#include <slepc.h>
#include <STLvectorUtils.hpp>
#include <petscWrapper.hpp>
#include <tools.hpp>
#include <parameters.hpp>
#include <masterElement.hpp>
#include <geometry.hpp>
#include <boundaries.hpp>
#include <calculus.hpp>
#include <fem.hpp>

using namespace std;

class StokesGeneralized{

  public:
    StokesGeneralized(){}
    ~StokesGeneralized(){}

    void initialize(Parameters parameters, const Geometry & geometry, const Boundary & boundary,
                                           double  (*viscosity)      (vector<double>,Parameters), 
                                    vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                           double  (*sigma)          (vector<double>,Parameters));
    void finalize();
    void computeJacobians();
    void assembleStabParameters(  double  (*viscosity)      (vector<double>,Parameters),
                           vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                  double  (*sigma)          (vector<double>,Parameters));
    void computeLocal2GlobalMappings();
    void setLHS(Mat A, Mat P = NULL);
    void assembleLHS_nonNewtonian(double  (*viscosity)      (vector<double>,Parameters), 
                           vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                  double  (*sigma)          (vector<double>,Parameters));
    void assembleLHS_boundary(double (*viscosity) (vector<double>,Parameters));

    Vec solve(Vec rhs, vector<double> (*analytic_p) (vector<double>,double,Parameters));

    Mat A, M;
    KSP ksp;

    Vec u0;
    int nbDofs;

    Boundary bd;
    Calculus calculus;

  private:

    Geometry geo;
    MasterElement fe;
    MasterElementBD feBD;
    Parameters par;

    /* Streamline upwind stabilization */
    vector<vector<double>> m_tau_stab;
    vector<MasterElement>   m_finiteElements;
    vector<MasterElementBD> m_finiteElementsBD;

    PetscErrorCode code;
    int m_verbose;

    int nbDofsPerNode; 
    int nbVertices;
    int nbDofsVel;
    int nbDofsPress;
    int nbNodesPerElement;
    int nbNodesPerElementBD;
    vector<vector<int>> m_loc2Global;

    /* stab parameters */
    double min_nu; 
    double max_nu_2;
    double grad_nu_inf_2;
    double C;
    double m_coef;
    double hmax;

    int m_world_rank, m, n;

    FEM fem;

};  

#endif
