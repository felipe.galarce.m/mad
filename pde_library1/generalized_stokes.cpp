/*=============================================================================
  This file is part of the code MAD
  Multy-physics for biomedicAl engineering and Data assimilation.
  Copyright (C) 2017-2025,
    
     Felipe Galarce at INRIA/WIAS/PUCV

  MAD is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  MAD is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MAD. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/

#include<generalized_stokes.hpp>

void StokesGeneralized::initialize(Parameters parameters, const Geometry & geometry, const Boundary & boundary,
                                           double  (*viscosity)      (vector<double>,Parameters), 
                                    vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                           double  (*sigma)          (vector<double>,Parameters)){
  MPI_Comm_rank(MPI_COMM_WORLD, &m_world_rank);
  if (m_world_rank == 0) cout << "Stokes Generalized: Initializing" << endl;
  par = parameters;
  geo = geometry;
  bd = boundary;
  m_verbose = par.verbose();

  nbDofs = 0;
  for (int i = 0; i < par.nbVariables(); i++){
    nbDofs += par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbDofsPress = geo.nbVertices;
  nbDofsVel = 0;
  for (int i = 0; i < par.nbVariables()-1; i++){
    nbDofsVel = par.nbDofsPerNode()[i]*geo.nbVertices;
  }
  nbVertices = geo.nbVertices;
  nbDofsPerNode = par.nbDofsPerNode()[0]; 
  nbNodesPerElement = geo.dimension()+1; 
  nbNodesPerElementBD = geo.dimension(); 

  mat(A, nbDofs, nbDofs); 
  fem.initialize(par, geo, bd, A);
  code = MatGetOwnershipRange(A, &m, &n); CHKERR(code);

  if (m_world_rank == 0) cout << "Stokes Generalized: dofs splitting:" << endl;
  cout << "  Proc #"<< m_world_rank << " : " << m << " " << n << endl;

  computeJacobians();
  assembleStabParameters(viscosity, grad_viscosity, sigma);
  computeLocal2GlobalMappings();
}

void StokesGeneralized::finalize(){
  MatDestroy(&M);
  MatDestroy(&A);
  VecDestroy(&u0);
  KSPDestroy(&ksp);
}

void StokesGeneralized::computeJacobians(){
  m_finiteElements.resize(geo.nbElements());
  int iter = 0;
  if (m_world_rank == 0) cout << "Stokes Generalized: Computing Jacobians." << endl;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elements()[partId][feId];
      vector<vector<double>> coordinates(nbNodesPerElement);
      for (int i = 0; i < nbNodesPerElement; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      m_finiteElements[iter].initialize(par, geo.dimension());
      m_finiteElements[iter].setCoordinates(coordinates);
      m_finiteElements[iter].computeSize();
      iter = iter + 1;
    }
  }
  iter = 0;
  m_finiteElementsBD.resize(geo.nbElementsBoundary());
  if (m_world_rank == 0) cout << "Stokes Generalized: Computing BD Jacobians." << endl;
  for (int partId = 0; partId < geo.elementsBD().size(); partId++){
    for (int feId = 0; feId < geo.elementsBD()[partId].size(); feId++){ /* loop on elements */
      vector<int> simplex = geo.elementsBD()[partId][feId];
      vector<vector<double>> coordinates(nbNodesPerElementBD);
      for (int i = 0; i < nbNodesPerElementBD; i++){
        coordinates[i] = geo.coordinates()[simplex[i]];
      }
      m_finiteElementsBD[iter].initialize(par, geo.dimension());
      m_finiteElementsBD[iter].setCoordinates(coordinates);
      m_finiteElementsBD[iter].computeSize();
      m_finiteElementsBD[iter].computeNormal();
      iter = iter + 1;
    }
  }
}

void StokesGeneralized::computeLocal2GlobalMappings(){
  int iter = 0;
  m_loc2Global.resize(geo.nbElements());
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Stokes Generalized: Computing local2Global mappings." << endl;
    for (vector<int> simplex : geo.elements()[partId]){ /* loop on elements */
      int nbNodesPerElement = simplex.size();
      /* loc2glob dof mapping */
      m_loc2Global[iter].resize(nbNodesPerElement*nbDofsPerNode);
      for (int i = 0; i < nbNodesPerElement; i++){
        for (int comp = 0; comp < nbDofsPerNode; comp++){
          m_loc2Global[iter][nbDofsPerNode*i+comp] = nbDofsPerNode*simplex[i]+comp;
        }
      }
      iter = iter + 1;
    }
  }
}

void StokesGeneralized::assembleStabParameters(double  (*viscosity)      (vector<double>,Parameters), 
                                        vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                               double  (*sigma)          (vector<double>,Parameters)){
 
  vector<double> visco(geo.coordinates().size()); 
  vector<double> dvisco(geo.coordinates().size()); 
  double sigmamax = 0.0;
  for (int i = 0; i < geo.coordinates().size(); i++){
    visco[i] = viscosity(geo.coordinates()[i], par);
    dvisco[i] = 0.0;
    for (int comp = 0; comp < geo.dimension(); comp++){
      dvisco[i] = dvisco[i] + grad_viscosity(geo.coordinates()[i], par)[comp]*grad_viscosity(geo.coordinates()[i], par)[comp] ;
      if (sigmamax < sigma(geo.coordinates()[i],par)){
        sigmamax = sigma(geo.coordinates()[i],par);
      }
    }
    dvisco[i] = sqrt(dvisco[i]);
  }
  min_nu = min(visco);
  max_nu_2 = pow(max(visco), 2);
  grad_nu_inf_2 = pow(max(dvisco), 2);
  C = par.gamma();

  int iter = 0;
  hmax = 0.0;
  if (m_world_rank == 0) cout << "Navier Stokes: Assembling stabilization parameters." << endl;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */
      double hk = m_finiteElements[iter].size();
      if (hk > hmax){
        hmax = hk;
      }
      iter = iter + 1;
    }
  }
  iter = 0;
  for (int partId = 0; partId < geo.elementsBD().size(); partId++){
    for (int feId = 0; feId < geo.elementsBD()[partId].size(); feId++){ /* loop on elements */
      double hk = m_finiteElementsBD[iter].size();
      if (hk > hmax){
        hmax = hk;
      }
      iter = iter + 1;
    }
  }
  m_coef = 1.0/12.0*hmax*hmax*par.delta() * min_nu / (hmax*hmax*grad_nu_inf_2 + C * max_nu_2);
  if (m_coef > 1.0/(3.0*sigmamax)){
    m_coef = 1.0/(3.0*sigmamax);
  }

  if (m_world_rank == 0) cout << "  h_max: " <<  hmax << endl;
  if (m_world_rank == 0) cout << "  delta: " <<  m_coef << endl;
}

void StokesGeneralized::assembleLHS_nonNewtonian(double  (*viscosity)      (vector<double>,Parameters), 
                                          vector<double> (*grad_viscosity) (vector<double>,Parameters),
                                                 double  (*sigma)          (vector<double>,Parameters)){

  if (par.cfd_stab() == "BVS"){
    assembleLHS_boundary(viscosity);
  }

  int iter = 0;
  for (int partId = 0; partId < geo.elements().size(); partId++){
    if (m_world_rank == 0) cout << "Stokes Generalized: Assembling discretization matrix. Part " << partId << endl;
    for (int feId = 0; feId < geo.elements()[partId].size(); feId++){ /* loop on elements */

      vector<int> simplex = geo.elements()[partId][feId];
      vector<double> feCenter(geo.dimension(), 0.0);
      for (int nodeId = 0; nodeId < nbNodesPerElement; nodeId++){
        feCenter = feCenter + 1.0/((float)nbNodesPerElement) * geo.coordinates()[simplex[nodeId]]; 
      }
      double coef = viscosity(feCenter, par);
      vector<double> gradNu_el = grad_viscosity(feCenter, par);
      double sigma_el = sigma(feCenter, par);

      fem.setSimplex(m_finiteElements[iter], geo.elements()[partId][feId]);
      iter = iter + 1;

      if (feId % (geo.elements()[partId].size() / m_verbose) == 0){
        if (m_world_rank == 0) cout << "  Elementary matrix for element: " << feId << "/" << geo.elements()[partId].size() - 1 << endl;
      }

      for (int var = 0; var < par.nbVariables()-1; var++){
        /* u \cdot v */
        fem.u_dot_v_scalar(sigma_el, var, var);

        /* grad^s u : grad^s v */
        for (int comp = 0; comp < geo.dimension(); comp++){
          // Add laplacian terms
          if (comp == var){
            fem.du_x_dv_y(2.0*coef, comp, comp, var, var);
          } else {
            fem.du_x_dv_y(coef, comp, comp, var, var);
          }
          // Add remaining terms
          if (comp != var){
            fem.du_x_dv_y(coef, comp, var, var, comp);
          }
        }
      }

      /* -p div(v) */
      for (int comp = 0; comp < geo.dimension(); comp++){
        fem.u_dv_y(-1.0, comp, comp, geo.dimension());
      }

      /* div(u) q */
      for (int comp = 0; comp < geo.dimension(); comp++){
        fem.du_x_v(1.0, comp, geo.dimension(), comp);
      }

      if (par.cfd_stab() == "BP"){
        // grad p grad q 
        for (int comp = 0; comp < geo.dimension(); comp++){
          fem.du_x_dv_y(m_coef, comp, comp, geo.dimension(), geo.dimension());
        }
      } else if (par.cfd_stab() == "PSPG"){
        // grad p grad q + sigma u grad q
        for (int comp = 0; comp < geo.dimension(); comp++){
          fem.du_x_dv_y(m_coef, comp, comp, geo.dimension(), geo.dimension());
          fem.u_dv_y(sigma_el * m_coef, comp, geo.dimension(), comp);
        }
        // grad u  grad nu \cdot grad q
        fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 0, 0, geo.dimension(), 0);
        fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 0, 0, geo.dimension(), 1);
        fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 1, 1, geo.dimension(), 0);
        fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 1, 1, geo.dimension(), 1);
        if (geo.dimension() == 3){
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 0, 0, geo.dimension(), 2);
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 1, 1, geo.dimension(), 2);
          fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 2, 2, geo.dimension(), 0);
          fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 2, 2, geo.dimension(), 1);
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 2, 2, geo.dimension(), 2);
        }

        // grad^T u  grad nu \cdot grad q
        fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 0, 0, geo.dimension(), 0);
        fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 1, 0, geo.dimension(), 0);
        fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 0, 1, geo.dimension(), 1);
        fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 1, 1, geo.dimension(), 1);
        if (geo.dimension() == 3){
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 0, 2, geo.dimension(), 0);
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 1, 2, geo.dimension(), 1);
          fem.du_x_dv_y(-1.0*gradNu_el[0] * m_coef, 2, 0, geo.dimension(), 2);
          fem.du_x_dv_y(-1.0*gradNu_el[1] * m_coef, 2, 1, geo.dimension(), 2);
          fem.du_x_dv_y(-1.0*gradNu_el[2] * m_coef, 2, 2, geo.dimension(), 2);
        }
      } else if (par.cfd_stab() == "BVS"){
        // grad p grad q + sigma u grad q
        for (int comp = 0; comp < geo.dimension(); comp++){
          fem.du_x_dv_y (                m_coef, comp, comp, geo.dimension(), geo.dimension());
          fem.u_dv_y    (sigma_el  *  m_coef, comp, geo.dimension(), comp);
        }
        // -2 grad^T u  grad nu \cdot grad q
        fem.du_x_dv_y(-2.0*gradNu_el[0] * m_coef, 0, 0, geo.dimension(), 0);
        fem.du_x_dv_y(-2.0*gradNu_el[1] * m_coef, 1, 0, geo.dimension(), 0);
        fem.du_x_dv_y(-2.0*gradNu_el[0] * m_coef, 0, 1, geo.dimension(), 1);
        fem.du_x_dv_y(-2.0*gradNu_el[1] * m_coef, 1, 1, geo.dimension(), 1);
        if (geo.dimension() == 3){
          fem.du_x_dv_y(-2.0*gradNu_el[2] * m_coef, 0, 2, geo.dimension(), 0);
          fem.du_x_dv_y(-2.0*gradNu_el[2] * m_coef, 1, 2, geo.dimension(), 1);
          fem.du_x_dv_y(-2.0*gradNu_el[0] * m_coef, 2, 0, geo.dimension(), 2);
          fem.du_x_dv_y(-2.0*gradNu_el[1] * m_coef, 2, 1, geo.dimension(), 2);
          fem.du_x_dv_y(-2.0*gradNu_el[2] * m_coef, 2, 2, geo.dimension(), 2);
        }
      }
    }
  }
  code = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERR(code); 
  code = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERR(code);

  /* LHS static matrix */
  double normLHS = norm(A);
  if (m_world_rank == 0) cout << "Stokes Generalized: Norm LHS = " << normLHS << endl;
}

Vec StokesGeneralized::solve(Vec rhs, vector<double> (*analytic_p) (vector<double>,double,Parameters)){  

  if (m_world_rank == 0) cout << "StokesGeneralized: blocking and solving." << endl;

  /* Fake Dirichlet pressure BC */
  matSetInsert(A, geo.dimension()*geo.nbVertices + geo.boundaryNodes(par.walls()[0])[0], 
                  geo.dimension()*geo.nbVertices + geo.boundaryNodes(par.walls()[0])[0], 1e30);
  vecSetInsert(rhs, geo.dimension()*geo.nbVertices + geo.boundaryNodes(par.walls()[0])[0], 
                    analytic_p(geo.coordinates()[geo.boundaryNodes(par.walls()[0])[0]], 0.0, par)[0] * 1e30);

  bd.block(A, "symmetric");
  bd.block(rhs, "symmetric");

  configureKSP(ksp, par);

  code = KSPSetOperators(ksp, A, A); CHKERR(code); 
  code = KSPSetUp(ksp); CHKERR(code);

  Vec u = vec(nbDofs);
  code = KSPSolve(ksp, rhs, u); CHKERR(code); 
  double rnorm; int its;
  code = KSPGetResidualNorm(ksp, &rnorm); CHKERR(code);
  code = KSPGetIterationNumber(ksp, &its); CHKERR(code);
  if (par.solver() == "gmres"){
    if (m_world_rank == 0) cout << "StokesGeneralized: " << its << " iterations. Residual norm = " << rnorm << endl;
  }
  return u;
}

void StokesGeneralized::assembleLHS_boundary(double (*viscosity) (vector<double>,Parameters)){

  int iter = 0;
  for (int partId = 0; partId < geo.elementsBD().size(); partId++){
    if (m_world_rank == 0) cout << "Stokes Generalized: Assembling BVS boundary matrix. Part " << partId << endl;
    for (vector<int> bdElement : geo.elementsBD()[partId]){

      fem.setSimplexBD(m_finiteElementsBD[iter], bdElement);
      vector<double> normal = m_finiteElementsBD[iter].normal();

      if (par.elementFieldType() == "nodal"){
        vector<double> nu(geo.dimension());
        for (int idNode = 0; idNode < geo.dimension(); idNode++){
          vector <double> coord = geo.coordinates()[bdElement[idNode]];
          nu[idNode] = viscosity(coord, par);
        }
        fem.BD_a_du_x_dv_y(nu,  normal[1] * m_coef, 0, 0, geo.dimension(), 1); /*  ny nu dq/dx dv/dx */
        fem.BD_a_du_x_dv_y(nu, -normal[1] * m_coef, 0, 1, geo.dimension(), 0); /* -ny nu dq/dx du/dy */
        fem.BD_a_du_x_dv_y(nu, -normal[0] * m_coef, 1, 0, geo.dimension(), 1); /* -nx nu dq/dy dv/dx */
        fem.BD_a_du_x_dv_y(nu,  normal[0] * m_coef, 1, 1, geo.dimension(), 0); /*  nx nu dq/dy du/dy */
        if (geo.dimension() == 3){
          fem.BD_a_du_x_dv_y(nu,  normal[2] * m_coef, 1, 1, geo.dimension(), 2); 
          fem.BD_a_du_x_dv_y(nu, -normal[2] * m_coef, 1, 2, geo.dimension(), 1); 
          fem.BD_a_du_x_dv_y(nu, -normal[1] * m_coef, 2, 1, geo.dimension(), 2); 
          fem.BD_a_du_x_dv_y(nu,  normal[1] * m_coef, 2, 2, geo.dimension(), 1); 
          fem.BD_a_du_x_dv_y(nu,  normal[0] * m_coef, 2, 2, geo.dimension(), 0); 
          fem.BD_a_du_x_dv_y(nu, -normal[0] * m_coef, 2, 0, geo.dimension(), 2); 
          fem.BD_a_du_x_dv_y(nu, -normal[2] * m_coef, 0, 2, geo.dimension(), 0); 
          fem.BD_a_du_x_dv_y(nu,  normal[2] * m_coef, 0, 2, geo.dimension(), 0); 
        }
      } else if (par.elementFieldType() == "P0"){
        vector<double> feCenter(geo.dimension(), 0.0);
        for (int nodeId = 0; nodeId < nbNodesPerElementBD; nodeId++){
          feCenter = feCenter + 1.0/((float)nbNodesPerElementBD) * geo.coordinates()[bdElement[nodeId]]; 
        }
        double nu = viscosity(feCenter, par);

        fem.BD_du_x_dv_y(nu *  normal[1] * m_coef, 0, 0, geo.dimension(), 1); /*  ny nu dq/dx dv/dx */
        fem.BD_du_x_dv_y(nu * -normal[1] * m_coef, 0, 1, geo.dimension(), 0); /* -ny nu dq/dx du/dy */
        fem.BD_du_x_dv_y(nu * -normal[0] * m_coef, 1, 0, geo.dimension(), 1); /* -nx nu dq/dy dv/dx */
        fem.BD_du_x_dv_y(nu *  normal[0] * m_coef, 1, 1, geo.dimension(), 0); /*  nx nu dq/dy du/dy */
        if (geo.dimension() == 3){
          exit(1);
        }
      }
      iter = iter + 1;
    }
  }
}
